% LaTex source code for ngsSH documentation
% last modified: 08-04-16
% Modified by: Maxwell Sherman
% Notes:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Preamble

\documentclass[11pt]{article}

\usepackage{fullpage}
\usepackage{scrextend}
\usepackage{enumitem}
\usepackage{verbatim}
\usepackage[parfill]{parskip}
\usepackage[colorlinks=True, linkcolor=blue, citecolor=blue]{hyperref}

% \newenvironment{APIentry}[1]
%   {{\it #1}
%    \begin{itemize}
%   }
%   {
%   \end{itemize}
%   }

\newcommand{\APIentry}[2] {
  \hspace*{1.5em} {\it #1:}

  \hspace*{3em} #2

}

\newenvironment{APIargs}[1]
  {\hspace*{1.5em}
   {\it #1:}

    \hspace*{3em}
    \begin{tabular}{ll}
  }
  {
    \end{tabular}

  }
\newenvironment{APIverbatim}[1]
  {\hspace*{1.5em}
   {\it #1:}
    \hspace*{3em}
    \verbatim
  }
  {
    \endverbatim

  }
% \newcommand{\description}[1] {
%     \begin{itemize}
%       \item[] #1
%     \end{itemize}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{ngsSH: A User Guide}

\author{
  Maxwell A. Sherman \\
  \texttt{maxwell\_sherman@hms.harvard.edu}
}

\date{\today}

\begin{document}

\maketitle

\tableofcontents

\section{Introduction}

  The [n]ext [g]eneration [s]equencing [SH]ell (ngsSH for short) is an interactive command line environment designed to greatly simplify the large-scale analysis of next generation sequencing data. It can be deployed on both local machines and large-scale servers with equal success. While a familiarity with a command line environment will be helpful, no prior programming knowledge is necessary to take advantage of the many functions already available within ngsSH. Nonetheless, the package is designed in such a way that even users with minimal familiarity with scripting languages should find it straightforward to expand the functionality of ngsSH to fit their own needs.

\subsection{Availability}
  The package is available from \url{https://bitbucket.org/maxwell\_sherman/next-gen-seq-shell} by contacting the author.

\subsection{Dependencies}

{\it python}
  \begin{itemize}
    \item python 2.7.6 or equivalent
    \item pathlib2
    \item cmd
    \item pandas
  \end{itemize}
{\it R}
  \begin{itemize}
    \item optparse
    \item GenomicRanges
    \item gtools
    \item ggplot2
    \item GridExtra
  \end{itemize}

\section{Basic Usage}

\textbf{NB}: ngsSH is designed to be used from within the top-level source folder; that is, \texttt{/path/to/ngsSH/}. This guide is written following this convention. Executing ngsSH from a different directory should only be done with great care.

\subsection{Starting ngsSH and getting help}

To see a list of all functions available within the ngsSH environment, simply execute:
\begin{verbatim}
  $ ./ngssh.py -h
\end{verbatim}
This will print a list of all functions and brief description of each. It is also the most basic usage of ngsSH and will work only if all python dependencies are satisfied (NB: help will still be displayed even if R dependencies are missing).

To start the shell, execute
\begin{verbatim}
  $ ./ngssh.py /path/to/data/dir shell
\end{verbatim}
where \texttt{/path/to/data/dir} is an absolute path to an ngsSH compatible data directory (see Section~\ref{subsect:dir_comp} for further details) and \texttt{shell} instructs the program to launch an interactive shell environment.

\textbf{NB}: by design, ngsSH cannot navigate to directories outside of the specified data directory.

If ngsSH launches without error, you will be greeted by:
\begin{verbatim}
  [n]ext [g]eneration [s]equencing [SH]ell

  Your root project directory is /path/to/data/dir

  [ngsSH]
\end{verbatim}
and your shell prompt will be replaced by \texttt{[ngsSH]}.

From this point, commands are passed to ngsSH as
\begin{verbatim}
  [ngsSH] <cmd> [args...]
\end{verbatim}
where \texttt{args} are always of the form \texttt{-f bar} or \texttt{--foo bar}.

To see all available functions, execute:
\begin{verbatim}
  [ngsSH] help
\end{verbatim}

To get a brief description of a particular function, execute:
\begin{verbatim}
  [ngsSH] help <cmd>
\end{verbatim}
where \texttt{<cmd>} is the name of the function.

To see usage for a particular command, execute:
\begin{verbatim}
  [ngsSH] <cmd> -h
\end{verbatim}

\subsection{Exiting ngsSH}

To exit ngsSH, simply type {\tt exit}. If this is successful, you will see:
\begin{verbatim}
  [ngsSH] exit
  Exiting...
  $
\end{verbatim}
where {\tt \$} is the usual bash prompt.

In the event that {\tt exit} fails to work, {\tt <ctrl>-d} will force exit the program. ({\tt exit} can fail if a change to the source code interferes with the CLI interpretation of user input).

\subsection{Convenient features}

ngsSH includes several features for the convenience of the user. These include:

\begin{itemize}
  \item {\tt tab-completion}:
    \begin{itemize}
      \item when typing a command {\tt <tab>} will auto-complete matching commands
      \item when typing args, {\tt <tab>} will auto-complete using contents of the current working directory.
    \end{itemize}
  \item {\tt history}: ngsSH stores the previous 10000 commands from all sessions. History is accessed with the up and down arrow keys
  \item {\tt reverse history search}: after typing a partial or complete command, the up and down arrow keys scroll through matching commands in history
  \item {\tt <ctrl>-c}: typing {\tt <ctrl>-c} aborts a command as in bash
  \item {\tt clear screen}: typing {\tt <ctrl>-l} clears the screen as in bash
  \item {\tt vim file editing}: files can be opened and edited with vim from within ngsSH be executing:
    \begin{verbatim}
    [ngsSH] vi <file>
    \end{verbatim}
\end{itemize}

\subsection{On data directory compatibility} \label{subsect:dir_comp}

ngsSH is quite flexible in the format of a data directory and will, in general, automatically handle file I/O modulo user-specified alterations. However, the shell operates under the basic assumption that the base data directory contains subdirectories of files to be analyzed. 

For example if the user wishes to perform an analysis on a set of bam files, in the project \texttt{my\_proj}, then the user should point ngsSH to \texttt{/path/to/my\_proj} and ngsSH will assume that the bam files will be found in a subdirectory \texttt{/path/to/my\_proj/bam}.

{\bf NB}: ngsSH should be pointed to \texttt{/path/to/my\_proj} {\it not} \texttt{/path/to/my\_proj/bam}.

{\bf NB}: ngsSH can be pointed to a data directory either at start-up by executing
\begin{verbatim}
 $ ./ngsSH.py /path/to/data/dir shell
\end{verbatim}
or by launching ngsSH in a parent directory and navigating to the desired project directory. i.e. following the above syntax 
\begin{verbatim}
  $ ./ngsSH.py /path/to/data
  .
  .
  .
  [ngsSH] cd dir
\end{verbatim}

\section{Options common to analysis functions}

All analysis functions within ngsSH share a common core set of options. To condense the API reference section and generally make usage explanations easier, we list these options here.

{\bf NB}: Please note, all options either have a default value (listed in [] below) or need not otherwise be specified.

\subsection{File I/O Options}

These options set behavior for finding files for a given analysis

\begin{APIargs}{file I/O options}
    {\tt -p, --pattern [PATTERN ...]}& optional pattern for file matching \\
    {\tt -v, --invert\_pattern [INVERT\_PATTERN ...]}& pattern to exclude from matching \\
    {\tt -r, \-recursive} & search for files recursively \\
    {\tt -S, \-Suffix SUFFIX} & suffix of file to match
\end{APIargs}

({\bf NB}: {\tt -S} currently only supported by cnv functions)

\subsection{Runtime Options}

These options set behavior when executing an analysis on a set of files.

\begin{APIargs}{runtime options}
  {\tt -e \{local,server,save\}} & execution location \\
  & \\
  \multicolumn{2}{l}{\qquad {\tt local}: execute commands on local machine} \\
  \multicolumn{2}{l}{\qquad {\tt server}: submit commands to a server for execution} \\
  \multicolumn{2}{l}{\qquad {\tt save}: save the commands to an executable SH file, but do not execute} \\
  & \\
  {\tt -E \{serial, parallel, print\}} & execution type \\
  & \\
  \multicolumn{2}{l}{\qquad {\tt serial}: execute commands one at a time on a single node (use with {\tt -e server})} \\
  \multicolumn{2}{l}{\qquad {\tt parallel}: execute jobs in parallel across specified number of nodes (use with {\tt -e local})} \\
  \multicolumn{2}{l}{\qquad {\tt print}: just print the commands, but do not execute} \\
  & \\
  \texttt{-V, --verbose} & verbose execution of commands \\
  \texttt{-n} & number of cores to use when running jobs in parallel \\
              & (only applicable with {\tt -E parallel})
\end{APIargs}

\subsection{Server Options}

These options set behavior when ngsSH submits a command to a server for execution.

{\bf NB}: these are options used by bsub

\begin{APIargs}{sever settings}
  {\tt -q QUEUE} & queue to submit job to [{\tt short}] \\
  {\tt -W WALL\_TIME} & wall-time [{\tt <automatically set based on queue>}] \\
  {\tt -R RESOURCE} & resource request \\
  {\tt -w DEPENDENCY} & job won't start until {\tt -w DEPENDENCY} finishes \\
  {\tt -N} & notify be email when task completed
\end{APIargs}

\subsection{Alignment Options}
Some functions require knowledge of alignment details, especially those which have species specific behavior.

\begin{APIargs}{alignment details}
  {\tt -B, --build \{hg19,hg18,mm9\}} & build [{\tt hg19}] \\
  {\tt -l, --read\_length \{36,50,75,100\}} & read length [{\tt 100}]
\end{APIargs}

\subsection{Generic Analysis Function Usage}
All Analysis functions have the following common usage:
  \begin{APIverbatim}{}
       analysis_fnc [-h] [-B {hg19,hg18,mm9}]
                    [-l {36, 50, 75, 100}]
                    [-p PATTERN [PATTERN ...]]
                    [-V INVERT_PATTERN [INVERT_PATTERN ...]]
                    [-r] [-S SUFFIX]
                    [-e {local,server,save}]
                    [-E {serial,parallel,print}] [-V]
                    [-n N_PROCS] [-q QUEUE]
                    [-W WALL_TIME] [-N] [-R RESOURCE]
                    [-w DEPENDENCY]
  \end{APIverbatim}


\section{Use case: calling CNVs} \label{sect:CNVs}

Below we give an example workflow for calling copy number variations (CNVs) in both unpaired samples and in tumor / matched-normal samples. It consists of three steps:
\begin{enumerate}
  \item Extracting uniquely mapped positions
  \item Normalizing and binning the uniquely mapped positions
  \item Performing segmentation based on the normalized bins
\end{enumerate}
Several optional post-processing steps can then be applied as desired.

{\bf NB}: the default settings in all function assume {\it unmatched} samples. We will note necessary option changes for tumor / matched normal samples.

\subsection{Starting directory structure}

\begin{itemize}
  \item {\it unmatched samples}: Within the project directory, create a {\tt bam/} directory. Move bam files for all samples into that directory.
  \item {\it matched samples}: create two directories {\tt bam\_tumor/} and {\tt bam\_normal} in the project directory. Move tumor sample bam files and normal sample bam files into the respective directories. ({\bf NB:} the files must be named such that the normal samples are in the same order as the tumor samples)
\end{itemize}

{\bf NB}: We suggest symbolically linking to bam files instead of copying or moving them from other locations.

\subsection{Extracting uniquely mapped positions}

This is accomplished using the command {\tt bam\_to\_mappability}. It is species specific so {\tt -B <build>} should be specified if it is not {\tt hg19}. See Section~\ref{sect:API_ref} for general usage notes.
\begin{itemize}
  \item {\it unmatched samples}:
    \begin{verbatim}
    [ngsSH] bam_to_mappability -q park_2h
    \end{verbatim}
  \item {\it matched samples}:
    \begin{verbatim}
    [ngsSH] bam_to_mappability -i bam_tumor -o map_tumor -q park_2h
    [ngsSH] bam_to_mappability -i bam_normal -o map_normal -q park_2h
    \end{verbatim}
\end{itemize}

{\bf NB}: If your project has $>100$ samples, we suggest using the {\tt -e save} option and submitting the jobs using the sh files as for human samples 24 jobs will be submitted for each sample (one per chromosome), and it can be more efficient to directly submit than to submit through a python interface.

\subsection{Calculating bins from mappable positions}

After extracting the uniquely mapped positions, we normalize the read counts and bin into bins containing equal numbers of uniquely mapped positions. This is done using the command {\tt map\_to\_bins}. Normalization is based on GC content and nucleotide composition. See Section~\ref{sect:API_ref} for general usage notes.

For high quality bulk samples, we suggest using a bin size of 100 or 1000 bp. For low quality samples or single cell samples, we suggest a bin size of at least 100000.

{\bf NB}: for deeply sequenced samples, this step can take $>6$ hours.

\begin{itemize}
  \item {\it unmatched samples}:
    \begin{verbatim}
    [ngsSH] map_to_bins -b 100 -q park_1d
    \end{verbatim}
  \item {\it unmatched samples}:
    \begin{verbatim}
    [ngsSH] map_to_bins -i map_tumor -o bins_tumor -b 100 -q park_2h
    [ngsSH] map_to_bins -i map_normal -o bins_normal -b 100 -q park_2h
    \end{verbatim}
\end{itemize}

\subsection{Performing segmentation of binned read counts}
After uniquely mapped read counts have been normalized and binned, we perform segmentation using {\tt bins\_to\_cnvs}. In this step, a smoothing factor must be specified. For high quality bulk data, we suggest using a smoothing factor of 3. For low quality or single cell data, we suggest a smoothing factor of at least 50. See Section~\ref{sect:API_ref} for general usage notes.

{\bf NB}: by default, the smoothing factor is automatically scaled based on the amount of bin-to-bin read depth variation. To turn this behavior off, use the {\tt --noscale} option.

\begin{itemize}
  \item {\it unmatched samples}:
    \begin{verbatim}
    [ngsSH] bins_to_cnvs -b 100 -L 3 -q park_2h
    \end{verbatim}
  \item {\it matched samples}:
    \begin{verbatim}
    [ngsSH] bins_to_cnvs -i bins_tumor --control bins_normal -b 100 -L 3 -q park_2h
    \end{verbatim}
    {\bf NB}: to use one control sample for all tumor samples, specify {\tt --control bins\_normal/sample\_name}
\end{itemize}

{\bf NB}: for high quality tumor / matched-normal samples, filtering for segments with an absolute log2 copy ratio > 0.2 and a p-value less than 0.05 should be sufficient to obtain an initial set of candidate CNV regions.

\subsection{CNV visualization}
  ngsSH includes several methods for visualizing segmentation results. These include:
  \begin{itemize}
    \item {\tt cnv\_post\_processing}: plots segmentation profile with bins in background and performs gene annotation.
      \begin{verbatim}
      [ngsSH] cnv_post_processing -b 100 -L 3
      \end{verbatim}
      {\bf NB}: plotting functionality is only available for {\it unmatched samples}
    \item {\tt plot\_bins\_agg}: plot bins from all samples on a single figure
      \begin{verbatim}
      [ngsSH] plot_bins_agg -b 100 -L 3
      \end{verbatim}
      {\bf NB}: the {\tt -p} and {\tt -v} options are particularly useful for plotting a subset of samples (a subset can be a single sample if so desired).
    \item {\tt plot\_segs\_agg}: plot a heatmap of all segments from all samples on a single figure.
      \begin{verbatim}
      [ngsSH] plot_segs_agg -b 100 -L 3 --loc 1:0-1000000000
      \end{verbatim}
      {\bf NB}: the {\tt --loc chr:start-end} option can be particularly useful to produce higher detail plots.
    \item {\tt plot\_bins\_segs\_agg}: plot segments with background bins for all samples on a single figure with (optional) gene annotation
      \begin{verbatim}
      [ngsSH] plot_bins_cnvs -b 100 -L 3 --loc 1:659400000-659700000 --genes
      \end{verbatim}
      {\bf NB}: this function is only meaningful for {\it unmatched samples}. Additionally, we suggest only using the {\tt --gene} option when plotting relatively small regions of the genome.
  \end{itemize}

  {\bf NB}: Please see Section~\ref{sect:API_ref} for general usage of the above functions.

\subsection{CNV Post Processing}
Especially for unmatched samples, post processing the raw CNV calls is particularly important for removing recurrent artifacts and common variants. Doing so requires having a panel of normal samples which have already been processed through the CNV calling pipeline or a BED-style file of regions of common variation. There are two filtration steps followed by a dimensional reduction step and gene annotation:
\begin{enumerate}
  \item Remove segments from individual samples if they overlap a region of normal variability by more than 50\% of their length.
  \item Remove segments from individual samples if they overlap a blacklist region by more than 50\% of their length. A blacklist region is defined as any region where at least 50\% of samples have a copy variation.
  \item Aggregate all samples into a single Minimal Common Regions (MCR) set of calls.
  \item Annotate the MCR call set.
\end{enumerate}

The actual filtration itself proceeds in two steps:
\begin{enumerate}
  \item Create a profile if normal variation using a panel of controls

    In the directory of the control samples we execute {\tt cnv\_seg\_aggregate}
    \begin{verbatim}
    [ngsSH] cnv_seg_aggregate -b 100 -L 3 -c 0.2 -p 1.0 --res mean
    \end{verbatim}
    This will produce a file in {\tt cnvs/agg} called
    \begin{verbatim}
    [project_name]__b[bin_size]_l[lambda].t[cutoff*10].p[pvalue*100].mean.seg.agg
    \end{verbatim}

    {\bf NB}: this step can be skipped if you already have a BED-style file of regions of normal variation.
  \item Use {\tt cnv\_post\_pipeline}
    \begin{verbatim}
    [ngsSH] cnv_post_pipeline -b 100 -L 3 -c 0.2 -d 0.05 
                              --f_normal_var /path/to/normal_variation.file 
    \end{verbatim}
    Please see Section~\ref{sect:API_ref} for more notes on this function
\end{enumerate}

\section{API Reference} \label{sect:API_ref}

Below is a list of all functions available within ngsSH by default along with general usage.

\subsection{ngssh.py}

% \begin{itemize}
  \APIentry{Description}{Top level executible which instantiates the shell environment}
  \APIentry{Usage}{{\tt \$ ./ngsSH.py [-h] dproj shell}}

  \begin{APIargs}{positional arguments}
      dproj& root directory of the project \\
      shell& start the shell
  \end{APIargs}

  \begin{APIargs}{optional arguments}
      -h, --help& show help message and exit
  \end{APIargs}
%end{itemize}

\subsection{CLI functions}

The following is a list of functions that set-up the general functionality of ngsSH. Analysis functions are listed in the proceeding section.

  {\bf NB}: the following is common usage for every function as is not documented individually

  \APIentry{Usage}{{\tt [ngsSH] CLI\_cmd [-h]}}
  \begin{APIargs}{optional arguments:}
      {\tt -h, --help} & show help message and exit
  \end{APIargs}

  {\bf cd}

    \APIentry{Description}{change directory relative to current working directory or absolutely relative to {\tt dproj}}
    \APIentry{Usage}{{\tt [ngsSH] cd [-h] PATH}}
    \begin{APIargs}{positional arguments}
        {\tt path} & path to new directory. Must be subdirectory of {\tt dproj}
    \end{APIargs}
    \APIentry{Notes}{ngsSH will not allow a user to navigate below {\tt dproj}}

  {\bf ls}

    \APIentry{Description}{list contents of current directory}
    \APIentry{Usage}{{\tt [ngsSH] ls [-h]}}
    \APIentry{Notes}{unlike bash, {\tt ls} can only list contents of current directory}

  {\bf pwd}

    \APIentry{Description}{print current working directory}
    \APIentry{Usage}{{\tt [ngsSH] [-h] pwd}}

  {\bf vi}

    \APIentry{Description}{view file using vim}
    \APIentry{Usage}{{\tt [ngsSH] vi [-h] PATH}}

  {\bf njobs}

    \APIentry{Description}{print number of running and pending jobs}
    \APIentry{Usage}{{\tt [ngsSH] njobs [-h]}}

  {\bf bjobs}

    \APIentry{Description}{list jobs submitted with bsub}
    \APIentry{Usage}{{\tt [ngsSH] bjobs [-h]}}

  {\bf bkill}

    \APIentry{Description}{kill specified job}
    \APIentry{Usage}{{\tt [ngsSH] bkill [-h] job\_id}}
    \begin{APIargs}{positional arguments}
        {\tt job\_id} & ID of job to be killed
    \end{APIargs}

  {\bf help}

    \APIentry{Description}{get help for function}
    \APIentry{Usage}{{\tt [ngsSH] help [-h] fnc}}

  {\bf exit}

    \APIentry{Description}{exit the shell}
    \APIentry{Usage}{{\tt [ngsSH] exit [-h]}}

\subsection{Analysis functions}

  {\bf NB}: the folling is common usage for every analysis function. For the sake of laconism, we document these here and leave them out of the individual function references below.

  \begin{APIverbatim}{Usage}
       analysis_cmd [-h] [-B {hg19,hg18,mm9}]
                    [-l {36, 50, 75, 100}]
                    [-p PATTERN [PATTERN ...]]
                    [-V INVERT_PATTERN [INVERT_PATTERN ...]]
                    [-r] [-S SUFFIX]
                    [-e {local,server,save}]
                    [-E {serial,parallel,print}] [-V]
                    [-n N_PROCS] [-q QUEUE]
                    [-W WALL_TIME] [-N] [-R RESOURCE]
                    [-w DEPENDENCY]
  \end{APIverbatim}
  \begin{APIargs}{file I/O options}
      {\tt -p, --pattern [PATTERN ...]}& optional pattern for file matching \\
      {\tt -v, --invert\_pattern [INVERT\_PATTERN ...]}& pattern to exclude from matching \\
      {\tt -r, \-recursive} & search for files recursively \\
      {\tt -S, \-Suffix SUFFIX} & suffix of file to match
  \end{APIargs}
  \begin{APIargs}{runtime options}
    {\tt -e \{local,server,save\}} & execution location \\
    % & \\
    % \multicolumn{2}{l}{\qquad {\tt local}: execute commands on local machine} \\
    % \multicolumn{2}{l}{\qquad {\tt server}: submit commands to a server for execution} \\
    % \multicolumn{2}{l}{\qquad {\tt save}: save the commands to an executable SH file, but do not execute} \\
    % & \\
    {\tt -E \{serial, parallel, print\}} & execution type \\
    % & \\
    % \multicolumn{2}{l}{\qquad {\tt serial}: execute commands one at a time on a single node (use with {\tt -e server})} \\
    % \multicolumn{2}{l}{\qquad {\tt parallel}: execute jobs in parallel across specified number of nodes (use with {\tt -e local})} \\
    % \multicolumn{2}{l}{\qquad {\tt print}: just print the commands, but do not execute} \\
    % & \\
    \texttt{-V, --verbose} & verbose execution of commands \\
    \texttt{-n} & number of cores to use when running jobs in parallel \\
                & (only applicable with {\tt -E parallel})
  \end{APIargs}
  \begin{APIargs}{sever settings}
    {\tt -q QUEUE} & queue to submit job to [{\tt short}] \\
    {\tt -W WALL\_TIME} & wall-time [{\tt <automatically set based on queue>}] \\
    {\tt -R RESOURCE} & resource request \\
    {\tt -w DEPENDENCY} & job won't start until {\tt DEPENDENCY} finishes \\
    {\tt -N} & notify be email when task completed
  \end{APIargs}
  \begin{APIargs}{alignment details}
    {\tt -B, --build \{hg19,hg18,mm9\}} & build [{\tt hg19}] \\
    {\tt -l, --read\_length \{36,50,75,100\}} & read length [{\tt 100}]
  \end{APIargs}

  {\bf bam\_by\_chrm}

  \APIentry{Description}{Separate one bam file into each chromosome}
  \APIentry{Usage}{{\tt [ngsSH] bam\_by\_chrm [-i DIR\_IN] [-o DIR\_OUT]}}
  \begin{APIargs}{optional arguments}
      {\tt -h, --help} & show help message and exit \\
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam\_by\_chrm/}]
  \end{APIargs}

  {\bf bam\_to\_mappability}

  \APIentry{Description}{Get uniquely mapped positions from a bam file}
  \begin{APIverbatim}{Usage}
       [ngsSH] bam_to_mappability [i DIR_IN] [-o DIR_OUT]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt mappability/}]
  \end{APIargs}

  {\bf bam\_to\_mapd}

  \APIentry{Description}{Calculate MAPD score from a bam file}
  \APIentry{Usage}{{\tt [ngsSH] bam\_to\_mapd [-i DIR\_IN] [-o DIR\_OUT]}}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt mapd/}]
  \end{APIargs}

  {\bf bins\_to\_mapd}

  \APIentry{Description}{Calculate MAPD score from a CNV bin file}
  \begin{APIverbatim}{Usage}
       [ngsSH] bam_to_mapd [-i DIR_IN] [-o DIR_OUT]
                           [-b BIN_SIZES [BIN_SIZES ...]]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bins/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt mapd/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores
  \end{APIargs}

  {\bf bam\_avg\_depth}
  \APIentry{Description}{calculate average depth of a bam file}
  \APIentry{Usage}{{\tt [ngsSH] bam\_avg\_depth [-i DIR\_IN] [-o DIR\_OUT]}}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt depth/}]
  \end{APIargs}

  {\bf bam\_downsample}

  \APIentry{Description}{downsample a bam file to a specified depth}
  \begin{APIverbatim}{Usage}
       [ngsSH] bam_downsample [-i DIR_IN] [-o DIR_OUT]
                             [-d DEPTH [DEPTH ...]]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -d, --depth} & desired depth(s) after downsampling
  \end{APIargs}

  {\bf map\_to\_bins}

  \APIentry{Description}{Calculate bins based on uniquely mapped reads using NBICseq algorithm}
  \begin{APIverbatim}{Usage}
       [ngsSH] map_to_bins [-i DIR_IN] [-o DIR_OUT]
                           [-b BIN_SIZES [BIN_SIZES ...]]
                           [-P PERC]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt mappability/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bins/}] \\
      {\tt -b, --bin\_sizes} & bin size(s) for which to calculate MAPD scores [100] \\
      {\tt -P, --perc} & subsampling percentage for normalization [0.0005]
  \end{APIargs}

  {\bf bins\_to\_cnvs}

  \APIentry{Description}{Call CNVs based on predefined bins. Uses BICseq2 algorithm}
  \begin{APIverbatim}{Usage}
       [ngsSH] bins_to_cnvs [-i DIR_IN] [-o DIR_OUT]
                            [-b BIN_SIZES [BIN_SIZES ...]]
                            [-L LAMTHA [LAMTHA]]
                            [--control CONTROL] [--noscale]
                            [--noBootstrap] [-m]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bins/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt cnvs/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
      {\tt --control} & path to control bin file or dir (optional) \\
      {\tt --noBootstrap} & do not perform a permuation test to assign p values \\
      {\tt -m, --multisample} & perform multi-sample segmentation using all samples \\
  \end{APIargs}

  {\bf cnv\_postproc}

  \APIentry{Description}{Annotate and plot a CNV profile}
  \begin{APIverbatim}{Usage}
       [ngsSH] cnv_postproc [-i DIR_IN] [-o DIR_OUT]
                            [-b BIN_SIZES [BIN_SIZES ...]]
                            [-L LAMTHA [LAMTHA]]
                            [--loc LOC]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt cnvs/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt cnvs/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
      {\tt --loc} & genome location to plot specified as {\tt chrom:start-end}
  \end{APIargs}

  {\bf cnv\_post\_pipeline}

  \APIentry{Description}{Remove normal variation, artifacts, and perform dimension reduction of CNV data}
  \begin{APIverbatim}{Usage}
       [ngsSH] cnv_post_pipeline [-i DIR_IN] [-o DIR_OUT]
                                 [-b BIN_SIZES [BIN_SIZES ...]]
                                 [-L LAMTHA [LAMTHA]] [-c CUTOFF]
                                 [-d PVALUE] [-f FRAC] [-F F_PROB]
                                 [-P PERC] [--f_norm F_NORM]
                                 [--f_norm_var F_NORM_VAR]
                                 [--f_annot F_ANNOT] [--noRenormSex]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
      {\tt -c, --cutoff} & log2 copy ratio threshold for segment significance [0.3] \\
      {\tt -d, --pvalue} & p-value threshold for segment significance [1] \\
      {\tt -f, --frac} & fraction of samples with variation to call problematic region [0.5] \\
      {\tt -F, --f\_prob} & path to problem file. Overrides {\tt -f} \\
      {\tt -P, --perc} & minimum percent overlap of case / control segments to remove case [0.5] \\
      {\tt --f\_norm\_var} & path to file defining regions of normal variability \\
      {\tt --f\_annot} & path to gene annotation file \\
      {\tt --noRenormSex} & do not mean renormalize sex chromosomes \\
  \end{APIargs}

  {\bf cnv\_seg\_aggregate}

  \APIentry{Description}{Aggregate CNV profiles into a single reduced profile}
  \begin{APIverbatim}{Usage}
       [ngsSH] cnv_seg_aggregate [-i DIR_IN] [-o DIR_OUT]
                                 [-b BIN_SIZES [BIN_SIZES ...]]
                                 [-L LAMTHA [LAMTHA]] [-c CUTOFF]
                                 [-d PVALUE] [--res {mean,median}]
                                 [-g {n,sig}] [--noRenormSex]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
      {\tt -c, --cutoff} & log2 copy ratio threshold for segment significance [0.3] \\
      {\tt -d, --pvalue} & p-value threshold for segment significance [1] \\
      {\tt --res \{mean,median\}} & ratio resolution method for overlapping segs [mean] \\
      {\tt -g, --groupby \{n,sig\}} & grouping for plotting. n: number; sig: significance [n] \\
      {\tt --noRenormSex} & do not mean renormalize sex chromosomes \\
  \end{APIargs}

  {\bf plot\_bins\_agg}

  \APIentry{Description}{Plot read-depth bin profiles for several samples}
  \begin{APIverbatim}{Usage}
       [ngsSH] plot_bins_agg [-i DIR_IN] [-o DIR_OUT]
                             [-b BIN_SIZES [BIN_SIZES ...]] [--loc LOC]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt --loc} & genome location to plot specified as {\tt chrom:start-end}
  \end{APIargs}

  {\bf plot\_segs\_agg}

  \APIentry{Description}{Plot CNV seg profiles for several samples}
  \begin{APIverbatim}{Usage}
       [ngsSH] plot_segs_agg [-i DIR_IN] [-o DIR_OUT]
                             [-b BIN_SIZES [BIN_SIZES ...]]
                             [-L LAMTHA [LAMTHA]] [--loc LOC]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
      {\tt --loc} & genome location to plot specified as {\tt chrom:start-end}
  \end{APIargs}

  {\bf plot\_bins\_segs\_agg}

  \APIentry{Description}{Plot CNV seg and bin profiles for several samples}
  \begin{APIverbatim}{Usage}
       [ngsSH] plot_bins_segs_agg [-i DIR_IN] [-o DIR_OUT]
                                  [-b BIN_SIZES [BIN_SIZES ...]]
                                  [-L LAMTHA] [-c CUTOFF] [--loc LOC]
                                  [--genes]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
      {\tt -c, --cutoff} & log2 copy ratio threshold for segment significance [0.3] \\
      {\tt --loc} & genome location to plot specified as {\tt chrom:start-end} \\
      {\tt --genes} & plot with hg19 gene annotation
  \end{APIargs}

  {\bf cnv2igv}

  \APIentry{Description}{Convert CNV files into IGV-viewable cbs file}
  \begin{APIverbatim}{Usage}
       [ngsSH] cnv2igv [-i DIR_IN] [-o DIR_OUT]
                       [-b BIN_SIZES [BIN_SIZES ...]]
                       [-L LAMTHA [LAMTHA ...]]
  \end{APIverbatim}
  \begin{APIargs}{optional arguments}
      {\tt -i, --dir\_in} & directory in which to search for files [{\tt bam/}] \\
      {\tt -o, --dir\_out} & directory in which to save files [{\tt bam/}] \\
      {\tt -b, --bin\_sizes} & bin sizes for which to calculate MAPD scores [100] \\
      {\tt -L, --lamtha} & smoothing factor(s) [3] \\
  \end{APIargs}

\end{document}
