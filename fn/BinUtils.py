# BinUtils.py - Methods for handling executable files
#
# v 0.5.2
# rev 2016-07-20 (MS: general bin cmd generation method)
# Notes: clean-up

import pathlib2 as pathlib

import FileUtils
import CmdUtils

def Bin_IO_cmd(file_input, file_output, **kwargs):
    """ Build command for any Rscript requiring an input (-i) and an out (-o) option
    """
    path = FileUtils.get_cmd_path('bin')

    try:
        cmd = "%s/%s -i %s -o %s" %(path, kwargs.pop('script'), file_input, file_output)

    except KeyError:
        raise TypeError("kwargs must include a keyword: 'script'")

    cmd = CmdUtils.build_cmd_args(cmd, **kwargs)

    return cmd

def Bin_cmd(*args, **kwargs):
    """ Build command for any bin script. 
        *args are positional arguments
        **kwargs are keyword arguments
    """
    path = FileUtils.get_cmd_path('bin')

    try:
        cmd = "%s/%s" %(path, kwargs.pop('script'))

    except KeyError:
        raise TypeError("kwargs must include a keyword: 'script'")

    cmd = CmdUtils.build_cmd_args2(cmd, *args, **kwargs)

    return cmd
