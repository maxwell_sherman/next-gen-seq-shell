# RUtils.py - Methods for handling R constructs
#
# v 0.4.5a
# rev 2016-07-06 (MS: cmd args set using CmdUtils
# Notes: clean-up

import pathlib2 as pathlib

import FileUtils
import CmdUtils

def Rscript_IO_cmd(file_input, file_output, **kwargs):
    """ Build command for any Rscript requiring an input (-i) and an out (-o) option
    """
    path = FileUtils.get_cmd_path('r')

    try:
        cmd = "Rscript %s/%s -i %s -o %s" %(path, kwargs.pop('rscript'), file_input, file_output)

    except KeyError:
        raise TypeError("kwargs must include a keyword: 'rscipt'")

    cmd = CmdUtils.build_cmd_args(cmd, **kwargs)

    return cmd
