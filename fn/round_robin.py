# round_robin.py - Round robin parallel distribution of commands for local and server use.
#
# v 0.5.1
# rev 2016-07-18 (MS: exec_cmd optionally suppresses stderr)
# Notes:

import multiprocessing as mp
import logging
import subprocess
import argparse
import sys
import ast

def squared(x):
    return x*x

def exec_cmd(cmd, verbose=0, suppress_error=False):
    """ Execute a system command
    """
    if verbose >= 2:
        print "\nExecuting: %s\n" %cmd

    # Need to read stdout otherwise fnc returns and subprocess runs in background
    # A subprocess running outside this function REALLY messes up mp.Pool()
    if suppress_error:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read()

    else:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    # p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)

    if verbose:
        print p
        # print p.stdout.read()

    return p

    # output, error = p.communicate()
    # logging.info(output)

def round_robin(cmd_list, n_procs=0, runtype='parallel', verbose=0):
    """ Distribute commands in parallel across avialbel processors

        Input:
            cmd_list    list        list of commands to be executed
            n_procs     int         number of processors to use
            runtype     str         runtype: parallel or serial

        Output:
            << system execution of commands >>
    """

    # if n_procs is zero, set it to none so mp.Pool() uses all procs on machine
    if not n_procs:
        n_procs = None

    # Distribute commands across all processors
    if runtype == 'parallel':
        # mp.log_to_stderr(logging.DEBUG)

        pl = mp.Pool(processes=n_procs)

        for cmd in cmd_list:
            pl.apply_async(exec_cmd, (cmd, verbose,))

        pl.close()
        pl.join()

    # Execute commands one at a time
    elif runtype == 'serial':
        for cmd in cmd_list:
            exec_cmd(cmd, verbose)

    elif runtype == 'print':
        for cmd in cmd_list:
            print cmd

    else:
        raise TypeError('Uknown runtype %s' %runtype)

def test(vals, n_procs=None):
    """ mp.Pool() testing
    """
    A = []
    pl = mp.Pool(processes=n_procs)

    for x in vals:
        res = pl.apply_async(squared, (x,), callback=A.append)
        print res.get()

    print A

    pl.close()
    pl.join()

def run():
    """ For running round_robin.py as a command line script
        Requires command line input (defined with argparse below)
    """
    # # Set up command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n_procs', default=0, type=int, help='Number of processors for execution')
    parser.add_argument('-E', '--exec_type', default='parallel', choices=['serial', 'parallel'], help='execution runtype')
    parser.add_argument('-V', '--verbose', default=1, action='count', help='verbosity of execution')
    parser.add_argument('cmd_file', type=argparse.FileType('r'), help='file of commands to be executed')
    p = parser.parse_args()

    cmd_list = p.cmd_file.read().split('\n')
    round_robin(cmd_list, p.n_procs, p.exec_type, p.verbose)

if __name__ == "__main__":
    run()
