# perlUtils.py - Methods for handling perl constructs
#
# v 0.6.9
# rev 2016-11-22 (MS: more permanent fix for map and fa file locations)
# Notes:

import pathlib2 as pathlib

import BamUtils
import FileUtils
import CmdUtils
import arg_parser

def perl_IO_cmd(file_in, file_out, **kwargs):
    path = FileUtils.get_cmd_path('perl')

    try:
        cmd = "perl %s/%s" %(path, kwargs.pop('perlscript'))

    except KeyError:
        raise TypeError("kwargs must include a keyword: 'perlscript'")

    cmd = CmdUtils.build_cmd_args(cmd, **kwargs)

    cmd += " %s %s" %(file_in, file_out)

    return cmd

def bic_seq_normalize_config(map_files, out_files, config_name, build, read_length, species='human'):
    chrms = BamUtils.get_chrm_numbers(species)
    chrm_labels = ['chr%s' %x for x in chrms]

    conf = arg_parser.load_config()
    path_species = pathlib.Path(conf.get("scripts", "map_to_bins")).parent / ("%s" %build)

    list_fasta = [str(path_species / ("fafiles/%s.fa" %chrm)) for chrm in chrm_labels]
    list_map = [str(path_species / ('CRG_%imer_Mappability/chrbychr/%s.%imer.CRG.%s.txt' %(read_length, build, read_length, chrm))) for chrm in chrm_labels]
    # list_fasta = ['/home/mas138/orchestra/sandbox/Install/NBICseq-norm_v0.2.4/%s/fafiles/%s.fa' %(build, chrm) for chrm in chrm_labels]
    # list_map = ['/home/mas138/orchestra/sandbox/Install/NBICseq-norm_v0.2.4/%s/CRG_%imer_Mappability/chrbychr/%s.%imer.CRG.%s.txt' %(build, read_length, build, read_length, chrm) for chrm in chrm_labels]

    f = file(str(config_name), 'w')
    f.write('\t'.join(['chrom_name', 'fa_file', 'mappability', 'readPosFile', 'bin_file_normalized', '\n']))

    for chrm, fasta, mmap, f_in, f_out in zip(chrm_labels, list_fasta, list_map, map_files, out_files):
        f.write('\t'.join([chrm, fasta, mmap, str(f_in), str(f_out), '\n']))

    f.close()

def BICseq_config_single(bin_files, config_name, control_files=None, species='human'):
    chrms = BamUtils.get_chrm_numbers(species)
    chrm_labels = ['chr%s' %x for x in chrms]

    f = file(str(config_name), 'w')

    if control_files:
        f.write('\t'.join(['chrom', 'sample', 'control', '\n']))

        for chrm, f_bin, f_control in zip(chrm_labels, bin_files, control_files):
            f.write('\t'.join([chrm, str(f_bin), str(f_control), '\n']))

    else:
        f.write('\t'.join(['chrom', 'sample', '\n']))

        for chrm, f_bin in zip(chrm_labels, bin_files):
            f.write('\t'.join([chrm, str(f_bin), '\n']))

    f.close()

def BICseq_config_multi(bin_files, config_name, species='human'):
    chrms = BamUtils.get_chrm_numbers(species)
    chrm_labels = ['chr%s' %x for x in chrms]

    f = file(str(config_name), 'w')

    # Build and write header
    header = ['chrom']
    header.extend(['sample_%i' %i for i, _ in enumerate(bin_files[chrm_labels[0]])])
    header.extend('\n')

    f.write('\t'.join(header))

    # Build and write lines
    for chrm in chrm_labels:
        line = [chrm]
        line.extend([str(path) for path in bin_files[chrm]])
        line.extend('\n')

        f.write('\t'.join(line))

    f.close()
