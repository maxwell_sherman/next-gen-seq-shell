# cli.py - commands & runtime for [n]ext [g]eneration [s]equencing [SH]ell
#
# v 0.6.9
# rev 2016-11-22 (MS: do_PCRqc)
# Notes: 

# Standard Modules
import os
import argparse
import traceback
import pathlib2 as pathlib
from cmd import Cmd
import subprocess

# Local Modules
import clidefs
import arg_parser
import FileUtils
import round_robin as rr
from decorators import parse_args

class ngsSH(Cmd):
    def __init__(self, dproj):
        Cmd.__init__(self)

        self.prompt = '\033[93m'+'[ngsSH] ' + '\033[0m'
        self.intro = "\n[n]ext [g]eneration [s]equencing [SH]ell\n"
        self.f_history = '.ngssh_hist_local'

        dproj = pathlib.Path(dproj)

        # Resolve any symbolic links
        self.path = {
            'dproj': dproj.resolve(),
            'cwd': dproj.resolve(),
        }
        print("\nYour root project directory is %s" %self.path['dproj'])

        self.__update_ls()
        self.__load_parsers()
        self.__load_config()

    def do_test_parse(self, args):
        print(args.split())
        p = self.parser['do_test_parse'].parse_args(args.split())
        print(p)

    @parse_args
    def do_cd(self, args):
        """Change current working directory
           [ngsSH] cd -h for more help
        """
        # Parse arguments
        self.path['cwd'] = FileUtils.change_dir(args.path, self.path['cwd'], self.path['dproj'])

        # Update ls and print new working directory
        self.__update_ls()
        print(self.path['cwd'])

    def complete_cd(self, text, line, begidx, endidx):
        """Autocomplete for cd command.
           Limited to dirs in current working directory
        """
        if not text:
            completions = self.ls['dlist'][:]

        else:
            completions = [d for d in self.ls['dlist'] if d.startswith(text)]
            # if len(completions) == 1:
            #     self.__update_ls(os.path.join(self.path['cwd'], completions[0]))

        return completions

    @parse_args
    def do_pwd(self, args):
        """Print current working directory
        """
        print(self.path['cwd'])

    @parse_args
    def do_ls(self, args):
        """Lists contents of current working directory
           Usage: [datash] ls
        """
        self.__update_ls()

        if self.ls['dlist']:
            FileUtils.prettyprint(self.ls['dlist'])

        if self.ls['flist']:
            FileUtils.prettyprint(self.ls['flist'])

    @parse_args
    def do_bjobs(self, args):
        """ List current jobs running on orchestra
            [ngsSH] bjobs -h for usage
        """
        rr.exec_cmd('bjobs', verbose=1)

    @parse_args
    def do_bkill(self, args):
        """ Kill job with id <job_id>
            [ngsSH] bkil <job_id>
        """
        rr.exec_cmd('bkill %i' % args.job_id, verbose=1)

    @parse_args
    def do_njobs(self, args):
        """ Display number of running and pending jobs
            [ngsSH] njobs
        """
        clidefs.exec_njobs()

    @parse_args
    def do_vi(self, args):
        """ View a file using vim
            [ngsSH] vi <path/to/file>
        """
        path = self.path['cwd'] / args.file
        subprocess.call(['vim', str(path)])

    @parse_args
    def do_bam_by_chrm(self, args):
        """ Separates a bam file into 24 bam files, one per chromosome
            [ngssh] bam_by_chrm -h for usage
        """
        clidefs.exec_bam_by_chrm(self.path['cwd'], args)

    @parse_args
    def do_bamqc(self, args):
        """ Run qualimap bamqc on bam files
            [ngsSH] bamqc for usage
        """
        clidefs.exec_bamqc(self.path['cwd'], args)

    @parse_args
    def do_fastqc(self, args):
        """ Run fastqc on fastq.gz files
            [ngsSH] fastqc for usage
        """
        clidefs.exec_fastqc(self.path['cwd'], args)

    @parse_args
    def do_PCRqc(self, args):
        """ Use CNV files to detect presence of PCR noise
            [ngsSH] PCRqc -h for usage
        """
        clidefs.exec_PCRqc(self.path['cwd'], args)

    @parse_args
    def do_bam_to_depth(self, args):
        """ Get depth information from bam files
            [ngssh] bam_to_depth -h for usage
        """
        clidefs.exec_bam_to_depth(self.path['cwd'], args)

    @parse_args
    def do_bam_to_mappability(self, args):
        """ Get uniquely mapped positions from a bam file
            [ngsSH] bam_to_mappability -h for usage
        """
        clidefs.exec_bam_to_mappability(self.path['cwd'], args)

    @parse_args
    def do_bam_to_mapd(self, args):
        """ Calculate mapd score from bam file
            [ngsSH] bam_to_mapd -h for usage
        """
        clidefs.exec_bam_to_mapd(self.path['cwd'], args)

    @parse_args
    def do_bam_downsample(self, args):
        """ downsample bam file to a desired depth
            [ngsSH] bam_downsample -h for usage
        """
        clidefs.exec_bam_downsample(self.path['cwd'], args)

    @parse_args
    def do_bam_avg_depth(self, args):
        """ Get average coverage of a bam file
            [ngssh] bam_avg_coverage -h for usage
        """
        clidefs.exec_bam_avg_depth(self.path['cwd'], args)

    @parse_args
    def do_map_to_bins(self, args):
        """ Calulate normalized bins based on mapped reads
            [ngsSH] map_to_bins -h for usage
        """
        clidefs.exec_map_to_bins(self.path['cwd'], args)

    @parse_args
    def do_bins_merge(self, args):
        """ Merge adjacent bins in a bin file to make file of larger bin size
            [ngsSH] bin_merge -h for usage
        """
        clidefs.exec_bins_merge(self.path['cwd'], args)

    @parse_args
    def do_bins_to_mapd(self, args):
        """ Calculate mapd score from bin files
            [ngsSH] bins_to_mapd -h for usage
        """
        clidefs.exec_bins_to_mapd(self.path['cwd'], args)

    @parse_args
    def do_bins_to_cnvs(self, args):
        """ Call copy number variations using specified bins
            [ngsSH] bins_to_cnvs -h for usage
        """
        clidefs.exec_bins_to_cnvs(self.path['cwd'], args)

    @parse_args
    def do_cnv_postproc(self, args):
        """ Postprocessing for cnv calls. Includes plotting and gene annotation.
            [ngsSH] cnv_postproc -h for usage
        """
        clidefs.exec_cnv_postproc(self.path['cwd'], args)

    @parse_args
    def do_cnv_annotate(self, args):
        """ Annotate cnv calls
            [ngsSH] cnv_annotate -h for usage
        """
        clidefs.exec_cnv_annotate(self.path['cwd'], args)

    @parse_args
    def do_cnv_post_pipeline(self, args):
        """ Postprocessing for cnv calls. Includes plotting and gene annotation.
            [ngsSH] cnv_postproc -h for usage
        """
        clidefs.exec_cnv_post_pipeline(self.path['cwd'], args)

    @parse_args
    def do_cnv_seg_aggregate(self, args):
        """ Produces a table of all significant CNV segments and the number of subjects sharing each call
            [ngsSH cnv_seg_aggregate -h for usage
        """
        clidefs.exec_cnv_seg_aggregate(self.path['cwd'], args)

    @parse_args
    def do_cnv_mapd_correlate(self, args):
        """ Correlated CNV calls and MAPD scores. Saves a 3x3 plot of correlations and histograms
            [ngsSH] cnv_mapd_correlated -h for usage
        """
        clidefs.exec_cnv_mapd_correlate(self.path['cwd'], args)

    @parse_args
    def do_cnv2igv(self, args):
        """ Convert cnv files into IGV seg files for use with IGV
            [ngsSH] cnv2igv -h for usage
        """
        clidefs.exec_cnv2igv(self.path['cwd'], args)

    @parse_args
    def do_cnv_normalize(self, args):
        """ Normalize cnv files and save normalization
            [ngsSH] cnv_normalize -h for usage
        """
        clidefs.exec_cnv_normalize(self.path['cwd'], args)

    @parse_args
    def do_plot_bins_agg(self, args):
        """ plot bin files for several samples in one fig
            [ngsSH] plot_bins_agg -h for usage
        """
        clidefs.exec_plot_bins_agg(self.path['cwd'], args)

    @parse_args
    def do_plot_bins_segs_agg(self, args):
        """ plot background bins and segs for several samples on one fig
            [ngsSH] plot_bins_segs_agg -h for usage
        """
        clidefs.exec_plot_bins_segs_agg(self.path['cwd'], args)

    @parse_args
    def do_plot_segs_agg(self, args):
        """ plot seg heatmaps for several samples on one fig.
            [ngsSH] plot_bins_segs_agg -h for usage
        """
        clidefs.exec_plot_segs_agg(self.path['cwd'], args)

    @parse_args
    def do_figure_glob(self, args):
        """ Recursively find all figures of a given type and move to a single directory
            [ngsSH] image_glob -h for usage
        """
        clidefs.exec_figure_glob(self.path['cwd'], args)

    @parse_args
    def do_depth_to_pgram(self, args):
        """ Calculate periodigram from depth information
            [ngsSH] depth_to_prgam -h for usage
        """
        clidefs.exec_depth_to_pgram(self.path['cwd'], args)

    def do_EOF(self, args):
        """Ctrl-D exits shell
        """
        # Parse arguments
        # p = self.parser['do_EOF'].parse_args(args.split())

        return True

    @parse_args
    def do_exit(self, args):
        """Exits from the console
        """
        return -1

    @parse_args
    def do_help(self, args):
        """Get help on commands
           'help' or '?' with no arguments prints a list of commands for which help is available
           'help <command>' or '? <command>' gives help on <command>
            '<command> -h' gives specific usage for <command>
        """
        ## The only reason to define this method is for the help text in the doc string
        Cmd.do_help(self, args.fnc)

    # Tasks before entering Cmd loop
    def preloop(self):
        """Initialization before prompting user for commands.
           Despite the claims in the Cmd documentaion, Cmd.preloop() is not a stub.
        """
        # Sets up command completion
        Cmd.preloop(self)

        # Load history
        self._hist = self.__load_history()

        # Initialize execution namespace for user
        self._locals  = {}
        self._globals = {}

    # Tasks exiting Cmd loop
    def postloop(self):
        """Take care of any unfinished business.
           Despite the claims in the Cmd documentaion, Cmd.postloop() is not a stub.
        """
        
        # Write history
        self.__write_history()

        # Clean up command completion
        Cmd.postloop(self)
        print("Exiting...")

    # Before command execution
    def precmd(self, line):
        """ This method is called after the line has been input but before
            it has been interpreted. If you want to modify the input line
            before execution (for example, variable substitution) do it here.
        """
        self._hist += [ line.strip() ]
        return line

    # After command execution
    def postcmd(self, stop, line):
        """If you want to stop the console, return something that evaluates to true.
           If you want to do some post command processing, do it here.
        """
        return stop

    # What to do for an empty line
    def emptyline(self):    
        """Do nothing on empty input line"""
        pass

    # What to do for unrecognized input
    def default(self, line):       
        """Called on an input line when the command prefix is not recognized.
           In that case we execute the line as Python code.
        """
        Cmd.default(self, line)
        # try:
        #     exec(line) in self._locals, self._globals
        # except Exception, e:
        #     print e.__class__, ":", e

    def completedefault(self, text, line, begidx, endidx):
        """Default autocomplete
           Default is to return list of dirs and fils
        """
        potentials = self.ls['dlist'] + self.ls['flist']

        if not text:
            completions = potentials

        else:
            completions = [p for p in potentials if p.startswith(text)]

        return completions

    # Main loop
    def cmdloop(self, intro='\n[n]ext [g]eneration [s]equencing [SH]ell\n'):
        try:
            Cmd.cmdloop(self, intro)

        except KeyboardInterrupt:
            print("^C")
            self.cmdloop('')

        except Exception as e:
            print("Oh no, and error")
            traceback.print_exc()
            # print e
            self.cmdloop('')

        except:
            self.cmdloop('')

    # Load argument parsers
    def __load_parsers(self):
        self.parser = arg_parser.load_parsers()

    def __load_config(self):
        self.config = arg_parser.load_config()

    # Function to read the history file
    def __load_history(self):
        with open(self.f_history) as f_in:
            lines = (line.rstrip() for line in f_in)
            lines = [line for line in lines if line]

        return lines

    # function to write the history file
    def __write_history(self):
        with open(self.f_history, 'w') as f_out:
            for line in self._hist[-10000:]:
                f_out.write(line+'\n')

    # update the list of directory contents
    def __update_ls(self, path=None):
        self.ls = {}

        if not path:
            path = self.path['cwd']

        if path.is_dir():
            self.ls['dlist'] = sorted([d.name+'/' for d in path.iterdir() if d.is_dir()])
            self.ls['flist'] = sorted([f.name for f in path.iterdir() if d.is_file()])

        else:
            raise OSError("The current working directory does not exist.")
