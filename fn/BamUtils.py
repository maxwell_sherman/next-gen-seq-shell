# BamUtils.py - commands for handling bam files
#
# v 0.6.0
# rev 2016-07-25 (MS: merged cmdGeneric into master)
# Notes:

import pathlib2 as pathlib

import round_robin as rr

def get_chrm_numbers(species='human'):
    # chrms = [1, 2]
    if species == 'human':
        chrms = range(1, 23)
        chrms.extend(['X', 'Y'])

    elif species == 'mouse':
        chrms = range(1, 20)
        chrms.extend(['X', 'Y'])

    else:
        raise TypeError("species %s not recognized." %species)

    return chrms

def get_chrm_labels(chrms):
    labels = ['chrm_%02d' %x if isinstance(x, int) else 'chrm_%s' %x for x in chrms]

    return labels

def bam_by_chrm(file_bam, file_out_prefix, species='human'):
    """ Generate command to separate a bam file by chromosome number

        Input:
            file_bam:           pathlib Path    path to bam file
            file_out_prefix:    pathlib Path    common prefix of all 24 output files

        Output:
            cmd_list:           list            list of commands to separate bam file into chromosomes
                                                length: 24 (22 autosomal chrms, 2 sex chrms)
    """

    # Chromosome numbers and labels
    chrms = get_chrm_numbers(species)
    chrms_labels = get_chrm_labels(chrms)

    # Generate commands
    cmd_list = []

    list_f_out = [file_out_prefix.with_suffix('.%s.bam' %label) for label in chrms_labels]

    for f_out, chrm in zip(list_f_out, chrms):
        cmd_list.append('samtools view -b -h ' + str(file_bam) + ' %s' %chrm + ' -o ' + str(f_out))

    return cmd_list

def bam_to_depth(file_bam, file_out, all_locs=True):
    """ Generate command to get depth information from a bam file

        Input:
            file_bam:               pathlib Path    path to bam file
            file_out:               pathlib Path    path to output file
            all_locs (optional):   boolean         include depth at all locations, even those with no reads

        Ouput:
            cmd:                    str             command to perform depth extraction
    """

    # Base command
    cmd = "samtools depth "

    # Append -a to get depth at all locations, even those without reads
    if all_locs:
        cmd += "-a "

    #  Add input and output files
    cmd += str(file_bam) + " > " + str(file_out)

    return cmd

def bam_to_mappability(file_in, file_out, chrom=None):
    """ Generate command to get depth information from a bam file

        Input:
            file_bam:               pathlib Path    path to bam file
            file_out:               pathlib Path    path to output file

        Ouput:
            cmd:                    str             command to perform depth extraction
    """
    cmd = "samtools view -q 30 %s" %str(file_in)

    if chrom:
        cmd += " %s" %chrom

    cmd += " | perl -ane 'print $F[3], \"\\n\";'"
    # cmd += " | grep 'XT:A:U' | perl -ane 'print $F[3], \"\\n\";'"
    cmd += " > %s" %file_out

    return cmd

#NOTE: DEPRECATED - WILL BE REMOVED IN FUTURE
def bam_to_mapd(file_in, file_out):
    """ Generate cmd to calculate mapd scores
    
        Input:
            file_in             pathlib path    path to bam file
            file_out            pathlib path    base path to out files
                                                (mapd script adds additional suffixes to this path) 

        Output:
            cmd:                str             command to calculate mapd scores
    """
    cmd = str(pathlib.Path.cwd() / 'bin' / 'mapd') + " %s %s" %(file_in, file_out)
    return cmd

def average_coverage(file_bam, file_out=None):
    """ Generate command to get average coverage of a bam file

        Input:
            file_bam:               pathlib Path    path to bam file
            file_out (optional):    pathlib Path    path to output file (optional)
                                                    default is to print stdout

        Ouput:
            cmd:                    str             command to get average coverage of a bam file 
    """
    # Get total reads in Bam file
    cmd = "samtools idxstats %s | tail -n 2 | head -n 1 | awk \'{print $3}\'" % str(file_bam)
    read_total = int(rr.exec_cmd(cmd))

    # Get read length
    cmd = "samtools view %s | head -n 100000 | awk '{sum+=length($10)} END{print sum/NR}'" % str(file_bam)
    read_length = round(float(rr.exec_cmd(cmd, suppress_error=True)))

    # Cmd to calculate depth
    cmd = "python -c \"print %i. * %i. / 3095693981.\"" %(read_total, read_length)
    # cmd = "expr %i \\* %i / 3095693981" %(read_total, read_length)
    # cmd = "samtools depth -a " + str(file_bam)

    # Add output file if necessary
    if file_out:
        cmd += " > " + str(file_out)

    return cmd

if __name__ == "__main__":
    f_bam = pathlib.Path('/path/to/file.bam')
    f_out = pathlib.Path('/path/to/file.out')

    cmd = bam_to_depth(f_bam, f_out)
    print cmd

    cmd = average_coverage(f_bam, f_out)
    print cmd
