# FileUtil.py - general file input/output utilities
#
# v 0.6.3
# rev 2016-08-15 (MS: species determination now includes grc)
# Notes: 

import pathlib2 as pathlib
import itertools as it
import ConfigParser
import re

import arg_parser

# Print any iterable
def prettyprint(l):
    for item in l:
        print '%s' %item

def check_dir_in(directory):
    """ Check directory for existence. Raise error if it does not exist
    """
    if not directory.is_dir():
        raise IOError('input directory does not exist')

def check_dir_out(directory):
    """ Check direcctory for existence. If not, create it.
        Parents are created as necessary
    """
    if not directory.is_dir():
        directory.mkdir(parents=True)

def dir_path_build_and_check(root_path, dir_in, dir_out):
    """ Build path to input dir and output dir and check for existence of each
    """ 
    # Build paths
    d_in  = root_path / dir_in
    d_out = root_path / dir_out

    # Check for existence
    check_dir_in(d_in)
    check_dir_out(d_out)

    return d_in, d_out 

def dir_mirror_structure(list_files, dir_in, dir_out, suffix_output):
    """ Create output files with a directory structure that mirrors that of the input directory
    """
    # Input paths beyond dir_in
    input_paths = [pathlib.Path(str(f).split(dir_in.name+'/')[-1]) for f in list_files]

    # Complete output paths
    output_paths = [dir_out / path.with_suffix(suffix_output) for path in input_paths] 

    # Create necessary subdirs in output directory
    for path in output_paths:
        check_dir_out(path.parent)

    return output_paths

def dir_by_samples(path_list, dir_out):
    """ Input:
            path_list       list            paths from which to extract samples
            dir_out         pathlib path    path to output directory

        Output:
            list_dir_out    list            pathlib paths to subject specific output dirs 
            subjects        list            strs of subjects names
    """

    # Setting an entirely new directory (from project directory)
    # Get subject prefixes (one per bam file)
    subjects = get_prefixes(path_list)

    # Create subdirs for each subject
    list_dir_out = []

    for subject in subjects:
        check_dir_out(dir_out / subject)
        list_dir_out.append(dir_out / subject)

    return list_dir_out, subjects

def add_subdir(sub_dir, file_list):
    """ Add a subdirectory new subdirectory to every file in the list
    """
    file_list_new = []

    for f in file_list:
        fname = f.name
        parent = f.parent

        new_parent = parent / sub_dir
        check_dir_out(new_parent)

        file_list_new.append(new_parent / fname)

    return file_list_new
        
# Get file prefixes (name before extension)
def get_prefixes(list_dir):
    return [f.stem for f in list_dir if f.is_file()]

def change_dir(path_new, path_old, root='/'):
    """ Input:
            path_new:  str,             new path to change to. Can be relative or absolute
            path_old:  pathlib Path,    Current path.
            root:      pathlib Path,    root path

        Output:
            path:      pathlib Path,    new path 
    """

    # Setting an entirely new directory (from project directory)
    if path_new.startswith('/'):
        path = root / path_new.lstrip('/')

    # Going back in the tree structure
    elif path_new.startswith('..'):
        path = path_old / path_new
        path = path.resolve()

        if str(root) not in str(path):
            print("Cannot go back farther than root directory. Defaulting to that")
            path = root

    # Going forward in the tree structure
    else:
        path = path_old / path_new

    # Ensure new path is actually a directory
    if path.is_dir():
        return path
    else:
        print("%s is not a directory. Reverting to old path" %str(path))
        return path_old

def file_glob(directory, file_ext, pattern=[''], invert_pattern=['a^'], recursive=False):
    """ Return files with a given prefix and matching an (optional) pattern

        Input:
            directory:  pathlib Path    directory to search
            file_ext:   str             file extension to match
            pattern:    str             additional pattern to match
            recursive:  boolean         search recursively (BEWARE: if true, searches all directories
                                        and subdirectories - maybe change this at some point)

        Output:
            file_list: list             all files as pathlib Paths matching extension and pattern
    """

    # Build pattern to match to
    match = '*' + file_ext

    # if pattern:
    #     match = '*' + pattern + match

    if recursive:
        match = '**/' + match

    # Look for files matching pattern and excluding invert pattern
    file_list = sorted([f for f in directory.glob(match) if f.is_file()])
    file_list = [f for f in file_list if check_pattern(f.name, pattern, invert_pattern)]

    # Remove files matching invert_pattern
    # if invert_pattern:
    #     file_list = [f for f in file_list if invert_pattern not in f.name]

    # Make sure at least one file was matched before returning. Otherwise, complain
    if file_list:
        return file_list

    else:
        raise IOError('No files found matching pattern %s' %match)

def dir_glob(directory, dir_name=None, pattern=[''], invert_pattern=['a^'], recursive=False):
    """ Return files with a given prefix and matching an (optional) pattern

        Input:
            directory:  pathlib Path    directory to search
            dir_name:   str             name of directory to match
            pattern:    str             additional pattern to match
            recursive:  boolean         search recursively (BEWARE: if true, searches all directories
                                        and subdirectories - maybe change this at some point)

        Output:
            file_list: list             all files as pathlib Paths matching extension and pattern
    """

    # Build pattern to match to
    match = '*'

    if dir_name:
        match += dir_name

    # if pattern:
    #     match = '*' + pattern + match

    if recursive:
        match = '**/' + match

    # Look for files matching pattern
    dir_list = sorted([d for d in directory.glob(match) if d.is_dir()])
    dir_list = [d for d in dir_list if check_pattern(str(d), pattern, invert_pattern)]
    # dir_list = [d for d in dir_list if check_pattern(d.name, invert_pattern=invert_pattern)]
    # dir_list = [d for d in dir_list if pattern in str(d)]
    # dir_list = [d for d in dir_list if not re.search(not_pattern, d.name)]

    # Make sure at least one file was matched before returning. Otherwise, complain
    if dir_list:
        return dir_list

    else:
        raise IOError('No dirs found matching pattern %s in %s' %(match, directory))

def get_sub_dirs(directory, pattern=[''], invert_pattern=['a^']):
    try:
        sub_dirs = dir_glob(directory, pattern=pattern, invert_pattern=invert_pattern)

    except IOError:
        sub_dirs = [directory]

    return sub_dirs

def check_pattern(string, pattern=[''], invert_pattern=['a^']):
    """ Regexpr check a path name for pattern(s) or for lack of pattern(s)

        Input:
            string          str        generic string
            pattern         list       list of patterns to include
            invert_pattern  list       list of patterns to exclude

        Returns:
            --              bool       True if contains pattern(s)
                                       False if contains invert_pattern(s)    
    """
    # Check for patterns to exclude
    if any([re.search(p, string) for p in invert_pattern]):
        return False

    # Check for patterns to include
    if any([re.search(p, string) for p in pattern]):
        return True

    else:
        return False

def match_bin_dirs(dir_bin, list_bins, pattern=[''], invert_pattern=['a^']):
    list_bin_dirs = []

    for bin_size in list_bins:
        list_bin_dirs.extend(dir_glob(dir_bin, 'b%d' %bin_size, pattern=pattern, 
                                      invert_pattern=invert_pattern, recursive=True))

    return(list_bin_dirs)

def match_seg_files(dir_seg, list_bins, list_lamtha, suffix='BICseq.out', pattern=[''], invert_pattern=['a^']):
    list_seg_files = []
    lamthas = []
    bins = []

    for bin_size, lamtha in it.product(list_bins, list_lamtha):
        list_f = file_glob(dir_seg, 'B%03d_l%02d.%s' %(bin_size, lamtha, suffix), pattern=pattern, invert_pattern=invert_pattern, recursive=True)
        list_seg_files.extend(list_f)
        lamthas.extend([lamtha for f in list_f])
        bins.extend([bin_size for f in list_f])

    return list_seg_files, bins, lamthas

def bin_dirs_from_seg_files(seg_files, dir_bin, list_bins):
    list_bin_dirs = []
    list_pattern = [f.name.split('__')[0] for f in seg_files]

    list_dirs = [match_bin_dirs(dir_bin / pattern, [bin_size]) for pattern, bin_size in zip(list_pattern, list_bins)]
    list_bin_dirs = [d for dirs in list_dirs for d in dirs]

    return list_bin_dirs

def iter_name(directory, base_name, suffix='.sh'):
    """ Iterate a base name to ensure file name is unique
    """
    check_dir_out(directory)

    i = 1

    while True:
        fname = directory / (base_name + '_%03d' %i + suffix)
        i += 1

        if not fname.exists():
            break

    return fname

def save_txt(fname, text):
    f = file(str(fname), 'w')
    f.write(text)
    f.close()

def species_from_build(build):
    if build.startswith('hg') or build.startswith('grc'):
        species = 'human'

    elif build.startswith('mm'):
        species = 'mouse'

    else:
        raise TypeError('build %s not recognized' %build)

    return species

def get_cmd_path(exec_type):
    path = arg_parser.load_config('paths')[exec_type]
    path = pathlib.Path(path).resolve()

    return str(path)

def language_from_script(script):
    """ Get langauge binary from scipt
        script must be a pathlib object
    """
    try:
        lang = arg_parser.load_config().get('languages', script.suffix)

    except ConfigParser.NoOptionError:
        lang = ''

    return lang
 
if __name__ == "__main__":
    directory = pathlib.Path('/home/mas138/orchestra/data/BSM/lowpass/FC_01846')
    directory = directory.resolve()
    file_ext = ".bam"
    pattern = "S59_L002"
    recursive = True

    flist = file_glob(directory, file_ext, pattern, recursive)
    print(flist)
    print(len(flist))
