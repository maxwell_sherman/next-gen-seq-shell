# arg_parser.py - general file input/output functions
#
# v 0.6.9
# rev 2016-11-22 (MS: parser for PCRqc)
# Notes: changed default output dir for cnv_postproc

import argparse
import ConfigParser

def load_parsers():
    VERSION = '0.0.1'
    parser = {}
    
    parser['argparse'] = argparse.ArgumentParser()
    parser['argparse'].add_argument('dproj', help='root directory of the project')
    parser['subparser'] = parser['argparse'].add_subparsers(title="Internal Commands", dest="subcommand", metavar='')

    ## Empty subcommand for entry into ngsSHell
    parser['shell'] = parser['subparser'].add_parser('shell')

    ## do_cd
    parser['do_cd'] = parser['subparser'].add_parser('cd', help='change directory')
    parser['do_cd'].add_argument('path', type=str, help='Path to new directory. Must be subdirectory of root project')

    ## do_ls (no args, but provides help string)
    parser['do_ls'] = parser['subparser'].add_parser('ls', help='list contents of directory')

    ## do_pwd (no args, but provides help string)
    parser['do_pwd'] = parser['subparser'].add_parser('pwd', help='prints current working directory')

    ## do_bjobs (no args, but provides help string)
    parser['do_bjobs'] = parser['subparser'].add_parser('bjobs', help='prints current jobs submitted to orchestra')

    ## do_bkill
    parser['do_bkill'] = parser['subparser'].add_parser('bkill', help='kill job with specified <job_id>')
    parser['do_bkill'].add_argument('job_id', type=int, help='id of job to kill')

    ## do_bkill
    parser['do_njobs'] = parser['subparser'].add_parser('njobs', help='Print number of running and pending jobs')

    ## do_vi
    parser['do_vi'] = parser['subparser'].add_parser('vi', help='view file using vim')
    parser['do_vi'].add_argument('file', type=str, help='file to open')

    ## do_help
    parser['do_help'] = parser['subparser'].add_parser('help', help='Get help for functions')
    parser['do_help'].add_argument('fnc', nargs='?', default='', help='Function name to get help on. Leave blank for list of all documented fncs.')

    ## do_EOF (no args, but provides help string)
    # parser['do_EOF'] = parser['subparser'].add_parser('EOF', help='Ctrl-D exits shells')

    ## do_exit parser (no args, but provides help string)
    parser['do_exit'] = parser['subparser'].add_parser('exit', help='Exits shells')

    ## do_test_parse parser
    parser['do_test_parse'] = parser['subparser'].add_parser('test_parse', help='test the argument parser')
    parser['do_test_parse'].add_argument('path', default=None, nargs='?', help='var1: str')
    # parser['do_test_parse'].add_argument('--var1', required=True, help='var1: str')
    # parser['do_test_parse'].add_argument('--var2', type=int, required=True, help='var1: int')
    # parser['do_test_parse'].add_argument('--opt1', action='store_true', default=False, help='optional arg1: str')

    ## Parent parsers
    # Runtime options
    parser['runtime'] = argparse.ArgumentParser(add_help=False)
    runtime = parser['runtime'].add_argument_group('runtime options')
    runtime.add_argument('-e', '--exec_loc', default='server', choices=['local', 'server', 'save'], help='execute commands locally, on a server, or save them. If local / save, server settings unused')
    runtime.add_argument('-E', '--exec_type', default='serial', choices=['serial', 'parallel', 'print', 'save'], help='execute commands serially, in parallel, or just print them')
    runtime.add_argument('-V', '--verbose', default=1, action='count', help='verbose execution of commands?')

    # File options
    parser['file'] = argparse.ArgumentParser(add_help=False)
    files = parser['file'].add_argument_group('file I/O options')
    files.add_argument('-p', '--pattern', default=[''], nargs='+', help='additional pattern for file matching')
    files.add_argument('-v', '--invert_pattern', default=['a^'], nargs='+', help='invert pattern matching to select non-matching files')
    files.add_argument('-r', '--recursive', action='store_true', default=False, help='search for files recursively')
    files.add_argument('-S', '--Suffix', default=None, help='optional suffix of file')

    # Alignment options
    parser['align'] = argparse.ArgumentParser(add_help=False)
    align = parser['align'].add_argument_group('aligment details')
    align.add_argument('-B', '--build', default='hg19', choices=['hg19', 'hg18', 'grc37', 'mm9'], help='genome build used in alignment')
    align.add_argument('-l', '--read_length', type=int, default=100, choices=[36, 50, 75, 100], help='read length of aligned library')

    # Server options
    parser['server'] = argparse.ArgumentParser(add_help=False)
    server = parser['server'].add_argument_group('server settings')
    server.add_argument('-n', default=0, type=int, metavar='N_PROCS', help='Runtime(!) / Server Setting: Number of processors')
    server.add_argument('-q', default='short', choices=['short', 'long', 'mcore', 'mini', 'park_2h', 'park_12h', 'park_1d', 'park_7d', 'park_15m', 'park_unlimited'], metavar='QUEUE', help='Server setting: queue to which to submit')
    server.add_argument('-W', default=None, metavar='WALL_TIME', help='Server setting: allowed wall-time for command')
    server.add_argument('-N', default=False, action='store_true', help='Server setting: notify by email when task completed')
    server.add_argument('-R', default=None, metavar='RESOURCE', help='Server setting: resource request e.g. "rusage[mem=2000]"')
    server.add_argument('-w', default=None, type=int, metavar='DEPENDENCY', help='Server setting: dependency on other submitted job')

    ## do_bam_by_chrm
    parser['do_bam_by_chrm'] = parser['subparser'].add_parser('bam_by_chrm', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Separate one bam file into 24 bam files, one for each chromosome')
    parser['do_bam_by_chrm'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for bam files')
    parser['do_bam_by_chrm'].add_argument('-o', '--dir_out', type=str, default='bam_by_chrm/', help='directory in which to save bam files')
    # parser['do_bam_by_chrm'].add_argument('--species', type=str, default='human', choices=['human', 'mouse'], help='what species is this?')

    ## do_bamqc
    parser['do_bamqc'] = parser['subparser'].add_parser('bamqc', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='run qualimap bamqc on bams')
    parser['do_bamqc'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for bam files')
    parser['do_bamqc'].add_argument('-o', '--dir_out', type=str, default='bamqc/', help='directory in which to save bamqc files')
    parser['do_bamqc'].add_argument('-j', '--java', type=str, default='30G', help='memory allocation for java')

    ## do_fastqc
    parser['do_fastqc'] = parser['subparser'].add_parser('fastqc', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='run fastqc on bams')
    parser['do_fastqc'].add_argument('-i', '--dir_in', type=str, default='fastq/', help='directory in which to search for fastq.gz files')
    parser['do_fastqc'].add_argument('-o', '--dir_out', type=str, default='fastqc/', help='directory in which to save fastqc files')
    parser['do_fastqc'].add_argument('--casava', action="store_true", default=False, help='use casava option of fastqc')

    ## do_PCRqc
    parser['do_PCRqc'] = parser['subparser'].add_parser('PCRqc', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Finds all significant CNV segs across files and calculates number of subjects sharing that segment')
    parser['do_PCRqc'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_PCRqc'].add_argument('-o', '--dir_out', type=str, default='PCRqc/', help='directory in which to save files')
    parser['do_PCRqc'].add_argument('-b', '--bin_sizes', type=int, default=100, help='bin size for cnv calling')
    parser['do_PCRqc'].add_argument('-L', '--lamtha', type=int, default=3, help='lambda for smoothing calls')
    parser['do_PCRqc'].add_argument('--prob', type=str, default='/home/mas138/orchestra/data/BSM/LibComp/NYGC/cnvs/agg/NYGC_bad_samples_MCR_merged_small.txt', help='path to problem region file')
    parser['do_PCRqc'].add_argument('-c', '--cutoff', type=float, default=0.2, help='threshold for segment ratio significance')
    # parser['do_PCRqc'].add_argument('-d', '--pvalue', type=float, default=1., help='threshold for segment p-value significance')
    parser['do_PCRqc'].add_argument('-P', '--perc', type=float, default=0.9, help='minimum percent overlap of case / control segs')
    parser['do_PCRqc'].add_argument('-u', '--buffer', type=int, default=0, help='amont by which to extend bad regions')
    # parser['do_PCRqc'].add_argument('--noRenormSex', action='store_true', default=False, help='do not renormalize sex chromosomes')

    ## do_bam_to_depth
    parser['do_bam_to_depth'] = parser['subparser'].add_parser('bam_to_depth', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Generate depth information from bam file')
    parser['do_bam_to_depth'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for bam files')
    parser['do_bam_to_depth'].add_argument('-o', '--dir_out', type=str, default='depth/', help='directory in which to save depth files')
    parser['do_bam_to_depth'].add_argument('-a', '--all_pos', action='store_true', default=False, help='extract depth at all positions, even those with no reads')

    ## do_bam_to_mappability
    parser['do_bam_to_mappability'] = parser['subparser'].add_parser('bam_to_mappability', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Get uniquely mapped positions from a bam file')
    parser['do_bam_to_mappability'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for files')
    parser['do_bam_to_mappability'].add_argument('-o', '--dir_out', type=str, default='mappability/', help='directory in which to save files')

    ## do_bam_to_mapd
    parser['do_bam_to_mapd'] = parser['subparser'].add_parser('bam_to_mapd', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Calculate mapd score from bam file')
    parser['do_bam_to_mapd'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for files')
    parser['do_bam_to_mapd'].add_argument('-o', '--dir_out', type=str, default='mapd/', help='directory in which to save files')

    ## do_bins_to_mapd
    parser['do_bins_to_mapd'] = parser['subparser'].add_parser('bins_to_mapd', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Calculate mapd score from bins file')
    parser['do_bins_to_mapd'].add_argument('-i', '--dir_in', type=str, default='bins/', help='directory in which to search for files')
    parser['do_bins_to_mapd'].add_argument('-o', '--dir_out', type=str, default='mapd/', help='directory in which to save files')
    parser['do_bins_to_mapd'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for which to calc mapd scores')

    ## do_bam_avg_depth
    # parser['do_bam_avg_depth'] = parser['subparser'].add_parser('bam_avg_depth', help='Calculate average coverage from a bam file')
    parser['do_bam_avg_depth'] = parser['subparser'].add_parser('bam_avg_depth', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Calculate average coverage of a bam file')
    parser['do_bam_avg_depth'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for files')
    parser['do_bam_avg_depth'].add_argument('-o', '--dir_out', type=str, default='depth/', help='directory in which to save average coverage calculations')
    # parser['do_bam_avg_depth'].add_argument('-n', '--n_procs', default=8, help='number of processors for round robin distributor')
    # parser['do_bam_avg_depth'].add_argument('-p', '--pattern', default='', help='additional pattern for bam file matching')
    # parser['do_bam_avg_depth'].add_argument('-r', '--recursive', action='store_true', default=False, help='search for files recursively')

    ## do_bam_downsample
    parser['do_bam_downsample'] = parser['subparser'].add_parser('bam_downsample', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Separate one bam file into 24 bam files, one for each chromosome')
    parser['do_bam_downsample'].add_argument('-i', '--dir_in', type=str, default='bam/', help='directory in which to search for bam files')
    parser['do_bam_downsample'].add_argument('-o', '--dir_out', type=str, default='bam/', help='directory in which to save bam files')
    parser['do_bam_downsample'].add_argument('-d', '--depth', nargs='+', default=10, help='desired depth after downsampling')

    ## do_map_to_bins
    parser['do_map_to_bins'] = parser['subparser'].add_parser('map_to_bins', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Calculate bin edges based on uniquely mapped reads')
    parser['do_map_to_bins'].add_argument('-i', '--dir_in', type=str, default='mappability/', help='directory in which to search for files')
    parser['do_map_to_bins'].add_argument('-o', '--dir_out', type=str, default='bins/', help='directory in which to save files')
    parser['do_map_to_bins'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for normalization')
    # parser['do_map_to_bins'].add_argument('-l', '--read_length', type=int, default=36, help='read length of library')
    # parser['do_map_to_bins'].add_argument('-B', '--build', type=str, default='hg19', choices=['hg19', 'hg18', 'mm9'], help='build of comparison genome')
    parser['do_map_to_bins'].add_argument('-P', '--perc', type=float, default='0.0005', help='subsampling percentage for normalization')
    parser['do_map_to_bins'].add_argument('--gc_bin', action="store_true", default=False, help='report gc content of each bin')
    parser['do_map_to_bins'].add_argument('--bin_only', action="store_true", default=False, help='only bin the data; do not perform normalization')

    ## do_bins_merge
    parser['do_bins_merge'] = parser['subparser'].add_parser('bins_merge', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Calculate mapd score from bins file')
    parser['do_bins_merge'].add_argument('-i', '--dir_in', type=str, default='bins/', help='directory in which to search for files')
    parser['do_bins_merge'].add_argument('-o', '--dir_out', type=str, default='bins/', help='directory in which to save files')
    parser['do_bins_merge'].add_argument('-b', '--bin_size', type=int, default=100, help='bin size of input')
    parser['do_bins_merge'].add_argument('-m', '--merge_size', type=int, nargs='+', default=[1000], help='merged bin size(s) of output')

    ## do_bins_to_cnvs
    parser['do_bins_to_cnvs'] = parser['subparser'].add_parser('bins_to_cnvs', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Call cnvs based on predefined bins. Uses BIC-Seq2 algorithm')
    parser['do_bins_to_cnvs'].add_argument('-i', '--dir_in', type=str, default='bins/', help='directory in which to search for files')
    parser['do_bins_to_cnvs'].add_argument('-o', '--dir_out', type=str, default='cnvs/', help='directory in which to save files')
    parser['do_bins_to_cnvs'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_bins_to_cnvs'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')
    parser['do_bins_to_cnvs'].add_argument('--control', type=str, default=None, help='path to control bin file (relative to ddata)')
    parser['do_bins_to_cnvs'].add_argument('--noscale', action="store_true", default=False, help='do not automatically scale lambda')
    parser['do_bins_to_cnvs'].add_argument('--noBootstrap', dest='bootstrap', action="store_false", default=True, help='do not perform a permutation test to assign p_values')
    parser['do_bins_to_cnvs'].add_argument('-m', '--multisample', action="store_true", default=False, help='perform multi-sample segmentation')
    # parser['do_bins_to_cnvs'].add_argument('-l', '--read_length', type=int, default=36, help='read length of library')
    # parser['do_bins_to_cnvs'].add_argument('-B', '--build', type=str, default='hg19', choices=['hg19', 'hg18', 'mm9'], help='build of comparison genome')

    ## do_cnv_postproc
    parser['do_cnv_postproc'] = parser['subparser'].add_parser('cnv_postproc', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Postprocessing of CNV calls; includes plotting and gene annotation')
    parser['do_cnv_postproc'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for files')
    parser['do_cnv_postproc'].add_argument('-o', '--dir_out', type=str, default='cnvs/figs', help='directory in which to save files')
    parser['do_cnv_postproc'].add_argument('--dir_bin', type=str, default='bins/', help='directory in which to find bin files')
    parser['do_cnv_postproc'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv_postproc'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')
    # parser['do_cnv_postproc'].add_argument('-l', '--read_length', type=int, default=36, help='read length of library')
    # parser['do_cnv_postproc'].add_argument('-B', '--build', type=str, default='hg19', choices=['hg19', 'hg18', 'mm9'], help='build of comparison genome')
    parser['do_cnv_postproc'].add_argument('--loc', type=str, default=None, help='specific genome location to plot')
    # parser['do_cnv_postproc'].add_argument('--suffix', type=str, default='.out', help='suffix of cnv file')

    ## do_cnv_seg_aggregate
    parser['do_cnv_seg_aggregate'] = parser['subparser'].add_parser('cnv_seg_aggregate', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Finds all significant CNV segs across files and calculates number of subjects sharing that segment')
    parser['do_cnv_seg_aggregate'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_cnv_seg_aggregate'].add_argument('-o', '--dir_out', type=str, default='cnvs/agg/', help='directory in which to save files')
    parser['do_cnv_seg_aggregate'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv_seg_aggregate'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')
    parser['do_cnv_seg_aggregate'].add_argument('-c', '--cutoff', type=float, default=0.3, help='threshold for segment significance')
    parser['do_cnv_seg_aggregate'].add_argument('-d', '--pvalue', type=float, default=1., help='threshold for segment p-value significance')
    parser['do_cnv_seg_aggregate'].add_argument('--f_norm', type=str, default=None, help='segmentation file to use for normalization')
    parser['do_cnv_seg_aggregate'].add_argument('--normby', type=str, default='segs', choices=['segs', 'chrm'], help='Grouping for normalization')
    parser['do_cnv_seg_aggregate'].add_argument('--res', type=str, default='mean', choices=['mean', 'median'], help='resolution method for overlapping seg ratios')
    parser['do_cnv_seg_aggregate'].add_argument('-g', '--groupby', type=str, default='n', choices=['n', 'sig'], help='Grouping for plotting')
    parser['do_cnv_seg_aggregate'].add_argument('--noRenormSex', dest='renormSex', action='store_false', default=True, help='do not renormalize sex chromosomes')

    ## do_cnv_normalize
    parser['do_cnv_normalize'] = parser['subparser'].add_parser('cnv_normalize', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Normalize CNV files and save normalized data')
    parser['do_cnv_normalize'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_cnv_normalize'].add_argument('-o', '--dir_out', type=str, default='cnvs/', help='directory in which to save files')
    parser['do_cnv_normalize'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv_normalize'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')
    parser['do_cnv_normalize'].add_argument('--f_norm', type=str, default=None, help='segmentation file to use for normalization')
    parser['do_cnv_normalize'].add_argument('--normby', type=str, default='segs', choices=['segs', 'chrm'], help='Grouping for normalization')

    ## do_cnv_post_pipeline
    parser['do_cnv_post_pipeline'] = parser['subparser'].add_parser('cnv_post_pipeline', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Finds all significant CNV segs across files and calculates number of subjects sharing that segment')
    parser['do_cnv_post_pipeline'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_cnv_post_pipeline'].add_argument('-o', '--dir_out', type=str, default='cnvs/agg/', help='directory in which to save files')
    parser['do_cnv_post_pipeline'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv_post_pipeline'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')
    parser['do_cnv_post_pipeline'].add_argument('-c', '--cutoff', type=float, default=0.3, help='threshold for segment ratio significance')
    parser['do_cnv_post_pipeline'].add_argument('-d', '--pvalue', type=float, default=1., help='threshold for segment p-value significance')
    parser['do_cnv_post_pipeline'].add_argument('-f', '--frac', type=float, default=0.5, help='fraction of samples with variation to call a region problematic')
    parser['do_cnv_post_pipeline'].add_argument('-F', '--f_prob', type=str, default=None, help='path to problem file. Overrides -f if supplied')
    parser['do_cnv_post_pipeline'].add_argument('-P', '--perc', type=float, default=0.5, help='minimum percent overlap of case / control segs')
    parser['do_cnv_post_pipeline'].add_argument('--f_norm', type=str, default=None, help='segmentation file to use for normalization')
    parser['do_cnv_post_pipeline'].add_argument('--f_norm_var', type=str, default=None, help='file of regions of normal variability')
    parser['do_cnv_post_pipeline'].add_argument('--f_annot', type=str, default=None, help='gene annotation file')
    parser['do_cnv_post_pipeline'].add_argument('--noRenormSex', action='store_true', default=False, help='do not renormalize sex chromosomes')

    ## do_cnv_post_pipeline
    parser['do_cnv_annotate'] = parser['subparser'].add_parser('cnv_post_pipeline', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Finds all significant CNV segs across files and calculates number of subjects sharing that segment')
    parser['do_cnv_annotate'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_cnv_annotate'].add_argument('-o', '--dir_out', type=str, default='cnvs/agg/', help='directory in which to save files')
    parser['do_cnv_annotate'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv_annotate'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')
    parser['do_cnv_annotate'].add_argument('-c', '--cutoff', type=float, default=0.3, help='threshold for segment ratio significance')
    parser['do_cnv_annotate'].add_argument('-d', '--pvalue', type=float, default=1., help='threshold for segment p-value significance')
    parser['do_cnv_annotate'].add_argument('-g', '--length', type=float, default=0, help='minimum length of cnv')
    parser['do_cnv_annotate'].add_argument('--f_annot', type=str, default=None, help='gene annotation file')

    ## do_plot_bins_agg
    parser['do_plot_bins_agg'] = parser['subparser'].add_parser('plot_bins_agg', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Postprocessing of CNV calls; includes plotting and gene annotation')
    parser['do_plot_bins_agg'].add_argument('-i', '--dir_in', type=str, default='bins/', help='directory in which to search for files')
    parser['do_plot_bins_agg'].add_argument('-o', '--dir_out', type=str, default='bins/agg', help='directory in which to save files')
    # parser['do_plot_bins_agg'].add_argument('--dir_bin', type=str, default='bins/', help='directory in which to find bin files')
    parser['do_plot_bins_agg'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_plot_bins_agg'].add_argument('--loc', type=str, default=None, help='specific genome location to plot')

    ## do_plot_bins_segs_agg
    parser['do_plot_bins_segs_agg'] = parser['subparser'].add_parser('plot_bins_segs_agg', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Plot background bins and segs for several samples on one ')
    parser['do_plot_bins_segs_agg'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for files')
    parser['do_plot_bins_segs_agg'].add_argument('-o', '--dir_out', type=str, default='cnvs/figs', help='directory in which to save files')
    parser['do_plot_bins_segs_agg'].add_argument('--dir_bin', type=str, default='bins/', help='directory in which to find bin files')
    parser['do_plot_bins_segs_agg'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_plot_bins_segs_agg'].add_argument('-L', '--lamtha', type=int, default=3, help='lambda for smoothing calls (this function can only take one value for lamtha!)')
    parser['do_plot_bins_segs_agg'].add_argument('-c', '--cutoff', type=float, default=0.3, help='log2 ratio cutoff for significance of segments')
    parser['do_plot_bins_segs_agg'].add_argument('--loc', type=str, default=None, help='specific genome location to plot')
    parser['do_plot_bins_segs_agg'].add_argument('--genes', action='store_true', default=False, help='plot with gene annotation (hg19)?')

    ## do_plot_segs_agg
    parser['do_plot_segs_agg'] = parser['subparser'].add_parser('plot_segs_agg', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Plot heatmaps of segs for several samples on one figure')
    parser['do_plot_segs_agg'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for files')
    parser['do_plot_segs_agg'].add_argument('-o', '--dir_out', type=str, default='cnvs/figs', help='directory in which to save files')
    parser['do_plot_segs_agg'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_plot_segs_agg'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda for smoothing calls')
    parser['do_plot_segs_agg'].add_argument('--loc', type=str, default=None, help='specific genome location to plot')

    ## do_cnv2igv
    parser['do_cnv2igv'] = parser['subparser'].add_parser('cnv2igv', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Convert cnv files into an IGV seg file')
    parser['do_cnv2igv'].add_argument('-i', '--dir_in', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_cnv2igv'].add_argument('-o', '--dir_out', type=str, default='cnvs/igv/', help='directory in which to save files')
    parser['do_cnv2igv'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv2igv'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')

    ## do_figure_glob
    parser['do_figure_glob'] = parser['subparser'].add_parser('figure_glob', parents=[parser['align'], parser['file']], help='Recursively find figures and copy to a single directory')
    parser['do_figure_glob'].add_argument('-i', '--dir_in', type=str, default='.', help='directory in which to search for files')
    parser['do_figure_glob'].add_argument('-o', '--dir_out', type=str, default='figures/', help='directory in which to save files')
    parser['do_figure_glob'].add_argument('-s', '--suffix', type=str, default='.png', choices=['.png', '.jpg', '.jpeg', '.pdf', '.eps'], help='suffix of files to grep for')

    ## do_cnv_mapd_correlate
    parser['do_cnv_mapd_correlate'] = parser['subparser'].add_parser('cnv_mapd_correlate', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Postprocessing of CNV calls; includes plotting and gene annotation')
    parser['do_cnv_mapd_correlate'].add_argument('--dir_cnv', type=str, default='cnvs/', help='directory in which to search for cnv files')
    parser['do_cnv_mapd_correlate'].add_argument('--dir_mapd', type=str, default='mapd/', help='directory in which to find mapd')
    parser['do_cnv_mapd_correlate'].add_argument('-o', '--dir_out', type=str, default='mapd/figures/', help='directory in which to save files')
    parser['do_cnv_mapd_correlate'].add_argument('-b', '--bin_sizes', type=int, nargs='+', default=[100], help='bin size(s) for cnv calling')
    parser['do_cnv_mapd_correlate'].add_argument('-L', '--lamtha', type=int, nargs='+', default=[3], help='lambda(s) for smoothing calls')

    # ## do_figure_glob
    # parser['do_figure_glob'] = parser['subparser'].add_parser('figure_glob', parents=[parser['file']], help='Recursively find figures and copy to a single directory')
    # parser['do_figure_glob'].add_argument('-i', '--dir_in', type=str, default='.', help='directory in which to search for files')
    # parser['do_figure_glob'].add_argument('-o', '--dir_out', type=str, default='figures/', help='directory in which to save files')
    # parser['do_figure_glob'].add_argument('-s', '--suffix', type=str, default='.png', choices=['.png', '.jpg', '.jpeg', '.pdf', '.eps'], help='suffix of files to grep for')

    ## do_depth_to_pgram
    parser['do_depth_to_pgram'] = parser['subparser'].add_parser('depth_to_pgram', parents=[parser['align'], parser['file'], parser['runtime'], parser['server']], help='Calculate periodigram from depth information')
    parser['do_depth_to_pgram'].add_argument('-i', '--dir_in', type=str, default='depth/', help='directory in which to search for files')
    parser['do_depth_to_pgram'].add_argument('-o', '--dir_out', type=str, default='rawpgram', help='directory in which to save files')
    parser['do_depth_to_pgram'].add_argument('-s', '--smooth', type=int, default='4', help='smoothing factor for pgram estimation')
    parser['do_depth_to_pgram'].add_argument('-t', '--taper', type=float, default='0', help='taper length for pgram estimation')
    
    return parser

def add_args(args, args_new):
    """ Add new args to args previously parsed by argparser
    """
    for key, item in args_new.iteritems():
        setattr(args, key, item)

def load_config(section=None):
    """ Loading user-defined configuration options from ngssh.conf
    """
    conf = ConfigParser.SafeConfigParser()
    conf.read("ngssh.conf")

    if section:
        return(dict(conf.items(section)))

    else:
        return(conf)
