# decorators.py - decorator functions
#
# v 0.6.9
# rev 2016-11-22 (MS: suffix includes PCRqc)
# Notes:

import CmdUtils
import arg_parser
import FileUtils as fUtils

import ConfigParser

def IO(func):
    def func_IO(ddata, p):
        # Get function name
        name = func.__name__.split('exec_')[-1]

        # Add args
        new_args = {'name': name, 
                    'save_cmd': ddata / 'server_cmds',
                    'tmp_dir': ddata / 'tmp',
                    'script': None,
                    'chrm': True,
                   }

        arg_parser.add_args(p, new_args)

        # Check for scripts
        try:
            p.script = p.conf.get('scripts', name)
        except ConfigParser.NoOptionError:
            pass

        # Will file names have "chrm" in them?
        if 'grc' in p.build:
            p.chrm = False

        # Input/output dirs
        p.dir_in, p.dir_out = fUtils.dir_path_build_and_check(ddata, p.dir_in, p.dir_out)
        fUtils.check_dir_out(p.save_cmd)
        fUtils.check_dir_out(p.tmp_dir)

        return func(ddata, p)

    func_IO.__name__ = func.__name__
    func_IO.__doc__ = func.__doc__

    return func_IO

def add_args(func):
    def func_args(ddata, p):
        # name = func.__name__.split('exec_')[-1]
        check_name(p, func)
        new_args = {# 'name': name, 
                    'save_cmd': ddata / 'server_cmds',
                    'tmp_dir': ddata / 'tmp',
                    'script': None,
                   }

        arg_parser.add_args(p, new_args)

        fUtils.check_dir_out(p.save_cmd)
        fUtils.check_dir_out(p.tmp_dir)

        return func(ddata, p)

    return func_args

# NOTE: DEPCRECATED. WILL BE REMOVED IN FUTURE
def lookup_script(func):
    def func_script(ddata, p):
        # name = func.__name__.split('exec_')[-1]
        check_name(p, func)
        script = CmdUtils.lookup_script_by_fnc(p.name)

        p.script = script

        return func(ddata, p)

    func_script.__name__ = func.__name__
    func_script.__doc__ = func.__doc__

    return func_script

def species(func):
    def func_species(ddata, p):
        species = fUtils.species_from_build(p.build)
        arg_parser.add_args(p, {'species': species})

        return func(ddata, p)

    func_species.__name__ = func.__name__
    func_species.__doc__ = func.__doc__

    return func_species

def suffix(func):
    def func_suffix(ddata, p):
        if not p.Suffix:
            # name = func.__name__.split('exec_')[-1]
            check_name(p, func)
            
            if p.name.startswith('cnv'):
                p.Suffix = "BICseq.out"

            elif p.name.startswith('plot'):
                p.Suffix = "BICseq.out"

            elif p.name.startswith('PCRqc'):
                p.Suffix = "BICseq.out"

            elif p.name.startswith('fastq'):
                p.Suffix = "fastq.gz"

            else:
                raise TypeError('function %s not recognized' % p.name)

        return func(ddata, p)

    func_suffix.__name__ = func.__name__
    func_suffix.__doc__ = func.__doc__

    return func_suffix

def walltime(func):
    def func_walltime(runtime):
        try:
            t_wall = runtime.conf.get('queues', runtime.q)

        except:
            print("No walltime associated with queueu %s. If you did not specify a walltime, default will be 12:00")
            t_wall = "12:00"

        # t_wall = CmdUtils.walltime_by_queue(runtime.q)

        if not runtime.W:
            runtime.W = t_wall

        return func(runtime)
    return func_walltime

def parse_args(func):
    def func_parse_args(self, args):
        name = func.__name__
        p = self.parser[name].parse_args(args.split())
        arg_parser.add_args(p, {'conf': self.config})

        return func(self, p)

    # Preserve function name and help docs
    func_parse_args.__name__ = func.__name__
    func_parse_args.__doc__ = func.__doc__

    return func_parse_args

def check_name(p, func):
    try:
        len(p.name)

    except AttributeError:
        name = func.__name__.split('exec_')[-1]
        arg_parser.add_args(p, {'name': name})
