# clidefs.py - Execution of CLI commands. Mostly file IO and calls to other fncs.
#
# v 0.6.9
# rev 2016-11-15 (MS: exec_PCRqc)
# Notes: overhaul of cnv_postproc

import itertools as it
import shutil
import pathlib2 as pathlib

import FileUtils as fUtils
import BamUtils
import CmdUtils
import RUtils
import perlUtils
import BinUtils
import round_robin as rr
import arg_parser
import CNVfn
import decorators

def exec_njobs():
    job_list     = rr.exec_cmd("bjobs").split('\n')
    job_list     = [' '.join(job.split()) for job in job_list if job][1:]
    running_list = [job.split()[0] for job in job_list if job.split()[2]=="RUN"]
    pending_list = [job.split()[0] for job in job_list if job.split()[2]=="PEND"]

    print "\nRunning jobs: %i" %len(running_list)
    print "Pending jobs: %i\n" %len(pending_list)

@decorators.IO
@decorators.species
def exec_bam_by_chrm(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in    input directory
                                        --dir_out   output directory
                                        --n_procs   number of processors
                                        --pattern   additional pattern for file matching
                                        --exec      execute cmds locally or on server

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, invert_pattern=p.invert_pattern)

    # Create directories in which to save chromosomal bam files for each subject
    dirs_chrm, subjects = fUtils.dir_by_samples(list_bam, p.dir_out)

    # Create output file prefixes
    f_out_prefix = [d / s for d, s in zip(dirs_chrm, subjects)]

    # Create separation commands
    cmd_list = CmdUtils.generate_IO_cmds(list_bam, f_out_prefix, BamUtils.bam_by_chrm, **{'species': p.species})

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_bam_to_mapd(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, recursive=p.recursive)

    # Preserve input directory structure in output directory
    list_f_out = fUtils.dir_mirror_structure(list_bam, p.dir_in, p.dir_out, '.50k')

    # Create commands
    args = [list_bam, list_f_out]
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, *args)

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_bins_to_mapd(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins for which to calc mapd

        Output:
    """
    # Get paths to bin dirs and create output files
    list_bin_dirs = fUtils.match_bin_dirs(p.dir_in, p.bin_sizes, p.pattern)
    list_f_out = [p.dir_out / (d.parts[-2]+"__%s.mapd" % d.parts[-1]) for d in list_bin_dirs]

    # Create commands
    kwargs = {'i': list_bin_dirs, 'o': list_f_out, 'chrm': p.chrm}
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_bam_to_depth(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --pattern       additional pattern for file matching
                                        --recursive     search for files recursively?
                                        --all_pos       get depth at all positions?
                                        --n_procs       number of processors

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, recursive=p.recursive)

    # Preserve input directory structure in output directory
    list_f_out = fUtils.dir_mirror_structure(list_bam, p.dir_in, p.dir_out, '.depth')

    # Create commands
    cmd_list = CmdUtils.generate_IO_cmds(list_bam, list_f_out, BamUtils.bam_to_depth)

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_bamqc(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --pattern       additional pattern for file matching
                                        --recursive     search for files recursively?
                                        --n_procs       number of processors

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, recursive=p.recursive)

    # Preserve input directory structure in output directory
    list_dir_out = fUtils.dir_mirror_structure(list_bam, p.dir_in, p.dir_out, '')
    # [fUtils.check_dir_out(d) for d in list_dir_out]

    args = [['bamqc' for f in list_bam]]
    kwargs = {'-bam': list_bam, '-outdir': list_dir_out, '-gd': p.build, '-nt': p.n, '--java-mem-size={}'.format(p.java): True, '-ip':True}
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd2, p.script, *args, **kwargs)

    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
@decorators.suffix
def exec_fastqc(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --pattern       additional pattern for file matching
                                        --recursive     search for files recursively?
                                        --n_procs       number of processors

        Output:
    """
    # Get bam files
    list_fastqc = fUtils.file_glob(p.dir_in, p.Suffix, pattern=p.pattern, recursive=p.recursive)

    # Preserve input directory structure in output directory
    list_dir_out = fUtils.dir_mirror_structure(list_fastqc, p.dir_in, p.dir_out, '')
    [fUtils.check_dir_out(d) for d in list_dir_out]

    args = [list_fastqc]
    kwargs = {'o': list_dir_out, 'casava': p.casava, '-t': p.n}
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, *args, **kwargs)

    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
@decorators.suffix
def exec_PCRqc(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --cutoff        signficance cutoff
                                        --perc          percent overlap
                                        --buffer        how much by which to extend bad regions

        Output:
    """
    list_f_cnv, _, _ = fUtils.match_seg_files(p.dir_in, [p.bin_sizes], [p.lamtha], p.Suffix, p.pattern, p.invert_pattern) 
    list_f_out = [p.dir_out / (f.name.split('.')[0] + '_pcrqc.txt') for f in list_f_cnv]

    kwargs = {'i': list_f_cnv, 'o': list_f_out, 'p': p.prob, 'P': p.perc, 'c': p.cutoff}
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
@decorators.species
def exec_bam_to_mappability(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, recursive=p.recursive)

    n_chr = len(BamUtils.get_chrm_numbers(p.species))

    if len(list_bam) % n_chr == 0:
        print "Bam files seem already separated by chromosome. Proceeding as such."

        # Preserve input directory structure in output directory
        list_f_out = fUtils.dir_mirror_structure(list_bam, p.dir_in, p.dir_out, '.map')
        chrm_list = None

    else:
        print "Looks like Bam files aren't separated by chromosome. Proceeding as such."

        dirs_out, subjects = fUtils.dir_by_samples(list_bam, p.dir_out)

        # Create output file prefixes
        f_out_prefix = [d / s for d, s in zip(dirs_out, subjects)]

        # Chromosome numbers and labels
        chrms = BamUtils.get_chrm_numbers(p.species)
        chrms_labels = BamUtils.get_chrm_labels(chrms)

        # Construct lists for BamUtils.bam_to_mappability command
        # Tricksy but not very readable. Sorry.
        chrm_list = [c for chroms in [chrms for i in range(len(list_bam))] for c in chroms]
        list_bam = [f for f in list_bam for chrm in chrms_labels]
        list_f_out = [str(prefix)+'.%s.map' %label for prefix in f_out_prefix for label in chrms_labels]
        # list_f_out = [prefix.with_suffix('.%s.map' %label) for prefix in f_out_prefix for label in chrms_labels]

    # Create commands
    cmd_list = CmdUtils.generate_IO_cmds(list_bam, list_f_out, BamUtils.bam_to_mappability, **{'chrom': chrm_list})

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_bam_downsample(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --depth         desired depth of downsampling

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, invert_pattern=p.invert_pattern)

    # Iterate through depths
    for desired_depth in p.depth:
        dir_out = p.dir_out / ("%sx" % desired_depth)
        list_f_out = fUtils.dir_mirror_structure(list_bam, p.dir_in, dir_out, ".%sx.bam" %(desired_depth))

        # Buid and execute command
        kwargs = {'i': list_bam, 'o': list_f_out, 'd': desired_depth}
        cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script,**kwargs)

        CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
@decorators.species
def exec_map_to_bins(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     desired size of bins
                                        --read_length   length of reads
                                        --build         genome build to compare against
                                        --perc          subsampling percentage for normalization

        Output:
    """
    # Get sub dirs in dir_in
    sub_dirs = fUtils.get_sub_dirs(p.dir_in, p.pattern)

    for bin_size in p.bin_sizes:
        list_config = []
        list_prm = []

        for sub_dir in sub_dirs:
            # Get bam files
            list_f_in = fUtils.file_glob(sub_dir, '.map', pattern=p.pattern, recursive=p.recursive)

            # Preserve input directory structure in output directory
            list_f_out = fUtils.dir_mirror_structure(list_f_in, p.dir_in, p.dir_out, '.b%i.bin' %bin_size)

            # Create a deeper subdir for better output organization based on bin size
            if p.bin_only:
                list_f_out = fUtils.add_subdir('b%02d_bin_only' %bin_size, list_f_out)
            else :
                list_f_out = fUtils.add_subdir('b%02d' %bin_size, list_f_out)

            # Configuration file names
            config_name_short = sub_dir.name + '__B%i.cfg' %bin_size 
            list_config.append(p.dir_out/ sub_dir.name / config_name_short)
            list_prm.append(list_config[-1].with_suffix('.prm'))

            perlUtils.bic_seq_normalize_config(list_f_in, list_f_out, list_config[-1], p.build, p.read_length, p.species)

        # Create commands
        args = [list_config, list_prm]
        kwargs = {'l': p.read_length, 'b': bin_size, 'p': p.perc, 'tmp': str(p.tmp_dir), 'gc_bin': p.gc_bin, 'bin_only': p.bin_only}
        cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, *args, **kwargs)

        # Execute commands
        CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_bins_merge(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_size     size of input bins
                                        --merge_size   size of output bins

        Output:
    """
    merge_size = []
    for m in p.merge_size:
        if m % p.bin_size == 0:
            merge_size.append(m)
        else:
            print "Merge size %i is not a multiple of bin size %i. Ignoring it." %(m, p.bin_size)

    list_bin_dirs = fUtils.match_bin_dirs(p.dir_in, [p.bin_size], p.pattern)

    for m in p.merge_size:
        list_dir_out_tmp = [d.parent / ("b%i" %m) for d in list_bin_dirs]
        list_dir_out = fUtils.dir_mirror_structure(list_dir_out_tmp, p.dir_in, p.dir_out, '')
        [fUtils.check_dir_out(d) for d in list_dir_out]

        kwargs = {'i': list_bin_dirs, 'o': list_dir_out, 'b': p.bin_size, 'm': m}
        cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

        CmdUtils.execute(cmd_list, p.exec_loc, p)
        
@decorators.IO
@decorators.species
def exec_bins_to_cnvs(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     desired size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --read_length   length of reads
                                        --build         genome build to compare against
                                        --control       bin dir of control genome(s)
                                        --noscale       do not automatically scale lambda
                                        --noBootstrap   do not assign p-values via a boostrap
                                        --multisample   perform multi-sample segmentation

        Output:
    """
    if p.multisample:
        CNVfn.bins2cnvs_multisample(ddata, p)

    else:
        CNVfn.bins2cnvs_singlesample(ddata, p)

@decorators.IO
@decorators.species
@decorators.suffix
def exec_cnv_postproc(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_bins      bins directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     desired size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --read_length   length of reads
                                        --build         genome build to compare against
                                        --suffix        suffix of cnv file

        Output:
    """
    # Directory paths and directory checks
    dir_bin, _ = fUtils.dir_path_build_and_check(ddata, p.dir_bin, p.dir_out)
    print dir_bin

    # Input files and dirs
    list_f_seg, list_bin_sizes, list_lamtha = fUtils.match_seg_files(p.dir_in, p.bin_sizes, p.lamtha, p.Suffix, p.pattern)
    list_bin_dirs = fUtils.bin_dirs_from_seg_files(list_f_seg, dir_bin, list_bin_sizes)

    # Construct gene output files and figure names
    # list_gene_out = [f.with_suffix('.genes') for f in list_f_seg]
    # list_fig_names = [(f.parent / f.stem).stem for f in list_f_seg]
    list_fig_names = [p.dir_out / (f.stem+'.binSeg.png') for f in list_f_seg]

    # if p.species == 'mouse':
    #     mouse = 1
    # else:
    #     mouse = 0

    # Create plotting commands
    kwargs = {'i': list_bin_dirs, 's': list_f_seg, 'o': list_fig_names, 'loc': p.loc, 'chrm': p.chrm}
    cmd_list_plot = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, './R/plotBinsAndSegsAgg_png.R', **kwargs)

    # Create gene commands
    # script = p.conf.get('paths', 'perl') + '/NBICseq_v0.6.3/annotGene_ms.pl'
    # args = [list_f_seg, list_gene_out]
    # kwargs = {'v': p.build}
    # cmd_list_gene = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, script, *args, **kwargs)

    # Execute commands
    CmdUtils.execute(cmd_list_plot, p.exec_loc, p)
    # CmdUtils.execute(cmd_list_gene, p.exec_loc, p)

@decorators.IO
@decorators.suffix
def exec_cnv_annotate(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --cutoff        ratio significance cutoff
                                        --pvalue        pvalue significance cutoff
                                        --f_annot       annotation file (abs path)

        Output:
    """
    list_f_cnv, _, _ = fUtils.match_seg_files(p.dir_in, p.bin_sizes, p.lamtha, p.Suffix, p.pattern, p.invert_pattern) 
    list_f_out = [f.with_suffix(".genes") for f in list_f_cnv]
    print list_f_cnv
    print list_f_out

    kwargs = {'i': list_f_cnv, 'o': list_f_out, 'a': p.f_annot, 'c': p.cutoff, 'p': p.pvalue, 'l': p.length}
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
@decorators.suffix
def exec_cnv_post_pipeline(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --cutoff        significance cutoff
                                        --f_norm_var    file of regions of normal variability (abs path)
                                        --f_annot       annotation file (abs path)
                                        --renormSex     mean renormalize sex chromosomes

        Output:
    """
    # Get files for specified bin size, lambda, and pattern
    list_f_cnv, _, _ = fUtils.match_seg_files(p.dir_in, p.bin_sizes, p.lamtha, p.Suffix, p.pattern, p.invert_pattern) 
    str_list_cnv = '\"' + ' '.join([str(f) for f in list_f_cnv]) + '\"'

    # Get base name
    subject = ddata.name
    b = '-b'.join([str(b) for b in p.bin_sizes])
    l = '-L'.join([str(l) for l in p.lamtha])
    fname = '%s__b%s_L%s.t%i.p%i' %(subject, b, l, p.cutoff*10, p.pvalue*100)

    # Construct command
    kwargs = {'i': str_list_cnv, 'o': p.dir_out, 'n': p.f_norm, 'N': p.f_norm_var, 'a': p.f_annot, 'b': fname, 'c': p.cutoff, 'p': p.pvalue, 'f': p.frac, 'F': p.f_prob, 'P': p.perc, 'noRenormSex': p.noRenormSex}
    cmd = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    # Execute command
    CmdUtils.execute(cmd, p.exec_loc, p)

@decorators.IO
@decorators.suffix
def exec_cnv_seg_aggregate(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --cutoff        significance cutoff
                                        --f_norm        normilization file
                                        --normby        grouping for normalization
                                        --groupby       grouping for plotting
                                        --res           resolution function
                                        --renormSex     renormalize sex chromosomes

        Output:
    """
    # Get files for specified bin size, lambda, and pattern
    list_f_cnv, _, _ = fUtils.match_seg_files(p.dir_in, p.bin_sizes, p.lamtha, p.Suffix, p.pattern, p.invert_pattern) 
    str_list_cnv = '\"' + ' '.join([str(f) for f in list_f_cnv]) + '\"'

    # Get base name
    subject = ddata.name
    b = '-b'.join([str(b) for b in p.bin_sizes])
    l = '-L'.join([str(l) for l in p.lamtha])
    fname = '%s__b%s_L%s.t%i.p%i.%s.agg' %(subject, b, l, p.cutoff*10, p.pvalue*100, p.res)

    # Build output paths and fig title
    f_out = p.dir_out / fname
    fig_out = f_out.with_suffix('.png')
    fig_title = "\'Bin: %s \t lambda: %s\'" %(p.bin_sizes[0], p.lamtha[0])

    # Do consensus analysis
    if not p.cutoff:
        p.cutoff = 1e-16

    kwargs = {'i': str_list_cnv, 'o': f_out, 'c': p.cutoff, 'p': p.pvalue, 'res': p.res, 'renormSex': p.renormSex}
    cmd = CmdUtils.build_generic_cmd(p.script, **kwargs)

    # Plot consensus analysis
    if p.groupby == 'n':
        script =  p.conf.get('paths', 'R') + "/plotProfile.R"

    elif p.groupby == 'sig':
        script =  p.conf.get('paths', 'R') + "/plotProfile_sig.R"

    kwargs = {'i': f_out, 'o': fig_out, "title": fig_title}
    cmd += '\n' + CmdUtils.build_generic_cmd(script, **kwargs)

    # Execute command
    CmdUtils.execute([cmd], p.exec_loc, p)

def exec_cnv_mapd_correlate(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_cnv       input cnv directory (relative to ddata)
                                        --dir_mapd      input mapd directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)

        Output:
    """
    # Add runtime arguments
    arg_parser.add_args(p, {'name': 'cnv_postproc', 'save_cmd': ddata / 'server_cmds'})

    # Directory paths and directory checks
    dir_cnv, dir_out = fUtils.dir_path_build_and_check(ddata, p.dir_cnv, p.dir_out)
    dir_mapd, _ = fUtils.dir_path_build_and_check(ddata, p.dir_mapd, p.dir_out)

    # Perform analysis by bin size / lambda combinations
    for b, l in it.product(p.bin_sizes, p.lamtha):
        cnv_suffix = 'B%03d_l%02d.BICseq.out' %(b, l)
        subject = ddata.name

        list_f_cnv = fUtils.file_glob(dir_cnv, cnv_suffix, pattern=p.pattern, recursive=True)
        list_f_mapd = [fUtils.file_glob(dir_mapd, 'b%d.mapd' %b, pattern=f.name.split('__')[0])[0] for f in list_f_cnv]
        f_out = dir_out / ('%s_mapd-cnv__B%03d_l%02d.png' %(subject, b, l))
        title = "Name: %s \t Bins: %i \t Lambda: %i" %(subject, b, l)

        CNVfn.cnv_mapd_correlate(list_f_cnv, list_f_mapd, f_out, title)

@decorators.IO
@decorators.suffix
def exec_cnv2igv(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)

        Output:
    """
    # Preallocate list for cnv files
    list_f_cnv = []

    # Get files for specified bin size, lambda, and pattern
    for b, l in it.product(p.bin_sizes, p.lamtha):
        cnv_suffix = 'B%03d_l%02d.%s' %(b, l, p.Suffix)
        list_f_cnv = fUtils.file_glob(p.dir_in, cnv_suffix, pattern=p.pattern, recursive=True)

    # Get base name
    subject = ddata.name

    # Construct output file
    fname = '%s_cnv-agg__B%03d_l%02d.cbs' %(subject, b, l)
    f_out = p.dir_out / fname

    CNVfn.cnv2igv(list_f_cnv, f_out)

@decorators.IO
@decorators.suffix
def exec_cnv_normalize(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --f_norm        normilization file
                                        --normby        grouping for normalization

        Output:
    """
    # Get path to normalization file
    p.f_norm = (ddata / p.f_norm).resolve()

    # Get files for specified bin size, lambda, and pattern
    list_f_cnv, _, _ = fUtils.match_seg_files(p.dir_in, p.bin_sizes, p.lamtha, p.Suffix, p.pattern) 

    # Output files
    list_f_out = [f.with_suffix('.norm') for f in list_f_cnv]

    # Build command
    kwargs = {'i': list_f_cnv, 'o': list_f_out, 'n': p.f_norm}
    cmd = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    # Execute command
    CmdUtils.execute(cmd, p.exec_loc, p)

@decorators.IO
@decorators.species
def exec_plot_bins_agg(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        segs directory (relative to ddata)
                                        --dir_bins      bins directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor (can only take one!)

        Output:
    """
    # Get bin dirs and combine into a string
    list_bin_dirs = fUtils.match_bin_dirs(p.dir_in, p.bin_sizes, p.pattern, p.invert_pattern)
    str_bin_dirs = '\"' + ' '.join([str(b) for b in list_bin_dirs]) + '\"'

    # Output file name
    subject = ddata.name
    fname = '%s_B%s' %(subject, '-B'.join([str(b) for b in p.bin_sizes]))

    if p.loc:
        fname += '_chr%s-%s' %(p.loc.split(':')[0], p.loc.split(':')[1])

    f_out = p.dir_out / (fname + '_agg.png')

    # Create and execute command
    kwargs = {"i": str_bin_dirs, "o": f_out, "loc": p.loc, "species": p.species, "chrm": p.chrm}
    cmd = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    CmdUtils.execute(cmd, p.exec_loc, p)

@decorators.IO
@decorators.species
@decorators.suffix
def exec_plot_bins_segs_agg(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        segs directory (relative to ddata)
                                        --dir_bins      bins directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor (can only take one!)
                                        --cutoff        significance cutoff for segs
                                        --genes         plot with gene annotation?

        Output:
    """
    dir_bin, _ = fUtils.dir_path_build_and_check(ddata, p.dir_bin, p.dir_out)

    # Get seg files and combine into a string
    list_seg_files, list_bin_sizes, _ = fUtils.match_seg_files(p.dir_in, p.bin_sizes, [p.lamtha], p.Suffix, p.pattern, p.invert_pattern)
    str_seg_files = '\"' + ' '.join([str(f) for f in list_seg_files]) + '\"'

    # Get bin dirs and combine into a string
    list_bin_dirs = fUtils.bin_dirs_from_seg_files(list_seg_files, dir_bin, list_bin_sizes)
    str_bin_dirs = '\"' + ' '.join([str(b) for b in list_bin_dirs]) + '\"'

    # Output file name
    subject = ddata.name
    fname = '%s_B%s_l%i' %(subject, '-B'.join([str(b) for b in p.bin_sizes]), p.lamtha)

    if p.loc:
        fname += '_chr%s-%s' %(p.loc.split(':')[0], p.loc.split(':')[1])

    f_out = p.dir_out / (fname + '_agg.pdf')

    # Build and execute command
    kwargs = {'i': str_bin_dirs, 'o': f_out, 's': str_seg_files, "loc": p.loc, 'c': p.cutoff, 'species': p.species, 'genes': p.genes, 'chrm': p.chrm}
    print kwargs
    cmd = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    CmdUtils.execute(cmd, p.exec_loc, p)

@decorators.IO
@decorators.suffix
def exec_plot_segs_agg(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        segs directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     size(s) of bins
                                        --lamtha        smoothing factor (can only take one!)

        Output:
    """
    # Get seg files and combine into a string
    list_seg_files, _, _ = fUtils.match_seg_files(p.dir_in, p.bin_sizes, p.lamtha, p.Suffix, 
                                            p.pattern, p.invert_pattern)
    str_seg_files = '\"' + ' '.join([str(f) for f in list_seg_files]) + '\"'

    # Output file name
    subject = ddata.name
    fname = '%s_B%s_l%s' %(subject, '-B'.join([str(b) for b in p.bin_sizes]), '-l'.join([str(l) for l in p.lamtha]))

    if p.loc:
        fname += '_chr%s' %p.loc.split(':')[0]

    f_out = p.dir_out / (fname + '_seg-agg.pdf')

    # Create and execute command
    kwargs = {"i": str_seg_files, "o": f_out, "loc": p.loc}
    cmd = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    CmdUtils.execute(cmd, p.exec_loc, p)

@decorators.IO
def exec_bam_avg_depth(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --pattern       additional pattern for file matching
                                        --recursive     search for files recursively?
                                        --n_procs       number of processors

        Output:
    """
    # Get bam files
    list_bam = fUtils.file_glob(p.dir_in, '.bam', pattern=p.pattern, invert_pattern=p.invert_pattern, recursive=p.recursive)

    if p.dir_out:
        list_f_out = fUtils.dir_mirror_structure(list_bam, p.dir_in, p.dir_out, '.depth')
    else:
        list_f_out = [None for f in list_bam]

    # Generate Commands
    args = [list_bam, list_f_out]
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, *args)

    # Call round robin distributor
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_depth_to_pgram(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --smooth        smoothing factor
                                        --taper         lenth of taper

        Output:
    """
    # Get input files
    list_f_in = fUtils.file_glob(p.dir_in, '.depth', pattern=p.pattern, recursive=p.recursive)

    # Preserve input directory structure in output directory
    list_f_out = fUtils.dir_mirror_structure(list_f_in, p.dir_in, p.dir_out, '.pgram')

    # Generate Commands
    kwargs = {'i': list_f_in, 'o': list_f_out, 's': p.smooth, 't': p.taper}
    cmd_list = CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, **kwargs)

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

@decorators.IO
def exec_figure_glob(ddata, p):
    """ Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --suffix        file extension to grep for

        Output:
    """
    # Get input files
    list_f_in = fUtils.file_glob(p.dir_in, p.suffix, pattern=p.pattern, recursive=True)

    for f in list_f_in:
        shutil.copy(str(f), str(p.dir_out / f.name))
