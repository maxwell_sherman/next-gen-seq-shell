# CNVfn.py - Classes, functions, and methods for CNV analysis
#
# v 0.6.0
# rev 2016-07-25 (MS: merged cmdGeneric into master)
# Notes: 

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import multiprocessing as mp
import pathlib2 as pathlib
import itertools as it

import FileUtils as fUtils
import BamUtils
import perlUtils
import CmdUtils

def chrom_label(chrom):
    try:
        label = int(chrom.split('r')[-1])
    except ValueError:
        label = str(chrom.split('r')[-1])

    return label

class CNV(object):
    """ Class for loading and manipulating CNV segmentation data

        init args:
            path_to_cnv     pathlib Path    path to CNV segmentation file
            cutoff          float [0-1]     copy number cutoff for segmentation significance

        init returns:
            self.data       pandas table    "chrom start end length bin ratio pvalue name"
                                               1     0   100  100    1   0.01  0.54  subj
            self.sig        pandas table    same as self.data but only includes CNV segs
                                            with abs(ratio) above a given threshold
    """
    def __init__(self, path_to_cnv=None, dataframe=None, cutoff=0.3):
        if path_to_cnv:
            self.path_to_cnv = path_to_cnv
            self.__load_data_from_file()
            self.__calc_seg_length()
            self.__add_name()

        elif type(dataframe) != 'NoneType':
            self.path_to_cnv = pathlib.Path.cwd()
            self.__load_data_from_frame(dataframe)

        else:
            ValueError("Must supply path_to_cnv or a dataframe")

        self.rename_column('log2.copyRatio', 'ratio')
        self.stats = self.basic_stats(cutoff)
        self.sig = self.segs_significant(cutoff)

    def __load_data_from_file(self):
        if self.path_to_cnv.suffix == '.csv':
            self.data = pd.read_csv(str(self.path_to_cnv))

        else:
            self.data = pd.read_table(str(self.path_to_cnv))

        self.data['chrom_label'] = self.data['chrom'].apply(chrom_label)

    def __load_data_from_frame(self, dataframe):
        self.data = dataframe
        self.data['chrom_label'] = self.data['chrom'].apply(chrom_label)

    def __calc_seg_length(self):
        self.data['length'] = self.data['end'] - self.data['start']

    def __add_name(self):
        name = self.path_to_cnv.name.split('.')[0]
        self.data['name'] = name

    def rename_column(self, old_name, new_name):
        self.data.rename(columns={old_name: new_name}, inplace=True)
        
    def basic_stats(self, cutoff):
        stats = {}

        stats['nseg']  = self.count_segs()
        stats['nsig']   = self.count_segs_significant(cutoff)
        stats['length'] = self.data['length'].describe()
        stats['ratio']  = self.data['ratio'].describe()

        return stats

    def count_segs(self):
        return self.data['chrom'].count()

    def count_segs_significant(self, cutoff):
        mask = abs(self.data['ratio']) > cutoff

        return pd.Series({'count': self.data['ratio'][mask].count(),
                          'threshold': cutoff})

    def segs_significant(self, cutoff):
        mask = abs(self.data['ratio']) > cutoff

        return self.data[mask]

    def normalize(self, f_normalize, by='segs'):
        if by == 'segs':
            self.normalize_per_seg(f_normalize)

        elif by == 'chrm':
            self.normalize_per_chrm(f_normalize)

        else:
            raise TypeError("Didn't recognize \'by\' option. Choose from [\'segs\', \'chrm\']") 

    def normalize_per_chrm(self, f_normalize):
        # Load normalization data as a CNV object and get average copy number per chrm
        # NOTE: Normalization is done on real copy number ratios, not log2 ratios 
        cnv = self.__class__(path_to_cnv=f_normalize)
        cnv.log2real()
        chrm_avg = cnv.ratio_avg_per_chrm()

        # Data to be normalized as real ratio
        self.log2real()

        # Do the normalization (divide each segment by average of that chrm from normalization file)
        data = self.data.copy()
        ratio = [(row[1]['ratio'] / chrm_avg[row[1]['chrom']]) for row in data.iterrows()]
        self.data['ratio'] = ratio

        # Convert ratios back to log2 scale
        self.real2log()

        # data['ratio'] = self.data['ratio_norm']

        # return data

    def normalize_per_seg(self, f_normalize):
        cnv = self.__class__(path_to_cnv=f_normalize)
        cnv.log2real()

        segs = zip(self.data['chrom_label'], self.data['start'], self.data['end'])
        seg_avg = cnv.ratio_avg_per_seg(segs)
        grouped = seg_avg.groupby('chrom_label')
        # print grouped.get_group(18)[['start', 'end', 'ratio']]
        
        self.log2real()
        # grouped = self.data.groupby('chrom')
        # print grouped.get_group("chr1")[['start', 'end', 'ratio']]

        self.data['ratio'] = self.data['ratio'] / seg_avg['ratio']
        # grouped = self.data.groupby('chrom')
        # print grouped.get_group("chr1")[['start', 'end', 'ratio']]
        self.real2log()
        
    def ratio_avg_per_chrm(self):
        grouped = self.data.groupby('chrom')

        # Perform weighted average of seg ratio per chrm where 
        # weights are seg proportion of total chrm length
        # Sorry for how ugly this is
        return grouped.agg(lambda x: np.average(x['ratio'],
                                                weights=x['length']/x['length'].sum()))['ratio']

    def ratio_avg_per_seg(self, seg_list):
        """ Average ratios across defined segments

            Input:
                seg_list        list            list of tuples of form (seg_start, seg_end)

            Output:
                df_avg          data frame      columns 'start', 'end', 'ratio' corresponding to
                                                seg_start, seg_end, and avg ratio for that seg
        """
        df_avg = pd.DataFrame(columns=['chrom_label', 'start', 'end', 'ratio'])

        for seg in seg_list:
            i_start = self.data.index[(self.data['chrom_label'] == seg[0]) & (self.data['start'] == seg[1])][0]
            i_end = self.data.index[(self.data['chrom_label'] == seg[0]) & (self.data['end'] == seg[2])][0]

            df = self.data.ix[i_start:i_end]
            df_avg = df_avg.append({'chrom_label': df.iloc[0]['chrom_label'],
                                    'start': df.iloc[0]['start'],
                                    'end'  : df.iloc[-1]['end'],
                                    'ratio': np.average(df['ratio'], weights=df['length']/df['length'].sum())
                                    # 'ratio': df['ratio'].mean()
                                   }, ignore_index=True)

        return df_avg

    def log2real(self, key='ratio'):
        """ log2 copy ratio to real copy ratio
        """
        self.data[key] = self.data[key].apply(lambda x: 2**x)

    def real2log(self, key='ratio'):
        """ Real copy number ratio to log2 copy ratio
        """
        self.data[key] = self.data[key].apply(np.log2)

    def igv_format(self):
        return self.data[['name', 'chrom_label', 'start', 'end', 'ratio']]

    def save_igv_format(self, fname):
        if fname.suffix != '.cbs':
            fname.with_suffix('.cbs')

        df = self.ivg_format()
        df.to_csv(str(fname), index=None, sep='\t')

    def save(self, fname):
        self.data.to_csv(str(fname), index=False, sep='\t')

def cnv_mapd_correlate(list_f_cnv, list_f_mapd, f_out, title='mapd_cnv_correlate'):
    # Create data frame
    df = pd.DataFrame(columns=('mapd', 'nseg', 'nsig', 'fcnv'))

    for f_cnv, f_mapd in zip(list_f_cnv, list_f_mapd):
        mapd = pd.read_table(str(f_mapd), header=None)
        cnv = CNV(f_cnv)

        df = df.append({'mapd': mapd[0].values[0], 
                        'nseg': cnv.stats['nseg'], 
                        'nsig': cnv.stats['nsig']['count'],
                        'fcnv': f_cnv.name
                       }, ignore_index=True)

    pd.tools.plotting.scatter_matrix(df.loc[:, ['mapd', 'nseg']], diagonal='hist')
    plt.suptitle(title)
    plt.savefig(str(f_out))
    plt.close()

    f_csv = f_out.with_suffix('.csv')
    df.to_csv(str(f_csv)) 

def bins2cnvs_singlesample(ddata, p):
    """ Perform segmentation on a single bin file. Optionally segment against a control genome

        Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     desired size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --read_length   length of reads
                                        --build         genome build to compare against
                                        --control       bin dir of control genome(s)
                                        --noscale       do not automatically scale lambda
                                        --noBootstrap   do not assign p-values via a boostrap

        Output:
    """
    # Get path to control dirs and look for sub dirs
    if p.control:
        p.control = (ddata / p.control).resolve()
        sub_dirs_ctrl = fUtils.get_sub_dirs(p.control, invert_pattern=['/b[0-9]+$'])
        p.control=True
    else:
        sub_dirs_ctrl = [None]
        p.control=False

    # Get sub dirs in dir_in
    sub_dirs = fUtils.get_sub_dirs(p.dir_in, p.pattern, p.invert_pattern)

    # Perform analysis per bin size
    for bin_size in p.bin_sizes:
        cmd_list = []

        # Iterate through case and control dirs. 
        # If more cases than controls (i.e. non-matched), uses first control as normalizer
        # once iteration of controls exhausted
        for sub_dir, d_ctrl in it.izip_longest(sub_dirs, sub_dirs_ctrl, fillvalue=sub_dirs_ctrl[0]):
            # Get bin files
            list_f_in = fUtils.file_glob(sub_dir, '.b%i.bin' %bin_size, pattern=p.pattern, recursive=p.recursive)

            if d_ctrl:
                list_f_cont = fUtils.file_glob(d_ctrl, '.b%i.bin' %bin_size, recursive=True)
            else:
                list_f_cont = None

            for lamtha in p.lamtha:
                if d_ctrl:
                    d_out = p.dir_out / (sub_dir.name.split('.')[0] + '_vs_' + 
                            list_f_cont[0].name.split('.')[0]) / \
                            ('b%i' %bin_size) / ('lambda_%02d' % lamtha)
                else:
                    d_out = p.dir_out / sub_dir.name.split('.')[0] / \
                            ('b%i' %bin_size) / ('lambda_%02d' % lamtha)

                fUtils.check_dir_out(d_out)

                # Output file names
                config_name_short = sub_dir.name.split('.')[0] + '__B%03d_l%02d.cfg' %(bin_size, lamtha) 
                f_config = d_out / config_name_short
                f_bicseq_out = f_config.with_suffix('.BICseq.out')
                f_gene_out = f_config.with_suffix('.genes')
                f_fig_out = f_config.with_suffix('.BICseq.seg.png')

                # Generate config file
                perlUtils.BICseq_config_single(list_f_in, f_config, list_f_cont, p.species)

                # Create commands
                args = [[f_config], [f_bicseq_out]]
                kwargs = {'lambda': lamtha, 'tmp': str(p.tmp_dir), 'fig': f_fig_out, 'title': config_name_short, 'noscale': p.noscale, 'bootstrap': p.bootstrap, 'control': p.control}
                cmd_list.extend(CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, *args, **kwargs))
                # kwargs = {'perlscript': p.script, 'bootstrap': '', 'lambda': lamtha, 'tmp': str(p.tmp_dir), 'fig': f_fig_out, 'title': config_name_short, 'noscale': p.noscale, 'bootstrap': p.bootstrap, 'control': p.control}
                # cmd_list.extend(CmdUtils.generate_IO_cmds([f_config], [f_bicseq_out], perlUtils.perl_IO_cmd, **kwargs))
                # kwargs = {'script': p.script, 'lambda': lamtha, 'tmp': str(p.tmp_dir), 'fig': f_fig_out, 'title': config_name_short, 'noscale': p.noscale, 'bootstrap': p.bootstrap, 'control': p.control}

        # Execute commands
        CmdUtils.execute(cmd_list, p.exec_loc, p)

def bins2cnvs_multisample(ddata, p):
    """ Perform segmentation on a multiple bin files simultaneously

        Input:
            ddata:  pathlib Path    current data directory
            p:      argparser       parameters
                                        --dir_in        input directory (relative to ddata)
                                        --dir_out       output directory (relative to ddata)
                                        --bin_sizes     desired size(s) of bins
                                        --lamtha        smoothing factor(s)
                                        --read_length   length of reads
                                        --build         genome build to compare against
                                        --control       bin dir of control genome(s)
                                        --noscale       do not automatically scale lambda
                                        --noBootstrap   do not assign p-values via a boostrap

        Output:
    """
    # Perform analysis per bin size
    for bin_size in p.bin_sizes:
        cmd_list = []

        list_f_in = fUtils.file_glob(p.dir_in, '.b%i.bin' %bin_size, pattern=p.pattern, recursive=p.recursive, invert_pattern=p.invert_pattern)

        # Group files by chromosome and get list of subjects
        chrms = BamUtils.get_chrm_numbers(species=p.species)
        chrm_labels = BamUtils.get_chrm_labels(chrms)

        file_dict = {'chr%s' %c: [f for f in list_f_in if l in f.name] for c, l in zip(chrms, chrm_labels)}
        list_subjects = [f.name.split('.')[0].split('-')[0] for f in file_dict['chr%s' %chrms[0]]]

        # Group by lambda
        for lamtha in p.lamtha:
            if len(list_subjects) < 5:
                basename = "-".join(list_subjects)

            else:
                basename = fUtils.iter_name(p.dir_out, 'multisample_%02d' % len(list_subjects), '').name

            print basename

            d_out = p.dir_out / basename / \
                    ('b%i' %bin_size) / ('lambda_%02d' % lamtha)

            fUtils.check_dir_out(d_out)

            # Output file names
            config_name_short = basename + '__B%03d_l%02d.cfg' %(bin_size, lamtha) 
            f_config = d_out / config_name_short
            f_bicseq_out = f_config.with_suffix('.BICseq.out')
            f_gene_out = f_config.with_suffix('.genes')
            f_fig_out = f_config.with_suffix('.BICseq.seg.png')

            # Generate config file
            perlUtils.BICseq_config_multi(file_dict, f_config, p.species)

            # Create commands
            args = [[f_config], [f_bicseq_out]]
            kwargs = {'lambda': lamtha, 'tmp': str(p.tmp_dir), 'fig': f_fig_out, 'title': config_name_short, 'noscale': p.noscale, 'bootstrap': p.bootstrap, 'control': p.control}
            cmd_list.extend(CmdUtils.generate_cmds(CmdUtils.build_generic_cmd, p.script, *args, **kwargs))
            # kwargs = {'perlscript': p.script, 'bootstrap': '', 'lambda': lamtha, 'tmp': str(p.tmp_dir), 'fig': f_fig_out, 'title': config_name_short, 'noscale': p.noscale, 'bootstrap': p.bootstrap, 'control': p.control}
            # cmd_list.extend(CmdUtils.generate_IO_cmds([f_config], [f_bicseq_out], perlUtils.perl_IO_cmd, **kwargs))
            # kwargs = {'script': p.script, 'lambda': lamtha, 'tmp': str(p.tmp_dir), 'fig': f_fig_out, 'title': config_name_short, 'noscale': p.noscale, 'bootstrap': p.bootstrap, 'control': p.control}

    # Execute commands
    CmdUtils.execute(cmd_list, p.exec_loc, p)

def cnv2igv(flist, fname):
    columns = ['name', 'chrom_label', 'start', 'end', 'ratio']
    df = pd.DataFrame(columns=columns)

    for f in flist:
        cnv = CNV(f)
        df = df.append(cnv.igv_format())

    if fname.suffix != '.cbs':
        fname.with_suffix('.cbs')

    df['start'] = df['start'].astype(int)
    df['end'] = df['end'].astype(int)
    # df['end'].apply(int)
    print df['start'][1:10]

    df.to_csv(str(fname), index=None, sep='\t')

if __name__ == "__main__":
    f1 = pathlib.Path('/home/mas138/orchestra/data/BSM/ASD/40x/cnvs/UMB1445-20150603-PFC-1b1-20150615-WGS/b10000/lambda_500/UMB1445-20150603-PFC-1b1-20150615-WGS__B10000_l500.BICseq.out')
    f2 = pathlib.Path('/home/mas138/orchestra/data/BSM/ASD/40x/cnvs/UMB4231-20150608-PFC-1b1-20150615-WGS/b10000/lambda_500/UMB4231-20150608-PFC-1b1-20150615-WGS__B10000_l500.BICseq.out')
    fout = pathlib.Path('/home/mas138/orchestra/data/BSM/ASD/40x/cnvs/test.csv')
    list_f_cnv = [f1, f2]

    cnv_overlap(list_f_cnv, fout)
    # grouped, df = cnv_overlap(list_f_cnv)
