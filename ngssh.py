#!/usr/bin/env python

# ngssh.py - top level executible for [n]ext [g]eneration [s]equencing [SH]ell
#
# v 0.4.6
# rev 2016-07-16 (MS: history file created if it does not exist)
# Notes: changed name of history file

# Standard Modules
import os, readline, sys
import argparse

# Local Modules
from fn import arg_parser
from fn.cli import ngsSH

if __name__ == "__main__":

    # Readline goodness
    readline.parse_and_bind('set editing-mod vi')
    readline.parse_and_bind('\C-l: clear-screen')
    readline.parse_and_bind('tab: complete')

    # Local history file
    fhist_local = '.ngssh_hist_local'
    # fhist_local = '.data_hist_local'

    if not os.path.isfile(fhist_local):
        f = open(fhist_local, 'w+')
        f.close()

    # Read local history file
    readline.read_history_file(fhist_local)

    # Reverse history search: Apple
    if sys.platform.startswith('darwin'):
        readline.parse_and_bind('"^[[A": history-search-backward')
        readline.parse_and_bind('"^[[B": history-search-foward')

    # Reverse history search: Linux
    if sys.platform.startswith('linux'):
        readline.parse_and_bind('"\e[A": history-search-backward')
        readline.parse_and_bind('"\e[B": history-search-foward')

    # Parse arguments
    parser = arg_parser.load_parsers()
    p = parser['argparse'].parse_args()

    # Run the console
    ngssh = ngsSH(p.dproj)
    ngssh.cmdloop()
