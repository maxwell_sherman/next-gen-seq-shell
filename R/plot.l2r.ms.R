#' Plot CNV segmentation with log2 ratio of each bin in the background
#' For help: plot.l2r.ms.R -h 
#'
#' Author: Maxwell Sherman & Semin Lee (?)
#' Revised: 03-17-16
#' Revision notes:

library("parallel")
library("Cairo")
library("optparse")

plot.l2r <- function(bin.dir.tmr, seg.file, bin.size, lamda, 
                     mouse, title, xy=1, endrule="median", k=3) {
    ## Plots CNV segments with log2 ratio of each bin in the backgroun
    ##
    ##  Args:
    ##      bin.dir.tmr     str     path to directory of bin data
    ##      seg.file        str     path to segmentation file
    ##      bin.size        int     size of bins
    ##      lamda           int     lambda smoothing factor
    ##      mouse           bool    is this mouse data?
    ##      title           str     title of plot
    ##      xy              bool    plot X chrm?
    ##      endrule         str     what to do with endpoints
    ##      k               int     number of neighboring points to include in
    ##                              running median
    ##
    ##  Returns:
    ##      --              --      Saves plot

    if (mouse == 1) {
        chrl <- c(1:19, "X")
    } else if (xy) {
        chrl <- c(1:22, "X")
    } else {
        chrl <- c(1:22)
    }

    names(chrl) <- chrl

    # Read bin files
    # start   end     obs     expected        var
    # 61762   78721   12      16.35   72.4539
    cntl.tmr <- lapply(chrl, function(c) {
           if (suppressWarnings(all(is.na(as.numeric(c))))) {
               file <- file.path(bin.dir.tmr, 
                                list.files(path=bin.dir.tmr, 
                                           pattern=sprintf("*.chrm_%s.*b%03d.*", c, bin.size))[1])
           } else {
               file <- file.path(bin.dir.tmr, 
                                list.files(path=bin.dir.tmr, 
                                           pattern=sprintf("*.chrm_%02d.*b%03d.*", 
                                                           as.numeric(c), bin.size))[1])
           }
           cnt <- scan(file, list(1,1,1,1.1,1.1), sep="\t", skip=1)
           names(cnt) <- c("start", "end", "obs", "expected", "var")
           return(cnt) 
        }  # End function
    )      # End lapply

    # Total tumor and normal cnt
    # In BICseq2, observed counts treated as tumor, expected as normal
    tc <- 0
    nc <- 0

    # Get the ratio of total tumor and normal read counts
    for (i in chrl) {
        tc <- tc + sum(cntl.tmr[[i]]$obs)
        nc <- nc + sum(cntl.tmr[[i]]$expected)
    }

    # Calculate [1/(tc/nc)] for entire genome for normalization purposes
    R <- nc / tc

    # Create data frame of bin edges and copy number ratio
    # chrom    start    end    ratio
    #   1          0    10000   0
    #   1      10001    20000   0.5
    bins <- data.frame(do.call(rbind, 
                              lapply(chrl,
                                     function(c) {
                                       print(paste("processing", c))
                                       r <- (cntl.tmr[[c]]$obs + 0.00000001) / 
                                                (cntl.tmr[[c]]$expected + 0.0000001) * R
                                       ec <- sub("chr", "", c)
                                       return(cbind(ec, cntl.tmr[[c]]$start, 
                                                    cntl.tmr[[c]]$end, log2(r))) 
                                     }  # End function
                                    )   # End lapply
                             )          # End do.call
                      )                 # End data.frame

    # Name columns. Ensure chrom is character, start / end integers, and ratio a float
    colnames(bins) <- c("chrom", "start", "end", "ratio")
    bins$chrom <- as.character(bins$chrom)
    bins$start <- as.integer(as.character(bins$start))
    bins$end   <- as.integer(as.character(bins$end))
    bins$ratio <- as.numeric(as.character(bins$ratio))

    segments <- read.table(seg.file, sep="\t", header=T, stringsAsFactors=F) 

    #chrom   start   end     binNum  observed        expected        log2.copyRatio
    #chr1    10027   990262  216     7163    4923.08 0.153223364203753
    segments <- segments[,c(1,2,3,7)]
    colnames(segments) <- c("chrom", "start", "end", "ratio")
    segments$chrom <- sub("chr", "", segments$chrom)

    # Plot
    pngf <- plotRatioNSeg(seg.file, bins, segments, title, bin.size, lamda, endrule=endrule, k=k) 
    print(paste("done plotting", pngf))
}

plotRatioNSeg <- function(seg.file, bins, segments, sampleName=NULL, bin.size, lamda, 
                         save = TRUE, endrule = c("none", "median", "keep", "constant"), 
                         k = 3, indexOnly = FALSE) {

    ## Actual plotting routine to plot CNV segs and bin ratios

    endrule     <- match.arg(endrule)
    graphList   <- list()
    locations   <- alignGenes(bins[, c("chrom", "start")], indexOnly = indexOnly)
    adjustments <- getAdjustments(bins[, c("chrom", "start")], indexOnly = indexOnly)
    num.segs    <- dim(segments)[1]

    if (save) {
        seg.dir <- dirname(seg.file)
        graphList[[sampleName]] <- file.path(seg.dir, paste(c(head(strsplit(basename(seg.file), 
                                                                           ".", fixed=T)[[1]], 
                                                                  n=-1), 
                                                             "l2r.png"), 
                                                            collapse="."))
        CairoPNG(filename = graphList[[sampleName]], width = 1800, height = 500)
    } 

    # Title generation
    main <- paste(sampleName, " ( b", bin.size, " : k", k, " : l", lamda, 
                  " : s", num.segs, " )", sep="")

    # Create plot and axis options
    graphics::plot(0, 0, type="n", main=main, xlab="Chromosome", ylab=" Log2 ratio", 
                   ylim=c(-5, 5), axes=FALSE, xlim=c(0, max(locations) + 10), cex.lab=1.2)

    # Create axis (on the left of plot) and draw a box arond it
    axis(2)
    box()

    # Add colored boxes around odd numbered chromosomes
    HighLightChrom(adjustments, -5, 5)

    # Plot bin ratios
    if (endrule != "none") {
        nas <- which(is.na(as.numeric(bins[, "ratio"])))
        if (length(nas) != 0) {
            points(locations[-nas], runmed(as.vector(bins[-nas, "ratio"]), 
                                           k=k, endrule=endrule), 
                   cex = 0.3, pch = 16, col='grey')
        } else {
            # points(locations, runmed(as.vector(bins[, "ratio"]), k=k, endrule=endrule), 
            #        cex=0.3, pch=16, col='grey80')
            points(locations, runmed(as.vector(bins[, "ratio"]), k = k, endrule = endrule), 
                   cex = 0.3, pch = 16, col=rgb(red=0.83, green=0.83, blue=0.83, alpha=0.7))
        }
    } else {
        points(locations, as.numeric(as.vector(bins[, "ratio"])), 
               cex=0.3, pch=16, col='grey80')
    }

    # Add lines at -1, 0, +1
    lines(c(min(locations), max(locations)), rep(0, 2), lwd = 2, col = "blue")
    lines(c(min(locations), max(locations)), rep(1, 2), col = "blue")
    lines(c(min(locations), max(locations)), rep(-1, 2), col = "blue")

    # Plot segments
    if (!indexOnly) {
        drawSegs(segments, adjustments)  # This is default
    } else {
        for (index in 1:nrow(temp)) {
            positions = range(locations[which(as.character(bins[, "chrom"]) ==
                                              as.character(temp[index, "chrom"]) &
                                              as.numeric(bins[, "start"]) <
                                              as.numeric(segments[index, "end"]) &
                                              as.numeric(bins[, "end"]) >
                                              as.numeric(segments[index, "start"]))])
            lines(c(positions[1], positions[2]), rep(segments[index, "ratio"], 2), col = "red", lwd = 3)
        }
    }

    # Label odd chromosomes
    markChrom(adjustments, -5)

    # Save plot
    if(save){
        dev.off()
    }
    if(save){
        return(graphList)
    }else{
        return(invisible())
    }
}

alignGenes = function(positions, indexOnly = FALSE){
    if(indexOnly){
        return(1:nrow(positions))
    }else{
        adjustments = getAdjustments(positions, indexOnly)
        for(chrom in names(adjustments)){
            positions[positions[, 1] == chrom, 2] =
                as.numeric(positions[positions[, 1] == chrom, 2]) +
                as.numeric(adjustments[chrom])
        }
        return(as.numeric(positions[, 2]))
    }
}

# This function gets the values that can be used to adjust chromosomal
# locations to align genes on different chromosomes so that they  appear
# in sequence from chromosome one to Y along a single strand
getAdjustments = function(positions, indexOnly = FALSE){
    if(indexOnly){
        temp = split.data.frame(positions, factor(positions[, 1]))
        temp = unlist(lapply(temp, FUN = function(x) nrow(x) + 1))
    }else{
        temp = split.data.frame(positions, factor(positions[, 1]))
        temp = unlist(lapply(temp, FUN = function(x) max(as.numeric(x[, 2]))))
    }
    # Chromosomes to 30 for other organisms (e. g. fish - 25)
    chroms = sort(as.numeric(names(temp)[names(temp) %in% 1:30]))
    if(any(names(temp) %in% "X")){
        chroms = c(chroms, "X")
    }
    if(any(names(temp) %in% "Y")){
        chroms = c(chroms, "Y")
    }
    adjustments = 0
    for(index in 1:length(chroms) - 1) {
        adjustments = c(adjustments, (adjustments[length(adjustments)]+
                                       temp[as.character(chroms[index])]))
    }
    names(adjustments) = chroms
    return(adjustments)
}

HighLightChrom <- function(adjustments, min, max) {
    for (index in 1:length(adjustments)) {
        if (index %% 2 == 1) {
            polygon(c(adjustments[index], adjustments[index + 1],
                      adjustments[index + 1], adjustments[index]),
                    c(min, min, max, max), col = "wheat1", border = "white")
        }
    }
    return(invisible())
}

drawSegs = function(segdata, adjustments, seqOnly = TRUE){
    drawSegLine = function(segLocs){
        toAdd = adjustments[as.character(as.vector(segLocs["chrom"]))]
        lines(c(as.numeric(segLocs["start"]) + toAdd,
                as.numeric(segLocs["end"]) + toAdd),
              rep(segLocs["ratio"], 2), col = "red", lwd = 3)
    }
    junck = apply(segdata, 1, FUN = drawSegLine)
    return(invisible())
}

markChrom = function(adjustments, min){
    chromLocs = NULL
    chromNames = NULL
    for(i in 1:length(adjustments) - 1){
        if(i %% 2 == 1){
            chromLocs = c(chromLocs, mean(c(adjustments[i], adjustments[i + 1])))
            chromNames = c(chromNames, names(adjustments)[i])
        }
    }
    text(chromLocs, rep(min - 0.125, length(chromLocs)), chromNames, cex = 1.2)
    return(invisible())
}

# Create parser options
option_list = list(
    make_option(c('-i', '--bin_dir'), type="character", default=NULL, help="input bin dir"),
    make_option(c('-o', '--seg_file'), type="character", default=NULL, help="BICseq output file"),
    make_option(c('--bin_size'), type="integer", default=100, help="BICseq bin size"),
    make_option(c('--lamda'), type="integer", default=3, help="lambda of BICseq algorithm"),
    make_option(c('--mouse'), type="integer", default=0, help="mouse?"),
    make_option(c('--title'), type="character", default=NULL, help="figure title")
)

# Create parser and parse command line arguments
opt_parser = OptionParser(option_list=option_list)
opts = parse_args(opt_parser)

bin.dir.tmr = opts$bin_dir
seg.file    = opts$seg_file
bin.size    = opts$bin_size
lamda       = opts$lamda
mouse       = opts$mouse
title       = opts$title
xy          = TRUE
k           = 5 # integer width of median window, must be odd

if (length(args) == 7)  { xy   = args[[7]] }
if (length(args) > 7)   { k    = as.numeric(args[[8]]) }

plot.l2r(bin.dir.tmr, seg.file, bin.size, lamda, mouse, title, xy=xy, k=k)
