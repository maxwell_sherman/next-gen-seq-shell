#' Plot aggregated cnv profile based on value of ratio
#' For help: plotProfile_sig.R -h 
#'
#' Author: Maxwell Sherman (Inspired by Ruibin Xi)
#' Revised: 05-23-16
#' Revision notes: --ymax command line argument added

library("Cairo")
library("ggplot2")
library("optparse")


MarkChromBoundary <- function(gplot, adjustments){
  # Marks boundaries of chromosomes with black dotted lines 
  #
  # Inputs:
  #    gplot:           ggplot2 plot    --
  #    adjustments:     vector          boundaries of chromosomes
  #
  # Returns:
  #    gplot:           ggplot2 plot
  gplot <- gplot + geom_vline(xintercept=adjustments, linetype='dotted')

  return(gplot)
}

LabelChrom <- function(gplot, adjustments, min, max, chromNames, height=10) {
  # Labels chromosomes. Even chrms at top of plot; odd chrms at bottom of plot
  #
  # Inputs:
  #    gplot:           ggplot2 plot    --
  #    adjustments:     vector          boundaries of chromosomes
  #    min              scalar          minimum y value
  #    max              scalar          maximum y value
  #    chromNames       list            list of chrm names
  #
  # Returns:
  #    gplot:           ggplot2 plot

  for(i in 1:(length(adjustments) - 1)) {
    if(i %% 2 == 1){
      y.pos = min
    } else {
      y.pos = max
    }

    x.pos = mean(c(adjustments[i], adjustments[i+1]))
    gplot <- gplot + annotate("text", label=chromNames[i], x=x.pos, y=y.pos)
  }
  return(gplot)
}

addHlines <- function(gplot, y_intercept, linetype='dashed', col='blue') {
  # Add horizontal lines at specified y values
  #
  # Inputs:
  #    gplot:           ggplot2 plot        --
  #    y_intercept:     scalar / vector     y values of lines
  #    linetype         character           type of line to use
  #    col              character           color of line
  #
  # Returns:
  #    gplot:           ggplot2 plot

  gplot <- gplot + geom_hline(yintercept=y_intercept, linetype=linetype, col='blue')

  return(gplot)
}

##### Start Main Statement ######

# Create option list for parser
option_list = list(
    make_option(c('-i', '--seg.file'), type="character", default=NULL, help="input seg file"),
    make_option(c('-o', '--fig.name'), type="character", default=NULL, help="output figure name"),
    make_option(c('--title'), type="character", default=NULL, help="figure title"),
    make_option(c('--ymax'), type="numeric", default=NULL, help="y-limit maximum")
)

# Create parser and parse command line arguments
opt_parser = OptionParser(option_list=option_list)
opts = parse_args(opt_parser)

segfile = opts$seg.file
figname = opts$fig.name
title   = opts$title

# Read seg file
segs = read.table(segfile,header=T)
# segs = read.csv(segfile,header=T)

# Create PNG
CairoPNG(filename=figname, width=1200, height=500)

# Get chromosomes, # chrom, # segs / chrom, boundaries of chroms
# NOTE: chrom.code loads chromosomes as they are ordered in the seg file
chrom.code = aggregate(1:nrow(segs), by=list(segs$chrom), min)
rk = order(chrom.code[ , 2])
chrom.length = aggregate(as.numeric(segs$end), by=list(segs$chrom), max)
chrom.length = chrom.length[rk, ]
num.segs.chrom = aggregate(segs$end, by=list(segs$chrom), length)
num.segs.chrom = num.segs.chrom[rk, ]
chrom.boundary = c(0, cumsum(chrom.length[ , 2]))

# Create data frame for plotting
adjust = rep(chrom.boundary[-length(chrom.boundary)], num.segs.chrom[ , 2])
loc.start = segs$start + adjust
loc.end = segs$end + adjust
loc.pos = c(rbind(loc.start, loc.end))
loc.ratio = rep(segs$ratio, each=2)
loc.std = rep(segs$ratio_std, each=2)
loc.sig <- rep(0, length(loc.start), each=2)
loc.sig[abs(loc.ratio) > 0.2] = 1
loc.sig <- as.factor(loc.sig)
loc.group = rep(1:length(loc.start), each=2)
df <- data.frame(loc.pos, loc.ratio, loc.std, loc.sig, loc.group)

if (is.null(opts$ymax)) {
    ymax <- ceiling(max(abs(loc.ratio) + abs(loc.std))) + 1
} else {
    ymax <- opts$ymax
}
ymin <- -ymax
# ymax <- ceiling(max(loc.ratio + loc.std)) + 1
# ymin <- -ymax

# Plot
j <- ggplot(df, aes(x=loc.pos, y=loc.ratio, group=loc.group))
j <- MarkChromBoundary(j, chrom.boundary)
j <- addHlines(j, c(-1, 0, 1))

# j <- j + ylim(ymin, ymax) + 
j <- j + 
         xlab("Chromosome") + 
         ylab("Log2 ratio") + 
         ggtitle(title) +
         scale_x_discrete(breaks=NULL) +
         scale_y_continuous(breaks=ymin:ymax) +
         theme(panel.grid=element_blank()) +
         theme_bw(base_size=14)

# Use either discrete or continous color palette depending on number of segments
j <- j + geom_ribbon(aes(ymin=loc.ratio - loc.std, ymax=loc.ratio + loc.std, fill=loc.sig), 
               alpha=0.5) +
         geom_step(aes(color=loc.sig), size=1.5) + 
         scale_color_manual("sig", values=c('green', 'red'), labels=c('not sig', 'sig')) + 
         scale_fill_manual("sig", values=c('green', 'red'), labels=c('not sig', 'sig')) + ylim(ymin, ymax)  

# Label chromosomes
# NOTE: j <- LabelChrom(..) will make dev.off() not work
LabelChrom(j, chrom.boundary, ymin , ymax, chrom.length[,1])

# Save
dev.off()
