#' Calculate MAPD scores from BICSeq bin files
#' For help: mapdFromBins.R -h 
#'
#' Author: Maxwell Sherman
#' Revised: 08-15-16
#' Revision notes: using generic fns from R/fnUtils.R

library("optparse")

source("R/fnUtils.R")

# loadBinData <- function(bin.dir.tmr, loc=NULL) {
#     ## Plots CNV segments with log2 ratio of each bin in the backgroun
#     ##
#     ##  Args:
#     ##      bin.dir.tmr     str     path to directory of bin data
#     ##
#     ##  Returns:
#     ##      bins            df      chr start   end log2ratio   color   subject
#     print(sprintf("Processing: %s", bin.dir.tmr))
# 
#     chrl <- c(1:22, "X")
#     names(chrl) <- chrl
# 
#     # Read bin files
#     # start   end     obs     expected        var
#     # 61762   78721   12      16.35           72.4539
#     cntl.tmr <- lapply(chrl, function(c) {
#            if (suppressWarnings(all(is.na(as.numeric(c))))) {
#                file <- file.path(bin.dir.tmr, 
#                                 list.files(path=bin.dir.tmr, 
#                                            pattern=sprintf("*.chrm_%s.*", c))[1])
#                                            # pattern=sprintf("*.chrm_%s.*b%03d.*", c, bin.size))[1])
#            } else {
#                file <- file.path(bin.dir.tmr, 
#                                 list.files(path=bin.dir.tmr, 
#                                            pattern=sprintf("*.chrm_%02d.*", 
#                                            # pattern=sprintf("*.chrm_%02d.*b%03d.*", 
#                                                            as.numeric(c)))[1])
#            }
#            cnt <- scan(file, list(1,1,1,1.1,1.1), sep="\t", skip=1)
#            names(cnt) <- c("start", "end", "obs", "expected", "var")
#            return(cnt) 
#         }  # End function
#     )      # End lapply
# 
#     # Total tumor and normal cnt
#     # In BICseq2, observed counts treated as tumor, expected as normal
#     tc <- 0
#     nc <- 0
# 
#     # Get the ratio of total tumor and normal read counts
#     tc.chr <- sapply(chrl, function(c) {sum(cntl.tmr[[c]]$obs)})
#     nc.chr <- sapply(chrl, function(c) {sum(cntl.tmr[[c]]$expected)})
#     tc <- sum(tc.chr)
#     nc <- sum(tc.chr)
# 
#     # Calculate [1/(tc/nc)] for entire genome for normalization purposes
#     R <- nc / tc
# 
#     # Create data frame of bin edges and copy number ratio
#     # chrom    start    end    ratio
#     #   1          0    10000   0
#     #   1      10001    20000   0.5
#     bins <- data.frame(do.call(rbind, 
#                               lapply(chrl,
#                                      function(c) {
#                                        # print(paste("processing", c))
#                                        r <- (cntl.tmr[[c]]$obs + 0.00000001) / 
#                                                 (cntl.tmr[[c]]$expected + 0.0000001) * R
#                                        ec <- sub("chr", "", c)
#                                        df <- data.frame(ec, cntl.tmr[[c]]$start, 
#                                                     cntl.tmr[[c]]$end, log2(r))
#                                        return(df)
#                                      }  # End function
#                                     )   # End lapply
#                              )          # End do.call
#                       )                 # End data.frame
# 
#     # Name columns.
#     colnames(bins) <- c("chrom", "start", "end", "ratio")
# 
#     bins$copy <- rep(0, nrow(bins))
#     bins$copy[bins$ratio > 0.3] <- 1
#     bins$copy[bins$ratio < -0.3] <- -1
# 
#     file <- list.files(bin.dir.tmr)[1]
#     subject <- unlist(strsplit(basename(file), '-'))[1]
#     bins$subject <- subject
# 
#     if (! is.null(loc)) {
#         bins <- bins[(bins$chrom==loc$chrom) & (bins$start>loc$start) & (bins$end<loc$end), ]
#     }
# 
#     print(sprintf("Done processing: %s", bin.dir.tmr))
# 
#     return(bins)
# }


mapdCalc <- function(log2.copyRatio){
	ncnr_1 <- c(0, log2.copyRatio)

	n <- length(log2.copyRatio)
	b <- abs(log2.copyRatio - ncnr_1[1:n])

	return(median(b[2:n]))
}

# Create parser options
option_list = list(
    make_option(c('-i', '--bin.dir'), type="character", default=NULL, help="input bin dir"),
    make_option(c('-o', '--mapd.file'), type="character", default=NULL, help="Output file in which to save MAPD score"),
    make_option('--chrm', default=FALSE, action='store_true')
)

# Create parser and parse command line arguments
opt_parser = OptionParser(option_list=option_list)
opts = parse_args(opt_parser)

bins <- loadBinData(opts$bin.dir, chrm=opts$chrm)
mapd <- mapdCalc(bins$ratio)

write.table(mapd, file=opts$mapd.file, col.names=FALSE, row.names=FALSE)
