#' Estimate a periodigram from depth data
#' For help: periodigram.r -h 
#'
#' Author: Maxwell Sherman
#' Revised: 02-26-16
#' Revision notes: Added print statements to track progress

# Load libraries
library("optparse")
library("bspec")
library("reshape2")
library("ggplot2")

source("R/fnUtils.R")

welchAnalysis <- function(df.bins, bin.size, scale=100) {
    list.subjects = unique(df.bins$subject)

    for (subj in unique(df.bins$subject)) {
        df <- df.bins[df.bins$subject == subj, ]
        # df$ratio <- 2^df$ratio
        # df$ratio <- df$ratio - mean(df$ratio)

        subj.ts <- ts(df$ratio, start=0, deltat=bin.size)
        subj.wel <- welchPSD(subj.ts, scale*bin.size)

        # X11()
        # plot(subj.ts)
        # message("Press Return To Continue")
        # invisible(readLines("stdin", n=1))

        if (! exists("df.welch")) {
            df.welch = data.frame(frequency=subj.wel$frequency)
        }

        df.welch[[subj]] <- subj.wel$power
    }

    df.melt <- melt(df.welch, id.vars="frequency")
    j <- ggplot(df.melt, aes(x=frequency, y=value, color=variable)) + geom_line() +
         scale_y_log10() + scale_x_log10()
    ggsave("welch.pdf", plot=j)

}

smoothedPSDAnalysis <- function(df.bins, bin.size, smooth=4) {
    # Construct smoothing kernel
    k = kernel("daniell", m=smooth) # m sets smoothness?

    for (subj in unique(df.bins$subject)) {
        print(subj)
        df <- df.bins[df.bins$subject == subj, ]
        subj.ts <- ts(df$ratio, start=0, deltat=bin.size)
        subj.pgram = spec.pgram(subj.ts, k, taper=opts$taper, plot=FALSE)
        print(length(subj.pgram$spec))

        if (! exists("df.pgram")) {
            df.pgram = data.frame(frequency=subj.pgram$freq)
        }

        df.pgram[[subj]] <- subj.pgram$spec
    }

    df.melt <- melt(df.pgram, id.vars="frequency")
    j <- ggplot(df.melt, aes(x=frequency, y=value, color=variable)) + geom_line() +
         scale_y_log10() + scale_x_log10()
    ggsave("pgram.pdf", plot=j)
}

# Create parser options
option_list = list(
    make_option(c('-i', '--bin_dirs'), type="character", default=NULL, help="input bin file(s) as \"file_1 file_2 ... file_n\""),
    make_option(c('-o', '--output_file'), type="character", default=NULL, help="File stem for output files"),
    make_option(c('-b', '--bin.size'), type="integer", default=100, help="size of bins"),
    make_option(c('-c', '--chr.loc'), type="character", default=1, help="chrm on which to perform analysis"),
    make_option(c('-s', '--smooth'), type="integer", default=4, help="smoothing factor for smoothing kernel"),
    make_option(c('-t', '--taper'), type="double", default=0, help="length of smoothing taper"),
    make_option(c('--chrm'), default=FALSE, action='store_true', help="is \'chrm\' in file names"),
    make_option('--species', default='human', help='what species is this data from')
)

# Create parser and parse command line arguments
opt_parser <- OptionParser(option_list=option_list)
opts <- parse_args(opt_parser)

loc <- ParseLocation(opts$chr.loc)
bin.dirs <- parsePaths(opts$bin_dir)
bins <- do.call(rbind, lapply(bin.dirs, loadBinData, loc=loc, species=opts$species, chrm=opts$chrm))

welchAnalysis(bins, opts$bin.size)
# smoothedPSDAnalysis(bins, opts$bin.size, opts$smooth)

# Read depth data from input file
# sprintf("Reading depth data from: %s", opts$input_file)
# data = read.table(opts$input_file, col.names=c('chrm', 'pos', 'depth'))

# Create time series object from depth data
# print("Creating time series from depth")
# chrm.depth = ts(data$depth, start=data$pos[1])
# 
# # Construct smoothing kernel
# k = kernel("daniell", m=opts$smooth) # m sets smoothness?
# 
# # Calculate periodigram and build data frame
# print("Calculating periodigram")
# chrm.per = spec.pgram(chrm.depth, k, taper=opts$taper, plot=FALSE)
# chrm.frame = data.frame(chrm.per$freq, chrm.per$spec)
# 
# # Save periodigram data as table
# sprintf("Writing periodigram data to: %s", opts$output_file)
# write.table(chrm.frame, file=opts$output_file, row.names=FALSE, col.names=c('freq', 'pwr'))

# pdf('r_script_pgram.pdf')
# plot(chrm.per$freq, chrm.per$spec, type='l', log='xy')
# dev.off()
