#' Plot log2 ratio of bins in n samples on one figure
#' can zoom in to specified genomic region per chromosome
#' For help: plotBinsAgg.R -h 
#'
#' Author: Maxwell Sherman
#' Revised: 10-27-16
#' Revision notes: bins plotted as is instead of as running median

library("parallel")
library("Cairo")
library("optparse")
library("ggplot2")

source("R/fnUtils.R")

plotBinsAgg <- function(bins, figname, k=5, endrule="median") {
    n.subj <- length(unique(bins$subject))
    CairoPNG(filename=figname, width=1200, height=100*n.subj)
    bins$copy <- as.factor(bins$copy)
    # bins$ratio <- runmed(as.vector(bins$ratio), k=k, endrule=endrule)
    bins$ratio[bins$ratio > 4] <- 4
    bins$ratio[bins$ratio < -4] <- -4

    j <- ggplot(bins, aes(x=start, y=ratio, color=copy)) +
         geom_segment(aes(xend=start, yend=0)) +
         facet_grid(subject~chrom, scales="free_x", space="free_x", switch='x') + 
         scale_color_manual(values=c('red', 'yellow', 'green'), 
                            labels=c('loss', 'neut', 'gain'),
                            guide=guide_legend(reverse=TRUE)) +
         xlab("Chromosome") + 
         ylab("Log2 ratio") + 
         scale_y_continuous(breaks=-4:4) +
         theme(axis.ticks.x=element_blank(), axis.text.x = element_blank())

    print(j)
    dev.off()
}

chrFromSpecies <- function(species) {
    if (species == 'human') {
        chrl <- c(1:22, "X")
    } else if (species == 'mouse') {
        chrl <- c(1:19, "X")
    } else {
        stop("species not recognized. Don't know what to use for chromosomes")
    }
    names(chrl) <- chrl

    return(chrl)
}

# Create parser options
option_list = list(
    make_option(c('-i', '--bin_dir'), type="character", default=NULL, help="input bin dirs. Format: 'dir_1 dir_2 ... dir_n'"),
    make_option(c('-o', '--out.file'), type="character", default=NULL, help="Output file path"),
    make_option(c('--loc'), type="character", default=NULL, help="specific genome location as chr:start-end"),
    make_option(c('--species'), type="character", default='human', help="what species is that datafrom"),
    make_option(c('--chrm'), default=FALSE, action='store_true', help="what species is that datafrom")
)

# Create parser and parse command line arguments
opt_parser = OptionParser(option_list=option_list)
opts = parse_args(opt_parser)

bin.dir.tmr = parsePaths(opts$bin_dir)
k           = 5 # integer width of median window, must be odd
loc         = ParseLocation(opts$loc)

bins <- do.call(rbind, lapply(bin.dir.tmr, loadBinData, loc=loc, species=opts$species, chrm=opts$chrm))
# print(head(bins))
# print(tail(bins))
plotBinsAgg(bins, opts$out.file)
