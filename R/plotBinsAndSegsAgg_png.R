#' Plot bins and CNV segmentation of n samples on one figure with gene annotation
#' can zoom in to specified genomic region per chromosome
#' For help: plotBinsAndSegsAggWithGenes.R -h 
#'
#' Author: Maxwell Sherman
#' Revised: 11-22-16
#' Revision notes: png version of plotBinsAndSegsAgg2.R

library("parallel")
library("Cairo")
library("optparse")
library("ggplot2")
library("gridExtra")

source("/home/mas138/sandbox/dev/ngsSH/R/plotGeneAnnot.R")
source("/home/mas138/sandbox/dev/ngsSH/R/fnUtils.R")
# source("fnUtils.R")

# loadBinData <- function(bin.dir.tmr, loc=NULL, species='human') {
#     print(bin.dir.tmr)
#     ## Plots CNV segments with log2 ratio of each bin in the backgroun
#     ##
#     ##  Args:
#     ##      bin.dir.tmr     str     path to directory of bin data
#     ##
#     ##  Returns:
#     ##      bins            df      chr start   end log2ratio   color   subject
#     print(sprintf("Processing: %s", bin.dir.tmr))
# 
#     if (! is.null(loc)) {
#         chrl <- as.character(loc$chrom)
#     } else {
#         chrl <- chrFromSpecies(species)
#         # chrl <- c(1:22, "X")
#     }
#     # chrl <- c(1:22, "X")
#     names(chrl) <- chrl
# 
#     # Read bin files
#     # start   end     obs     expected        var
#     # 61762   78721   12      16.35           72.4539
#     cntl.tmr <- lapply(chrl, function(c) {
#            if (suppressWarnings(all(is.na(as.numeric(c))))) {
#                file <- file.path(bin.dir.tmr, 
#                                 list.files(path=bin.dir.tmr, 
#                                            pattern=sprintf("*.chrm_%s.*", c))[1])
#                                            # pattern=sprintf("*.chrm_%s.*b%03d.*", c, bin.size))[1])
#            } else {
#                file <- file.path(bin.dir.tmr, 
#                                 list.files(path=bin.dir.tmr, 
#                                            pattern=sprintf("*.chrm_%02d.*", 
#                                            # pattern=sprintf("*.chrm_%02d.*b%03d.*", 
#                                                            as.numeric(c)))[1])
#            }
#            cnt <- scan(file, list(1,1,1,1.1,1.1), sep="\t", skip=1)
#            names(cnt) <- c("start", "end", "obs", "expected", "var")
#            return(cnt) 
#         }  # End function
#     )      # End lapply
# 
#     # Total tumor and normal cnt
#     # In BICseq2, observed counts treated as tumor, expected as normal
#     tc <- 0
#     nc <- 0
# 
#     # Get the ratio of total tumor and normal read counts
#     tc.chr <- sapply(chrl, function(c) {sum(cntl.tmr[[c]]$obs)})
#     nc.chr <- sapply(chrl, function(c) {sum(cntl.tmr[[c]]$expected)})
#     tc <- sum(tc.chr)
#     nc <- sum(tc.chr)
# 
#     # Calculate [1/(tc/nc)] for entire genome for normalization purposes
#     R <- nc / tc
# 
#     # Create data frame of bin edges and copy number ratio
#     # chrom    start    end    ratio
#     #   1          0    10000   0
#     #   1      10001    20000   0.5
#     bins <- data.frame(do.call(rbind, 
#                               lapply(chrl,
#                                      function(c) {
#                                        # print(paste("processing", c))
#                                        r <- (cntl.tmr[[c]]$obs + 0.00000001) / 
#                                                 (cntl.tmr[[c]]$expected + 0.0000001) * R
#                                        ec <- sub("chr", "", c)
#                                        if (ec[1] != 'X') {
#                                             label <- sprintf("%02d", as.numeric(ec))
#                                        } else {
#                                             label <- as.character(ec)
#                                        }
#                                        df <- data.frame(ec, cntl.tmr[[c]]$start, 
#                                                     cntl.tmr[[c]]$end, log2(r), label)
#                                        return(df)
#                                      }  # End function
#                                     )   # End lapply
#                              )          # End do.call
#                       )                 # End data.frame
# 
#     # Name columns. Ensure chrom is character, start / end integers, and ratio a float
#     colnames(bins) <- c("chrom", "start", "end", "ratio", "chrm.label")
# 
#     bins$copy <- rep(0, nrow(bins))
#     bins$copy[bins$ratio > 0.3] <- 1
#     bins$copy[bins$ratio < -0.3] <- -1
# 
#     file <- list.files(bin.dir.tmr)[1]
#     subject <- unlist(strsplit(basename(file), '-'))[1]
#     bins$subject <- subject
# 
#     if (! is.null(loc)) {
#         bins <- bins[(bins$chrom==loc$chrom) & (bins$start>loc$start) & (bins$end<loc$end), ]
#     }
# 
#     print(sprintf("Done processing: %s", bin.dir.tmr))
# 
#     return(bins)
# }
# 
# loadSegData <- function(seg.file, loc=NULL) {
#     ## Plots CNV segments with log2 ratio of each bin in the backgroun
#     ##
#     ##  Args:
#     ##      seg.files       char    path to seg file
#     ##
#     ##  Returns:
#     ##      segs            df      chr start   end ratio   subject
#     print(sprintf("Processing: %s", seg.file))
#     segs <- read.table(seg.file, header=TRUE, sep='\t', stringsAsFactors=F)
# 
#     subject <- unlist(strsplit(basename(seg.file), '-'))[1]
#     segs$subject <- subject
#     segs$chrm.label <- sapply(segs$chrom, function(c) { 
#                                             suf <- tail(unlist(strsplit(c, 'chr')), n=1)
#                                             if (suppressWarnings(all(is.na(as.numeric(suf))))) {
#                                                 return(as.character(suf))
#                                             } else {
#                                                 return(sprintf("%02d", as.numeric(suf)))
#                                             }
#                                           })
# 
#     segs$chrm.num <- sapply(segs$chrom, function(c) { 
#                                             suf <- tail(unlist(strsplit(c, 'chr')), n=1)
#                                             if (suf == 'X') {
#                                                 return(23)
#                                             } else if (suf == 'Y') {
#                                                 return(24)
#                                             } else {
#                                                 return(as.numeric(suf))
#                                             }
#                                           })
# 
#     names(segs)[names(segs)=="log2.copyRatio"] <- "ratio"
#     print(sprintf("Done processing: %s", seg.file))
# 
#     if (! is.null(loc)) {
#         segs <- segs[(segs$chrm.num==loc$chrom) & (segs$start>loc$start) & (segs$end<loc$end), ]
#     }
# 
#     return(segs)
# }

# loadGeneData <- function(gene.file, loc=NULL) {
#     genes <- readRDS(gene.file)
# 
#     if (! is.null(loc)) {
#         chrom = paste('chr', loc$chrom, sep='')
#         genes <- genes[(genes$chr==chrom) & (genes$s>loc$start) & (genes$e<loc$end), ]
#         genes <- genes[order(genes$s), ]
#     }
# 
#     gene.names <- unique(genes$name)
# 
#     df.genes <- do.call(rbind, 
#                        lapply(gene.names, 
#                               function(c) {
#                                 inds <- which(genes$name == c)
#                                 df <- data.frame(chrom = genes$chr[inds[1]],
#                                                  start = genes$s[inds[1]],
#                                                  end   = genes$e[tail(inds, n=1)],
#                                                  name  = c
#                                                 )
#                                 return(df)
#                               }
#                              )
#                        )
# 
#     return(df.genes)
# }

plotBinsAndSegsAgg <- function(bins, segs, figname, genes=Null, k=5, endrule="median", cutoff=0.1) {
    n.subj <- length(unique(bins$subject))

    # if (n.subj < 5) {
    #     CairoPNG(filename=figname, width=1200, height=500)
    # } else {
    #     CairoPNG(filename=figname, width=1200, height=100*n.subj)
    # }

    bins$copy <- as.factor(bins$copy)
    bins$ratio <- runmed(as.vector(bins$ratio), k=k, endrule=endrule)
    bins$ratio[bins$ratio > 2] <- 2
    bins$ratio[bins$ratio < -2] <- -2
    bins$subject <- factor(bins$subject, levels=sort(bins$subject, decreasing=T))

    segs$ratio[segs$ratio > 2] <- 2
    segs$ratio[segs$ratio < -2] <- -2

    segs$copy <- 0
    segs$copy[segs$ratio > cutoff]  <- 1
    segs$copy[segs$ratio < -cutoff] <- -1
    segs$copy <- as.factor(segs$copy)
    segs$subject <- factor(segs$subject, levels=sort(segs$subject, decreasing=T))

    ymax <- ceiling(max(abs(segs$ratio)))
    ymin <- -ymax

    j <- ggplot(bins, aes(x=start, y=ratio)) +
         geom_point(color='grey') +
         geom_hline(yintercept=0, linetype='dashed') +
         geom_segment(data=segs, aes(x=start, xend=end, y=ratio, yend=ratio, color=copy), size=2) +
         facet_grid(subject~chrm.label, scales="free_x", space="free_x", switch='x') + 
         scale_color_manual(values=c('darkred', 'yellow3', 'green4'), 
                            labels=c('loss', 'neut', 'gain'),
                            guide=guide_legend(reverse=TRUE),
                            limits=levels(as.factor(-1:1))) +
         xlab("Chromosome") + 
         ylab("Log2 ratio") +
         theme_bw(base_size=16) +
         theme(axis.ticks.x=element_blank(), axis.text.x = element_blank(), strip.text.x = element_text(size = 16))
         # ylim(-4, 4) +
         # scale_y_continuous(breaks=-4:4)
         # theme(axis.ticks.x=element_blank(), axis.text.x = element_blank())

    # if (! is.null(genes)) {
    #     j <- j + geom_vline(data=genes, aes(xintercept=start), color='blue', linetype='longdash') +
    #            geom_vline(data=genes, aes(xintercept=end), color='orange', linetype='longdash') +
    #            geom_label(data=genes, aes(x=(start+end)/2, y=ymax-(ymax-ymin)/10, label=name))
    # }

    j <- j + ylim(ymin, ymax)

    s <- ggplot_build(j)
    print(s$panel$y_scale)

    if (! is.null(loc) ) {
      p.ideo <- plotIdeogram(loc=loc)
      # fixed(p.ideo) <- T
    }

    if (! is.null(genes$genes)) {
        p.gene <- plotGeneAnnot(genes$genes, genes$features, loc)
        j <- plot_grid(j, p.gene, align='v', ncol=1, rel_heights=c(9, 2.5)) # + xlim(loc$start, loc$end)
        j <- plot_grid(p.ideo, j, ncol=1, rel_heights=c(1, 7)) # + xlim(loc$start, loc$end)

    } else if (! is.null(loc)) {
        j <- plot_grid(p.ideo, j, ncol=1, rel_heights=c(1, 6), rel_widths=c(2, 4)) # + xlim(loc$start, loc$end)
    }

    if (n.subj < 5) {
        ggsave(figname, j, width=12, height=9)
    } else {
        ggsave(figname, j, width=12, height=(1*n.subj + 4))
    }

    # if (! is.null(genes)) {
    #     p <- plotGeneAnnot(genes$genes, genes$features)
    #     j <- addGeneAnnot2Plot(j, p)
    # }

    # # CairoPDF(file=opts$out.file, width=12, height=5)
    # if (n.subj < 5) {
    #     figname = sub("pdf", "png", figname)
    #     CairoPNG(filename=figname, width=2000, height=1000)
    #     # CairoPDF(file=figname, width=12, height=5)
    # } else {
    #     figname = sub("pdf", "png", figname)
    #     CairoPNG(filename=figname, width=2400, height=1000)
    #     # CairoPNG(filename=figname, width=2400, height=250*n.subj)
    #     # CairoPDF(file=figname, width=12, height=1*n.subj)
    # }
    # print(j)
    # dev.off()

    # print(j)
    # dev.off()

    return(j)
}

# ParseLocation <- function(loc.str) {
#     if (is.character(loc.str)) {
#         spl.chr <- strsplit(loc.str, ':')
#         # chrom <- paste('chr', spl.chr[[1]][1], sep='')
#         chrom <- as.numeric(spl.chr[[1]][1])
#         # chrom.num <- as.numeric(spl.chr[[1]][1])
# 
#         spl.loc <- strsplit(spl.chr[[1]][2], '-')
#         start <- as.numeric(spl.loc[[1]][1])
#         end <- as.numeric(spl.loc[[1]][2])
# 
#         return(data.frame(chrom, start, end))
#     } else {
#         return(NULL)
#     }
# }
# 
# parsePaths <- function(path) {
#     return(unlist(strsplit(path, " ")))
# }
# 
# checkBinSegMatch <- function(bin.dirs, seg.files) {
#     if (length(bin.dirs) != length(seg.files)) {
#         stop("You did not provide the same number of bin dirs and seg files")
#     }
# 
#     subjects.bins <- sapply(bin.dirs, function(c) {
#                                              file <- list.files(c)[1]
#                                              return(unlist(strsplit(basename(file), '-'))[1])
#                                       })
#     subjects.segs <- sapply(seg.files, function(c) { unlist(strsplit(basename(c), '-'))[1]})
#     print(subjects.bins)
#     print(subjects.segs)
# 
#     if (any(subjects.bins != subjects.segs)) {
#         stop("Bin dirs and seg files do not match or were provided in different orders")
#     }
# }
# 
# chrFromSpecies <- function(species) {
#     if (species == 'human') {
#         chrl <- c(1:22, "X")
#     } else if (species == 'mouse') {
#         chrl <- c(1:19, "X")
#     } else {
#         stop("species not recognized. Don't know what to use for chromosomes")
#     }
#     names(chrl) <- chrl
# 
#     return(chrl)
# }

# Create parser options
option_list = list(
    make_option(c('-i', '--bin.dirs'), type="character", default=NULL, help="input bin dirs. Format: 'dir_1 dir_2 ... dir_n'"),
    make_option(c('-s', '--seg.files'), type="character", default=NULL, help="input seg files. Format 'file_1 file_2 ... file_n'"),
    make_option(c('-o', '--out.file'), type="character", default=NULL, help="Output file path"),
    make_option(c('-a', '--gene.file'), type="character", default="/home/mas138/sandbox/dev/ngsSH/R/hg19.genes.RData", help="gene file for annotation"),
    make_option(c('--genes'), action='store_true', default=FALSE, help="include gene annotation?"),
    make_option(c('--loc'), type="character", default=NULL, help="specific genome location as chr:start-end"),
    make_option(c('-c', '--cutoff'), type="numeric", default="0.1", help="significance cutoff for seg coloring"),
    make_option(c('--species'), type="character", default='human', help="what species is that datafrom"),
    make_option('--chrm', default=FALSE, action='store_true', help="\'chrm\' in bin file names")
)

# Create parser and parse command line arguments
opt_parser = OptionParser(option_list=option_list)
opts = parse_args(opt_parser)

bin.dirs    = parsePaths(opts$bin.dirs)
seg.files   = parsePaths(opts$seg.files)
k           = 5 # integer width of median window, must be odd
loc         = ParseLocation(opts$loc)

# checkBinSegMatch(bin.dirs, seg.files)

bins <- do.call(rbind, lapply(bin.dirs, loadBinData, loc=loc, chrm=opts$chrm))
segs <- do.call(rbind, lapply(seg.files, loadSegData, loc=loc))

if (opts$genes) {
    genes <- loadGeneData(opts$gene.file, loc=loc)
} else {
    genes <- NULL
}
# print(head(bins))
# print(tail(bins))
j <- plotBinsAndSegsAgg(bins, segs, opts$out.file, genes=genes, cutoff=opts$cutoff)
# p <- plotGeneAnnot(genes$genes, genes$features)
# g <- addGeneAnnot2Plot(j, p)
# 
# CairoPDF(file=opts$out.file, width=12, height=5)
# print(g)
# # ggsave(opts$out.file, g)
# dev.off()
# ggsave(opts$out.file, g)
