#! /usr/bin/env python

# bamDownSample.py - downsample bam file to desired depth
#
# v 0.6.5
# rev 2016-10-27 (MS: can take downsampling ratio or desired depth of downsampling)
# Notes: 

import argparse
import subprocess
import ConfigParser

def calc_avg_depth(file_bam):
    conf = ConfigParser.SafeConfigParser()
    conf.read("ngssh.conf")
    script = conf.get("scripts", "bam_avg_depth")
    # path = pathlib.Path.cwd()
    # script = path / "bin/bamAvgDepth.sh"
    cmd = str(script) + ' ' + file_bam
    # cmd = "./bamAvgDepth.sh {}".format(file_bam)
    depth_str = exec_cmd(cmd, verbose=True)

    return float(depth_str)

def calc_downsample_ratio(depth_actual, depth_desired):
    if depth_desired > depth_actual:
        raise ValueError("Desired depth %f exceeds actual depth %f" %(depth_desired, depth_actual))

    ratio = depth_desired / depth_actual

    return ratio

def samtools_downsample(bam_in, bam_out, ratio):
    cmd = "samtools view -h -s {:0.3f} -o {} {}".format(ratio, bam_out, bam_in)
    exec_cmd(cmd, verbose=True)

def samtools_index(bam):
    cmd = "samtools index {}".format(bam)
    exec_cmd(cmd, verbose=True)

def exec_cmd(cmd, verbose=False, suppress_error=False):
    if verbose:
        print( "\nExecuting: %s\n" %cmd)

    if suppress_error:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read()

    else:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()

    return p

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--in_bam', required=True, help='Input bam file')
    parser.add_argument('-o', '--out_bam', required=True, help='output bam file')
    parser.add_argument('-d', '--depth', dest='depth_desired', type=float, help='desired depth to which to downsample bam')
    parser.add_argument('-r', '--ratio', default=0, type=float, help='desired ratio of downsampling; overrides -d')
    # parser.add_argument('-l', '--read_length', default=151, help='length of reads in bam file')

    return parser.parse_args()

def run():
    p = parse_args()

    if not p.ratio:
        depth_actual = calc_avg_depth(p.in_bam)
        ratio = calc_downsample_ratio(depth_actual, p.depth_desired)
    else:
        ratio = p.ratio

    samtools_downsample(p.in_bam, p.out_bam, ratio)
    samtools_index(p.out_bam)

if __name__ == "__main__":
    run()
