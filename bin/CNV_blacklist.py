#! /usr/bin/env python

# CNV_blacklist.py - command line tool for finding and removing blacklist regions in several CNV profiles
#
# v 0.6.5
# rev 2016-10-27 (MS: better filtering and reading input paths from file)
# Notes: modified from segPipe.py

from __future__ import print_function
import pathlib2 as pathlib
import argparse
import subprocess
import sys

class SegPipe(object):
    def __init__(self):
        self.files = {}
        self.R_loc = '/home/mas138/sandbox/dev/ngsSH/R'
        self.__parse_args()
        # self.files['seg'] = self.p.seg_files
        self.__parse_seg_files()
        # print(self.files['seg'])
        self.dir_out = pathlib.Path(self.p.dir_out)

    def seg_pipeline(self):
        # 1. Create agg file from raw seg files
        if not self.p.f_prob:
            f_seg_agg = self.dir_out / (self.p.basename + '.seg.agg')
            self.create_seg_agg_file('seg', f_seg_agg)

        # f_seg_agg = self.dir_out / (self.p.basename + '.seg.agg')
        # self.create_seg_agg_file('seg', f_seg_agg)

        # 2. Create problem BED file from seg agg file
        if not self.p.f_prob:
            f_prob = self.dir_out / (self.p.basename + '.blacklist') 
            self.create_seg_prob_file(f_seg_agg, f_prob)
        else:
            f_prob = pathlib.Path(self.p.f_prob)

        # 3. Remove sig segs in each seg file overlapping with problem regions
        self.files['prob_removed'] = [f.with_suffix('.prob.removed') for f in self.files['seg']]
        for f, f_out in zip(self.files['seg'], self.files['prob_removed']):
            self.warn_problem_regions(f, f_out, f_prob, buf=0)

    def create_seg_agg_file(self, file_key, f_out, pval=1.):
        seg_str = self.files_to_str(file_key)
        cmd = 'Rscript ' + self.R_loc + '/seg2Agg.R ' '-i ' + seg_str + ' -o ' + \
              str(f_out) + ' -c %f' %self.p.cutoff + ' -p %f' %pval + ' --res mean'
        print(cmd, file=sys.stderr)
        self.__exec_cmd(cmd)

    def create_seg_prob_file(self, f_agg, f_out):
        cmd = 'Rscript ' + self.R_loc + '/segProbs2BED.R ' '-i ' + str(f_agg) + ' -o ' + \
              str(f_out) + ' -f %s' %self.p.frac
        print(cmd, file=sys.stderr)
        self.__exec_cmd(cmd)

    def create_seg_renorm_files(self, f_prob):
        for f_in, f_out in zip(self.files['seg'], self.files['renorm']):
            cmd = 'Rscript ' + self.R_loc + '/segRmProbs.R ' '-i ' + str(f_in) + ' -o ' + \
                  str(f_out) + ' -p ' + str(f_prob) + ' -c %f' %self.p.cutoff
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)

    def annotate_file(self, f_in, f_out):
            cmd = 'Rscript ' + self.R_loc + '/annotGene.R ' '-i ' + str(f_in) + ' -o ' + \
                  str(f_out) + ' -a ' + self.p.annot_file
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)

    def warn_problem_regions(self, f_in, f_out, f_prob, buf=100000):
            cmd = 'Rscript ' + self.R_loc + '/segWarnProbs.R ' '-i ' + str(f_in) + ' -o ' + \
                  str(f_out) + ' -p ' + str(f_prob) + ' -b ' + str(buf) + \
                  ' -P ' + str(self.p.perc)
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)
        
    def files_to_str(self, file_key):
        return '\"' + ' '.join([str(f) for f in self.files[file_key]]) + '\"'

    def __parse_seg_files(self):
        if self.p.file_list:
            with open(self.p.seg_files[0]) as f:
                self.files['seg'] = [pathlib.Path(line.strip('\n')) for line in f]

        else:
            self.files['seg'] = [pathlib.Path(f) for f in self.p.seg_files]

        # self.files['seg'] = [pathlib.Path(f) for f in self.p.seg_files]
        # self.files['seg'] = [pathlib.Path(f.strip()) for f in self.p.seg_files.split(' ')]

    def __exec_cmd(self, cmd):
        print( "\nExecuting: %s\n" %cmd)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
        print( p)

        return p

    def __parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--seg_files', required=True, nargs='+', help='Input seg files. Format: file_1 file_2 ... file_n. Or file containing paths to files')
        parser.add_argument('-l', '--file_list', action="store_true", default=False, help='-i is a file containing a list of input files')
        # parser.add_argument('-a', '--annot_file', default=self.R_loc + '/hg19.genes.RData')
        parser.add_argument('-o', '--dir_out', default='.', help='output directory')
        parser.add_argument('-b', '--basename', default='cnv_blacklist_output', help='basename for output files')
        parser.add_argument('-c', '--cutoff', type=float, default=0.2,  help='Ratio threshold for seg significance')
        parser.add_argument('-p', '--pvalue', type=float, default=1.,  help='p-value threshold for significance')
        parser.add_argument('-f', '--frac', type=float, default=0.5,  help='fraction of cases having variation in a region to call it problematic')
        parser.add_argument('-F', '--f_prob', type=str, default=None,  help='path to problem file. Overrides -f option if supplied')
        parser.add_argument('-P', '--perc', type=float, default=0.5,  help='percent overlap minimum when comparing case / control segs')

        self.p = parser.parse_args()

if __name__ == "__main__":
    pipe = SegPipe()
    pipe.seg_pipeline()
