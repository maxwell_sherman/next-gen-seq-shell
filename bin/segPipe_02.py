#! /usr/bin/env python

# segPipe.py - command line tool for running a CNV post-processing pipeline
#
# v 0.6.5
# rev 2016-10-27 (MS: reordered steps to remove prob regions then normal regions)
# Notes: 

from __future__ import print_function
import pathlib2 as pathlib
import argparse
import subprocess
import sys

class SegPipe(object):
    def __init__(self):
        self.files = {}
        self.R_loc = '/home/mas138/sandbox/dev/ngsSH/R'
        self.__parse_args()
        self.__parse_seg_files()
        self.dir_out = pathlib.Path(self.p.dir_out)

    def seg_pipeline(self):
        # 1. Create agg file from raw seg files
        f_seg_agg = self.dir_out / (self.p.basename + '.seg.agg')
        self.create_seg_agg_file('seg', f_seg_agg)

        # 2. Create problem BED file from seg agg file
        if not self.p.f_prob:
            f_prob = self.dir_out / (self.p.basename + '.prob') 
            self.create_seg_prob_file(f_seg_agg, f_prob)
        else:
            f_prob = pathlib.Path(self.p.f_prob)

        # 3. Remove problematic Regions
        # self.files['norm'] = [f.with_suffix('.norm') for f in self.files['seg']]
        # self.create_seg_norm_files()
        self.files['prob_removed'] = [f.with_suffix('.prob.removed') for f in self.files['seg']]
        for f, f_out in zip(self.files['seg'], self.files['prob_removed']):
            self.warn_problem_regions(f, f_out, f_prob, buf=0)

        self.files['prob_removed'] = [f.with_suffix('.prob.removed.good') for f in self.files['seg']]

        # 3. Remove regions of normal variability
        self.files['normal_removed'] = [f.with_suffix('.normal.removed') for f in self.files['seg']]
        for f, f_out in zip(self.files['prob_removed'], self.files['normal_removed']):
            self.warn_problem_regions(f, f_out, self.p.norm_var_file, buf=0)

        self.files['normal_removed'] = [f.with_suffix('.normal.removed.good') for f in self.files['seg']]

        # Remove segs overlapping with problematic regions
        # self.files['prob_removed'] = [f.with_suffix('.normal.prob.removed') for f in self.files['seg']]
        # for f, f_out in zip(self.files['normal_removed'], self.files['prob_removed']):
        #     self.warn_problem_regions(f, f_out, f_prob, buf=0)

        # self.files['prob_removed'] = [f.with_suffix('.normal.prob.removed.good') for f in self.files['seg']]

        # # 4. Remove sig segs in each seg file overlapping with problem regions
        # self.files['renorm'] = [f.with_suffix('.renorm') for f in self.files['seg']]
        # self.create_seg_renorm_files(f_prob)

        # 5. Create agg file from fully filtered files
        f_renorm_agg = self.dir_out / (self.p.basename + '.filtered.agg')
        self.create_seg_agg_file('prob_removed', f_renorm_agg, pval=self.p.pvalue)

        # # 6. (optional) remove segs that overlap with regions of normal variation
        # if self.p.norm_var_file:
        #     f_base = self.dir_out / (self.p.basename + '.normal')
        #     self.warn_problem_regions(f_renorm_agg, f_base, self.p.norm_var_file, buf=0)
        #     f_renorm_agg = self.dir_out / (self.p.basename + '.normal.good')

        #     for f in self.files['renorm']:
        #         self.warn_problem_regions(f, f.with_suffix('.normal'), self.p.norm_var_file, buf=0)

        # 6. Annotate renormed agg file
        f_gene = self.dir_out / (self.p.basename + '.filtered.gene')
        self.annotate_file(f_renorm_agg, f_gene)

        # # 7. Warn of segs near problem regions
        # f_base = self.dir_out / (self.p.basename + '.renorm')
        # self.warn_problem_regions(f_gene, f_base, f_prob)

    def create_seg_agg_file(self, file_key, f_out, pval=1.):
        seg_str = self.files_to_str(file_key)
        cmd = 'Rscript ' + self.R_loc + '/seg2Agg.R ' '-i ' + seg_str + ' -o ' + \
              str(f_out) + ' -c %f' %self.p.cutoff + ' -p %f' %pval + ' --res mean'

        if self.p.renormSex:
            cmd += ' --renormSex'

        print(cmd, file=sys.stderr)
        self.__exec_cmd(cmd)

    def create_seg_prob_file(self, f_agg, f_out):
        cmd = 'Rscript ' + self.R_loc + '/segProbs2BED.R ' '-i ' + str(f_agg) + ' -o ' + \
              str(f_out) + ' -f %s' %self.p.frac
        print(cmd, file=sys.stderr)
        self.__exec_cmd(cmd)

    def create_seg_norm_files(self):
        for f_in, f_out in zip(self.files['seg'], self.files['norm']):
            cmd = 'Rscript ' + self.R_loc + '/cnvNorm.R ' '-i ' + str(f_in) + ' -o ' + str(f_out) + ' -n ' + self.p.norm_file
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)

    def create_seg_renorm_files(self, f_prob):
        for f_in, f_out in zip(self.files['norm'], self.files['renorm']):
            cmd = 'Rscript ' + self.R_loc + '/segRmProbs.R ' '-i ' + str(f_in) + ' -o ' + \
                  str(f_out) + ' -p ' + str(f_prob) + ' -c %f' %self.p.cutoff
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)

    def annotate_file(self, f_in, f_out):
            cmd = 'Rscript ' + self.R_loc + '/annotGene.R ' '-i ' + str(f_in) + ' -o ' + \
                  str(f_out) + ' -a ' + self.p.annot_file
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)

    def warn_problem_regions(self, f_in, f_out, f_prob, buf=100000):
            cmd = 'Rscript ' + self.R_loc + '/segWarnProbs.R ' '-i ' + str(f_in) + ' -o ' + \
                  str(f_out) + ' -p ' + str(f_prob) + ' -b ' + str(buf) + \
                  ' -P ' + str(self.p.perc)
            print(cmd, file=sys.stderr)
            self.__exec_cmd(cmd)
        
    def files_to_str(self, file_key):
        return '\"' + ' '.join([str(f) for f in self.files[file_key]]) + '\"'

    def __parse_seg_files(self):
        self.files['seg'] = [pathlib.Path(f.strip()) for f in self.p.seg_files.split(' ')]

    def __exec_cmd(self, cmd):
        print( "\nExecuting: %s\n" %cmd)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
        print( p)

        return p

    def __parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--seg_files', required=True, help='Input seg files. Format: \'file_1 file_2 ... file_n\'')
        # parser.add_argument('-n', '--norm_file', required=True, help='normalization seg file')
        parser.add_argument('-N', '--norm_var_file', required=True, help='file of regions of normal variability (optional)')
        parser.add_argument('-a', '--annot_file', default=self.R_loc + '/hg19.genes.RData')
        parser.add_argument('-o', '--dir_out', required=True, help='output directory')
        parser.add_argument('-b', '--basename', required=True, help='basename for output files')
        parser.add_argument('-c', '--cutoff', type=float, default=0.3,  help='Ratio threshold for seg significance')
        parser.add_argument('-p', '--pvalue', type=float, default=1.,  help='p-value threshold for significance')
        parser.add_argument('-P', '--perc', type=float, default=0.5,  help='percent overlap minimum when comparing case / control segs')
        parser.add_argument('-f', '--frac', type=float, default=0.5,  help='fraction of cases having variation in a region to call it problematic')
        parser.add_argument('-F', '--f_prob', type=str, default=None,  help='path to problem file. Overrides -f option if supplied')
        parser.add_argument('--noRenormSex', dest='renormSex', action="store_false", default=True,  help='Do not mean renormalize sex chromosomes')

        self.p = parser.parse_args()

if __name__ == "__main__":
    pipe = SegPipe()
    pipe.seg_pipeline()
