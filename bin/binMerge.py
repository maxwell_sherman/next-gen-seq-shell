#! /usr/bin/env python

# binMerge.py - wrapper for binMerge binary. Expects execution from top level directory
#
# v 0.6.7
# rev 2016-11-10 (MS: created)
# Notes: 

from __future__ import print_function
import pathlib2 as pathlib
import argparse
import subprocess
import sys

class binMerge(object):
    def __init__(self):
        self.__parse_args()
        self.__check_merge_size()
        self.__input_files()
        self.__output_files()
        self.__build_merge_cmds()

    def exec_merge_cmds(self):
        for cmd in self.cmd_list:
            print( "\nExecuting: %s\n" %cmd, file=sys.stderr)
            p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()

        return p

    def __check_merge_size(self):
        if self.p.merge_size % self.p.bin_size != 0:
            raise ValueError("Merge size %i is not a multiple of bin size %i" %(self.p.merge_size, self.p.bin_size))
        else:
            self.n = self.p.merge_size / self.p.bin_size

    def __input_files(self):
        self.files_bin = sorted([f for f in self.p.bin_dir.glob("*.bin") if f.is_file()])

    def __output_files(self):
        fnames = [f.name.replace("b%i" %self.p.bin_size, "b%i" %self.p.merge_size)
                  for f in self.files_bin]
        self.files_out = [self.p.out_dir / name for name in fnames]

    def __build_merge_cmds(self):
        self.cmd_list = []

        for fin, fout in zip(self.files_bin, self.files_out):
            cmd = "./bin/binMerge %s -o %s -n %i" %(fin, fout, self.n)
            self.cmd_list.append(cmd)

    def __parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-i', '--bin_dir', required=True, help='Input bin directory')
        parser.add_argument('-o', '--out_dir', required=True, help='Output directory')
        parser.add_argument('-b', '--bin_size', required=True, type=int, help='input bin size')
        parser.add_argument('-m', '--merge_size', required=True, type=int, help='output bin size')

        self.p = parser.parse_args()
        self.p.bin_dir = pathlib.Path(self.p.bin_dir)
        self.p.out_dir = pathlib.Path(self.p.out_dir)

if __name__ == "__main__":
    bm = binMerge()
    bm.exec_merge_cmds()
