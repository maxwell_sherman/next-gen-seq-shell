#!/bin/sh

# sh script for calculating average depth of a WGS bam file
# By default prints to stdout unless an output file is specified
#
# Author: Maxwell Sherman (with input from Daniel Kwon)
# Last revision: 08/15/2016
#
# Usage: ./avgDepth <input_bam> [<output_file>]

read_total=`samtools idxstats $1 | head -n -1 | awk '{sum += $3 + $4} END{print sum}'`
read_length=`samtools view $1 | head -n 100000 | awk '{sum+=length($10)} END{round=sprintf("%.0f",sum/NR); print round}'`

if [ $# -eq 2 ]
    then
        python -c "print round($read_length. * $read_total. / 3095693981., 4)" > $2
else
    echo `python -c "print round($read_length. * $read_total. / 3095693981., 4)"`
fi
