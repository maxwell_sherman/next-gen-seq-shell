#!/usr/bin/perl
#
# Alice E. Lee :  ejalice.lee@gmail.com
# Aug. 26, 2010
# read hg18 refFlat.txt.gz and wgRna.txt.gz downloaded from UCSC
# read hg19 refFlat.txt.gz ( wgRNA is not available on hg19 )
# and list genes or Rnas belong in each cna segment

use strict;
use Getopt::Std;
use File::Basename;
use IO::File;

my $usage = qq{
Usage: annotGene.pl [-v assembly version] [-o outfile] [-m] [-p cutoff] [-n cutoff] <segment_file> 

Options: -v assembly version (hg19) 
-o outfile (sgment_file.genes) 
-m enable microRna annotation (default: OFF)
-p cutoff positive log2ratio cutoff for annotation (0.3) 	
-n cutoff negative log2ratio cutoff for annotation (-0.3) 	

};

my %opts = ();
getopts("hmo:p:n:v:", \%opts);

if ( @ARGV  < 1 || defined($opts{h}) ) { die $usage }

my $sf = shift;
my $of = substr($sf, 0, -4) . ".genes"; 
my $m = 0;
my $p = 0.3;
my $n = -0.3;
my $v = "hg19";

if ( defined($opts{v}) ) { $v = $opts{v} }; 
if ( defined($opts{o}) ) { $of = $opts{o} }; 
if ( defined($opts{m}) ) { $m = 1 }; 
if ( defined($opts{p}) ) { $p = $opts{p} }; 
if ( defined($opts{n}) ) { $n = $opts{n} }; 

open(S, "< $sf") or die "Can't open file $sf";
open(O, "> $of") or die "Can't open file $of";

# previous and current chromosomes, for the first chromosome entry, open the chromosome annotation file 
my ($pchr, $cchr, @g, @m);
my ($sstart, $send, $gstart, $gend, $mstart, $mend);
my ($gidx, $midx) = (0, 0);
my $size=0;

my $h = <S>; chomp($h);
print O "$h\tsize\tgenes\n";

$gidx = 0; $midx = 0;
while (<S>) {
    chomp; my @a = split(/\t/);		
    $cchr = $a[0];		
    $sstart = $a[1]; $send = $a[2];
    $size = $send - $sstart + 1;

    my %genes = ();

    print O "$_\t$size";

    # if log2ratio is within the cutoff range, proceed the annotation
    if ( scalar($a[4]) >= $p || scalar($a[4]) <= $n ) {

        if ($cchr ne $pchr) {
            # if a new chromosome appears, open the annotation files 
            open(G, "< /home/sl279/BiO/Store/Annotations/$v/refFlat/$cchr") or die "/home/sl279/BiO/Store/Annotations/$v/refFlat/$cchr";
            #open(G, "< /home/sl279/BiO/Store/Annotations/$v/refFlat/$cchr") or die "/home/sl279/BiO/Store/Annotations/$v/refFlat/$cchr";
            #open(G, "< /home/el114/gcc/data/annotation/$v/refFlat/refFlat.$cchr") or die "Can't open file /data1/gcc/data/annotation/$v/refFlat/$cchr";
            @g = <G>;

            #if ( $m == 1 ) {	
                #open(M, "< /home/el114/gcc/data/annotation/$v/wgRna/wgRna.$cchr") or die "Can't open file /data1/gcc/data/annotation/wgRna/$cchr";
                #@m = <M>;
            #}

            $gidx = 0; $midx = 0;
        }

        for (my $i=$gidx; $i<@g; $i++) {
            my @b = split(/\t/, $g[$i]); 			
            $gstart = $b[4]; $gend = $b[5]; 

            if ($send < $gstart) { 
                $gidx = $i; # next time check starting from this gene entry 
                last; 
            }

            # otherwise check the overlap
            if ( &overlap($sstart, $send, $gstart, $gend) ) {
                #print "match: $_\t$g[$i]";
                if (!exists $genes{$b[0]}) { 
                    $genes{$b[0]} = 1;
                } 
            }
        }

        if ($m == 1) {	
            for (my $i=$midx; $i<@m; $i++) {
                my @b = split(/\t/, $g[$i]); 			
                $mstart = $b[2]; $mend = $b[3]; 

                if ($send < $mstart) { 
                    $midx = $i; # next time check starting from this gene entry 
                    last; 
                }

                # otherwise check the overlap
                if ( &overlap($sstart, $send, $mstart, $mend) ) {
                    #print "match: $_\t$m[$i]";
                    if (!exists $genes{$b[4]}) { 
                        $genes{$b[4]} = 1;
                    } 
                }
            }
        }
        # print the gene list
        if ( keys( %genes ) != 0 ) { 
            print O "\t";
            my $first = 1;
            for my $key (sort keys  %genes ) {
                if ($first == 1) {
                    print O "$key";
                    $first = 0;
                } else {
                    print O " $key";
                }			
            }
        }
        $pchr = $cchr;
    }
    print O "\n";	
} 

sub overlap {
    my ($s1, $e1, $s2, $e2) = @_;

    my $overlap=0;

    if (($s1 <= $s2 && $s2 <= $e1) || ($s2 <= $e1 && $s1 <= $e2)) { $overlap = 1 }

    return $overlap;
}
