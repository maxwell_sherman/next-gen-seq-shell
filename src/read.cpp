/*
read -- functions for reading tables

v 0.0.2
rev 2016-11-10 (MS: created)
Notes:
*/

#include "read.h"

using namespace std;

size_t filesize(char* file, int *ncols) {
    ifstream ifs(file);
    // size_t rows = std::count(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>(), '\n');

    // ifs.unget();
    // if(ifs.get() != '\n') { ++rows; }

    ifs.seekg(0, ifs.end);
    int fsize = ifs.tellg();
    ifs.seekg(0, ifs.beg); 
    // cout << fsize << endl;
    // 
    string line;
    string n;
    int cols = 0;
    getline(ifs, line);
    std::istringstream is(line);

    while (is >> n)
        cols++;

    // cout << count << endl;
    // *nrows = fsize / count;
    // cout << fsize / count << endl;
    // cout << n << endl;
    // *nrows = fsize;
    *ncols = cols;

    return fsize;

}

double* read_table(char *file, int *nrows, int *ncols, int skip){
// vector< vector<double> > read_table(char *file, int *nrows, int *ncols, int skip){
    // Declare data and open file
    // vector< vector<double> > data;
    int fsize;
    fsize = filesize(file, ncols);
    // cout << *nrows << "\t" << *ncols << endl;

    double* data = new double[fsize];
    // double* data = new double[(*nrows)*(*ncols)];
    data[0] = 0;
    ifstream ifs(file);

    // Ensure file opened
    if (! ifs.is_open()) {
        cerr << "failed to open " << file << endl;
        exit(0);
    }

    int row = 1;
    int i = 0;

    // Read lines of file
    while((true)) {
        string line;

        // Skip lines as necessary
        if (row <= skip) {
            getline(ifs, line);
            row++;
            continue;
        }

        // Exit if EOF or skip if comment
        if ( getline(ifs, line) == 0) {
            break;
        } else if (line[0] == '#')
            continue;

        double n;
        // vector<double> v;

        // For c++ beginners: basically split string at white spaces and iterate over array
        int j = 0;
        std::istringstream is(line);
        while (is >> n) {
            data[i*(*ncols) + j] = n;
            j++;
            // populate v with doubles from line
            // v.push_back(n);
        }
        i++;

        // make v a row of data
        // data.push_back(v);
    } 
    ifs.close();

    // calc nrows and ncols
    *nrows = i;
    // *ncols = data[0].size();

    cerr << "Processed "<< *nrows << " rows and " << *ncols << " columns\n";

    return data;
}
