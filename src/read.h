#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iterator>
#include <sstream>
#include <algorithm>

double* read_table(char *file, int *nrows, int *ncols, int skip);

size_t filesize(char* file, int *ncols);
