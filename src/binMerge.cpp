/*
binMerge -- algorithm for quickly merging bins to create larger bins

v 0.0.2
rev 2016-11-10 (MS: created)
Notes:
*/

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <iomanip>
#include "read.h"

using namespace std;

typedef struct arguments_t arguments;

struct arguments_t {
    char* i;
    char* o;
    int n;
};

static void print_usage(char* argv[]);

static arguments parse_args(int argc, char* argv[]);

static void bin_tile(double* data, char* outfile_path, int n, int nrows, int ncols);
// static void bin_tile(vector< vector<double> >* data, char* outfile_path, int n, int nrows, int ncols);

int main(int argc, char *argv[]) {
    arguments args;
    int nrows, ncols;
    double* data;
    // vector< vector<double> > data;

    args = parse_args(argc, argv);
    // cout << args.i << endl << args.o << endl << args.n << endl;

    data = read_table(args.i, &nrows, &ncols, 1);

    bin_tile(data, args.o, args.n, nrows, ncols);
    //bin_tile(&data, args.o, args.n, nrows, ncols);

    return 0;
}

static void bin_tile(double* data, char* outfile, int n, int nrows, int ncols) {
// static void bin_tile(vector< vector<double> >* data, char* outfile, int n, int nrows, int ncols) {
    // Open file and check for success
    ofstream file_out(outfile);

    if (! file_out.is_open()) {
        cerr << "The file " << outfile << " could not be opened" << endl;
        exit(EXIT_FAILURE);
    }

    // Write header
    file_out << "start\tend\tobs\texpected\tvar" << endl; 

    // Caclulate number of lines which would remain unmerged at end of file
    int rem = nrows % n;

    // iterate by sub-blocks for tiling
    for (int i = 0; i < nrows - rem; i = i + n) {
        // Get start, end, and initial obs, exp, and var values
        double start = (int) data[i*ncols];  // data[i][0]
        double end = (int) data[(i+n-1)*ncols+1];  // data[i+n-1][1]

        int obs = (int)data[i*ncols+2];  // data[i][2]
        double exp = data[i*ncols+3];  // data[i][3]
        double var = data[i*ncols+4];  // data[i][4]

        // Iterate over sub-block and sum obs, exp, var
        for (int j = i+1; j < i+n; j++) {
            obs += (int) data[j*ncols+2];
            exp += data[j*ncols+3];
            var += data[j*ncols+4];
        }

        // If last iteration, add bins remaining at end of file
        if (i == nrows - n - rem) {
            end = (int) data[(i+n+rem-1)*ncols+1];

            for (int k = i+n; k < i+n+rem; k++) {
                obs += (int) data[k*ncols+2];
                exp += data[k*ncols+3];
                var += data[k*ncols+4];
            }
        }

        // Write results of tiling to file
        file_out << std::fixed << std::setprecision(0);
        file_out << start << "\t" << end << "\t" << obs << "\t";
        file_out << std::setprecision(2) << exp << "\t" << var << endl;

    }

    file_out.close();
    cerr << "Wrote " << (nrows - rem) / n << " rows and " << ncols << " columns" << endl;
}

static void print_usage(char* argv[0]) {
    cerr << "Usage: " << argv[0] << " <bin_file> [options] \n"
                      << "\t<bin_file>: input bin file\n" << endl
                      << "Options:\n"
                      << "\t-o: output file\n"
                      << "\t-n: # contiguous bins to merge"
                      << endl;
}

static arguments parse_args(int argc, char* argv[]) {

    int opt;
    arguments args;

    args.n = 10;
    args.o = NULL;

    while ((opt = getopt(argc, argv, "o:n:h")) != -1) {
        switch (opt) {
            case 'o':
                args.o = strdup(optarg);
                break;
            case 'n':
                args.n = atoi(optarg);
                break;
            case 'h':
                print_usage(argv);
                exit(EXIT_FAILURE);
            default:
                print_usage(argv);
                exit(EXIT_FAILURE);
        }
    }

    if (argc - optind != 1) {
        print_usage(argv);
        exit(EXIT_FAILURE);
    } else{
        args.i = strdup(argv[optind]);
    }

    return args;
}
