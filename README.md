# README #

The [n]ext [g]eneration [s]equencing [SH]ell is a command line interface (i.e. shell environment) for the flexible creation and execution of pipelines for next-generation sequencing data analysis.

## Features ##

* Safe(-ish) sandbox approach (cannot access system below a specified root directory)
* Automatic file input/output and data directory management
* A variety of analysis functions builtin
* Easy to add new, user-specified analysis functions

## Contact ##

Questions comments or concerns? Contact authors at:

* maxwell_sherman {at} hms.harvard.edu
